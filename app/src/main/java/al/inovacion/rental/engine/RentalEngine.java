package al.inovacion.rental.engine;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import com.plumillonforge.android.chipview.Chip;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import al.inovacion.rental.R;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.data.models.app.HttpResponse;
import al.inovacion.rental.data.models.edmunds.Makes;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Arbër Thaçi on 17-03-21.
 * Email: arberlthaci@gmail.com
 */

public class RentalEngine {

    public final static String RENTAL_LOG_TAG = "rental_log_tag";

    public static final int MENU_ITEM_ACCOUNT_INDICATOR = 6;

    public static final int PICK_IMAGE_REQUEST = 1001;
    public static final int REQUEST_IMAGE_CAPTURE = 1002;

    public static final int IMAGE_STATUS_NEUTRAL = -1;
    public static final int IMAGE_STATUS_SUCCESS = 1;
    public static final int IMAGE_STATUS_FAILED = 2;

    public static final int CARS_TO_BOOK = 1;
    public static final int USER_CARS = 2;

    public static final int MINIMUM_IMAGES_ALLOWED = 2;
    public static final int MAXIMUM_IMAGES_ALLOWED = 10;

    public static final int LOCAL_IMAGE = 1;
    public static final int SERVER_IMAGE = 2;

    public static final int WARRANTY_PERCANTAGE = 200; // (percent => 200%)

    public static final int YOYO_DURATION = 700;
    public static final int YOYO_REPEAT = 3;

    public static final int HTTP_RESPONSE_SUCCESS = 0;
    public static final int HTTP_RESPONSE_FAILED = 1;

    public static final String GSON_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    // Server API Data
    public final static String RENTAL_API_BASE_URL = "https://fastrent.al/api";
    public final static String RENTAL_API_HEADER_TOKEN_NAME = "Authorization";
    public final static String RENTAL_API_HEADER_TOKEN_VALUE = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk1ZDliMzAwNTBjODgzYTZhNTMzMGI2Mzc0MGQ3NDNkMWZhNGU1YmFiMTllMWNkMmQ5MWViNTAyN2VmZWVlNmJjMWUzZDE0ZTVhNzQ0YTQ2In0.eyJhdWQiOiIxIiwianRpIjoiOTVkOWIzMDA1MGM4ODNhNmE1MzMwYjYzNzQwZDc0M2QxZmE0ZTViYWIxOWUxY2QyZDkxZWI1MDI3ZWZlZWU2YmMxZTNkMTRlNWE3NDRhNDYiLCJpYXQiOjE0OTgxMjk1MzcsIm5iZiI6MTQ5ODEyOTUzNywiZXhwIjoxNTI5NjY1NTM3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.EbPWHGUVoaPEuPVO0dnaoj1G23ZoNQOFUyNZ7RqcfvLGMVWUL2dPSEWSSx9sYpiiZDF71mfwQE424dU4-q4_dVqSJkUOYTlvW6VUvMTYe9R5SovdXwvv377XaOPPN7McR_GjyIRYNk2__PO74bNlkQgwbG7LWhgqDQ3QjPGLb7wVEV3nQvpw8rS8KqDiECXkUVjwHWtiG7TkQzppU6xExXqPf7Mi37_GjjGxITfnLixpSUD8ynBUOEpxEeQ1jUWLhDzBmB31mLNuE9DBg-B_JP8LEWNv23GZpJGOAJFC6dO-HPgDD5LNbxJiLiB2PKyGGGM6-ncKUfci4mjNrFrF2vNVU6htr0C5IKb3FSFo1JgwRqp0KIJYmXr9EiMmmlSnw__TpBcnAFKjvAGDXbuGyAnqaAGoB9OHqclV9_8ecRQcfnXEq1SOs-TMAWdSHEqAkO-WTNPJyFWi9QuBmdcw38afpF6xTjSCJlUhULxa46IyAHPMLNYy0llRZrCmJTOIJrKnisJ0_POk4QgygswBT1VEPnVtvFPCsHXOWXTgMpLSKyYlcq2rKmoAf1w-OM_BB4oUnKZrgLdKndR0Nu4n-3UWaB78EEFZ_h7LT7ukaZNVwhCaSX83s85qrGKEM1b_pYY6w2KkrRVNrDGO2apQC4o8YA1P7xReEn9TeZDGF90";
    public final static String RENTAL_KINEZERI_NAME = "_method";
    public final static String RENTAL_KINEZERI_VALUE = "PUT";
    public final static String RENTAL_API_USERS_ROUTE = "/users";
    public final static String RENTAL_API_LOG_IN_ROUTE = "/login";
    public final static String RENTAL_API_SIGN_OUT_ROUTE = "/logout";
    public final static String RENTAL_API_UPDATE_PASSWORD_ROUTE = "/updatepass";
    public final static String RENTAL_API_RESET_PASSWORD_ROUTE = "/password/email";
    public final static String RENTAL_API_CARS_ROUTE = "/cars";
    public final static String RENTAL_API_SEARCH_FILTERS_ROUTE = "/search?";
    public final static String RENTAL_API_CAN_BOOK_CARS_ROUTE = "/can-book";
    public final static String RENTAL_API_ADD_CAR_PHOTO_ROUTE = "/add-photo";
    public final static String RENTAL_API_PHOTOS_ROUTE = "/photos";
    public final static String RENTAL_API_DELETE_PHOTO_ROUTE = "/delete";
    public final static String RENTAL_API_CHANGE_STATUS_ROUTE = "/changestatus";
    public final static String RENTAL_API_CITIES_ROUTE = "/cities";
    public final static String RENTAL_API_COUNTRY_ALBANIA = "/1";
    public final static String RENTAL_API_EDMUNDS_API_KEY = "eg7j58k5q5qynkfum6mxx52r";
    public final static String RENTAL_API_EDMUNDS_CAR_MAKES_ROUTE = "https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=" + RENTAL_API_EDMUNDS_API_KEY + "&state=new&view=full";

    // Fragment's tag
    public final static String SPLASH_FRAGMENT_TAG = "splash_ft";
    public final static String INTRO_FRAGMENT_TAG = "intro_ft";
    public final static String LOG_IN_FRAGMENT_TAG = "log_in_ft";
    public final static String FORGOT_PASSWORD_FRAGMENT_TAG = "forgot_password_ft";
    public final static String SIGN_UP_FRAGMENT_TAG = "sign_up_ft";
    public final static String UPLOAD_CARD_ID_FRAGMENT_TAG = "upload_card_id_ft";

    public final static String BROWSE_CARS_FRAGMENT_TAG = "browse_cars_ft";
    public final static String CAR_OWNER_FRAGMENT_TAG = "car_owner_ft";
    public final static String VIEW_CAR_FRAGMENT_TAG = "view_car_ft";
    public final static String DASHBOARD_FRAGMENT_TAG = "dashboard_ft";
    public final static String MESSAGES_FRAGMENT_TAG = "messages_ft";
    public final static String LIST_YOUR_CAR_FRAGMENT_TAG = "list_your_car_ft";
    public final static String LIST_YOUR_CAR_SECOND_STEP_FRAGMENT_TAG = "list_your_car_second_step_ft";
    public final static String LIST_YOUR_CAR_THIRD_STEP_FRAGMENT_TAG = "list_your_car_third_step_ft";
    public final static String LIST_YOUR_CAR_FOURTH_STEP_FRAGMENT_TAG = "list_your_car_fourth_step_ft";
    public final static String LIST_YOUR_CAR_FIFTH_STEP_FRAGMENT_TAG = "list_your_car_fifth_step_ft";
    public final static String TRIP_HISTORY_FRAGMENT_TAG = "trip_history_ft";
    public final static String FAVORITES_FRAGMENT_TAG = "favorites_ft";
    public final static String ACCOUNT_FRAGMENT_TAG = "account_ft";
    public final static String UPDATE_PASSWORD_FRAGMENT_TAG = "update_fragment_ft";
    public final static String HOW_RENTAL_WORKS_FRAGMENT_TAG = "how_rental_works_ft";
    public final static String ABOUT_US_FRAGMENT_TAG = "about_us_ft";
    public final static String SEND_FEEDBACK_FRAGMENT_TAG = "send_feedback_ft";
    public final static String TERMS_OF_SERVICES_FRAGMENT_TAG = "terms_of_services_ft";

    // Shared Preferences
    public final static String RENTAL_SHARED_PREF = "rental_shared_prefs";
    public final static String PREF_ID_KEY = "id";
    public final static String PREF_FIRST_NAME_KEY = "first_name";
    public final static String PREF_LAST_NAME_KEY = "last_name";
    public final static String PREF_EMAIL_KEY = "email";
    public final static String PREF_PHONE_KEY = "phone";
    public final static String PREF_AVATAR = "avatar";
    public final static String PREF_CARD_ID = "card_id";
    public final static String PREF_CARD_ID_STATUS = "card_id_status";

    // Documents types
    public static final int USER_DOCUMENT_ID_CARD = 1;
    public static final int CAR_DOCUMENT_KOLAUDIMI = 2;
    public static final String CAR_DOCUMENT_KOLAUDIMI_PARAM = "kolaudimi";
    public static final int CAR_DOCUMENT_INSURANCE = 3;
    public static final String CAR_DOCUMENT_INSURANCE_PARAM = "insurance";
    public static final int CAR_DOCUMENT_LICENSE = 4;
    public static final String CAR_DOCUMENT_LICENSE_PARAM = "leje_qarkullimi";

    // Documents statuses
    public static final int ACTIVE = 1;
    public static final int INACTIVE = 2;
    public static final int ID_NOT_CONFIRMED = 3;
    public static final int ID_NOT_UPLOADED = 4;
    public static final int ID_EXPIRED = 5;
    public static final int CAR_PHOTOS_NOT_CONFIRMED = 6;
    public static final int CAR_PHOTOS_NOT_UPLOADED = 7;
    public static final int CAR_KOLAUDIMI_NOT_UPLOADED = 8;
    public static final int CAR_KOLAUDIMI_NOT_CONFIRMED = 9;
    public static final int CAR_KOLAUDIMI_EXPIRED = 10;
    public static final int CAR_LEJE_QARKULLIMI_NOT_CONFIRMED = 11;
    public static final int CAR_LEJE_QARKULLIMI_NOT_UPLOADED = 12;
    public static final int CAR_LEJE_QARKULLIMI_EXPIRED = 13;
    public static final int CAR_INSURANCE_NOT_UPLOADED = 14;
    public static final int CAR_INSURANCE_NOT_CONFIRMED = 15;
    public static final int CAR_INSURANCE_EXPIRED = 16;
    public static final int CAR_DOCUMENTS_NOT_UPLOADED = 17;
    public static final int CAR_DOCUMENTS_NOT_CONFIRMED = 18;
    public static final int CAR_DOCUMENTS_EXPIRED = 19;
    public static final int WAITING_APPROVAL = 20;
    public static final int APPROVED = 21;
    public static final int REJECTED = 22;

    // Cashed data
    private static User user;
    private static ArrayList<Car> carsList;
    private static List<Chip> filters;
    private static int viewCarMode;
    private static Car selectedCar;
    private static User carOwner;
    private static Makes makes;

    private static ArrayList<String> months;

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static HttpResponse retrieveStream(String url) {
        OkHttpClient client = new OkHttpClient();
        InputStream inputStream = null;
        int status = HTTP_RESPONSE_FAILED;

        try {
            Request request = new Request.Builder()
                    .addHeader(RENTAL_API_HEADER_TOKEN_NAME, RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .build();

            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                inputStream = response.body().byteStream();
                status = HTTP_RESPONSE_SUCCESS;
            }
        }
        catch (Exception e) {
            Log.i(RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return new HttpResponse(inputStream, status);
    }

    public static String getRealPathFromURI(Context mContext, Uri mUri) {
        try {
            Cursor cursor = mContext.getContentResolver().query(mUri, null, null, null, null);
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = mContext.getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();

            return path;
        }
        catch (Exception e) {
            return "file_not_found";
        }
    }

    public static float getCorrectRotationFromOrientation(Context mContext, Uri mUri) {
        float rotation = 0f;

        try {
            ExifInterface ei = new ExifInterface(RentalEngine.getRealPathFromURI(mContext, mUri));
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            if (orientation > 0) {
                rotation = 90f;
            }
        }
        catch (Exception e) {
            Log.i(RENTAL_LOG_TAG, "Error for getCorrectRotationFromOrientation", e);
        }

        return rotation;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static void setUpAnimationDecoratorHelper(final Context mContext, RecyclerView mRecyclerView) {
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {

            Drawable background;
            boolean initiated;

            private void init() {
                background = ContextCompat.getDrawable(mContext, R.drawable.background_red_board_shape);
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                if (!initiated) {
                    init();
                }

                if (parent.getItemAnimator().isRunning()) {
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;

                    int left = 0;
                    int right = parent.getWidth();
                    int top = 0;
                    int bottom = 0;

                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View child = parent.getLayoutManager().getChildAt(i);
                        if (child.getTranslationY() < 0) {
                            lastViewComingDown = child;
                        }
                        else if (child.getTranslationY() > 0) {
                            if (firstViewComingUp == null) {
                                firstViewComingUp = child;
                            }
                        }
                    }

                    if (lastViewComingDown != null && firstViewComingUp != null) {
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }
                    else if (lastViewComingDown != null) {
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = lastViewComingDown.getBottom();
                    }
                    else if (firstViewComingUp != null) {
                        top = firstViewComingUp.getTop();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }

                    background.setBounds(left, top, right, bottom);
                    background.draw(c);
                }
                super.onDraw(c, parent, state);
            }
        });
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        RentalEngine.user = user;
    }

    public static ArrayList<Car> getCarsList() {
        return carsList;
    }

    public static void setCarsList(ArrayList<Car> carsList) {
        RentalEngine.carsList = carsList;
    }

    public static List<Chip> getFilters() {
        if (filters == null) {
            return new ArrayList<>();
        }
        return filters;
    }

    public static void setFilters(List<Chip> filters) {
        RentalEngine.filters = filters;
    }

    public static int getViewCarMode() {
        return viewCarMode;
    }

    public static void setViewCarMode(int viewCarMode) {
        RentalEngine.viewCarMode = viewCarMode;
    }

    public static Car getSelectedCar() {
        return selectedCar;
    }

    public static void setSelectedCar(Car selectedCar) {
        RentalEngine.selectedCar = selectedCar;
    }

    public static User getCarOwner() {
        return carOwner;
    }

    public static void setCarOwner(User carOwner) {
        RentalEngine.carOwner = carOwner;
    }

    public static Makes getMakes() {
        return makes;
    }

    public static void setMakes(Makes makes) {
        RentalEngine.makes = makes;
    }

    private static void initializeMonths() {
        months = new ArrayList<>();
        months.add("Janar");
        months.add("Shkurt");
        months.add("Mars");
        months.add("Prill");
        months.add("Maj");
        months.add("Qershor");
        months.add("Korrik");
        months.add("Gusht");
        months.add("Shtator");
        months.add("Tetor");
        months.add("Nëntor");
        months.add("Dhjetor");
    }

    public static String getMonth(int index) {
        if (months == null || months.size() == 0) {
            initializeMonths();
        }
        return months.get(index);
    }
}
