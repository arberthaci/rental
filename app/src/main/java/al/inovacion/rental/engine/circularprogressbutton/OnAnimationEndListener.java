package al.inovacion.rental.engine.circularprogressbutton;

public interface OnAnimationEndListener {

    void onAnimationEnd();
}
