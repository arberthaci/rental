package al.inovacion.rental.intro;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import com.crashlytics.android.Crashlytics;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;

import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.BackButtonEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.event.StoreUserDataEvent;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.data.models.api.UserResponse;
import io.fabric.sdk.android.Fabric;

import al.inovacion.rental.R;
import al.inovacion.rental.engine.RentalEngine;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Arbër Thaçi on 17-03-21.
 * Email: arberlthaci@gmail.com
 */

public class SignUpFragment extends Fragment implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageView toolbarImage;
    private EditText emailField;
    private EditText firstNameField;
    private EditText lastNameField;
    private EditText phoneField;
    private EditText passwordField;
    private EditText passwordAgainField;
    private LinearLayout signUpBtn;
    private TextView signUpLabel;
    private AVLoadingIndicatorView loading;
    private ImageView ivSuccess;

    private User userModel;
    private int selectedPage = 0;
    private boolean isLoading = false;

    public SignUpFragment() {}

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new CurrentFragmentTagEvent(RentalEngine.SIGN_UP_FRAGMENT_TAG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.introSignUpBtn:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                if (!isLoading) {
                    tryToRegister();
                }
                break;
        }
    }

    private void initializeComponents(View view) {
        toolbar = (Toolbar) view.findViewById(R.id.signUpToolbar);
        toolbarImage = (ImageView) view.findViewById(R.id.signUpImageToolbar);
        emailField = (EditText) view.findViewById(R.id.etIntroSignUpEmail);
        firstNameField = (EditText) view.findViewById(R.id.etIntroSignUpFirstName);
        lastNameField = (EditText) view.findViewById(R.id.etIntroSignUpLastName);
        phoneField = (EditText) view.findViewById(R.id.etIntroSignUpPhone);
        passwordField = (EditText) view.findViewById(R.id.etIntroSignUpPassword);
        passwordAgainField = (EditText) view.findViewById(R.id.etIntroSignUpPasswordAgain);
        signUpBtn = (LinearLayout) view.findViewById(R.id.introSignUpBtn);
        signUpLabel = (TextView) view.findViewById(R.id.tvSignUpLabel);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.introLoading);
        ivSuccess = (ImageView) view.findViewById(R.id.introSuccess);
    }

    private void afterInitialization() {
        if (selectedPage == 0) {
            toolbarImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.car_intro_toolbar_1));
        }
        else if (selectedPage == 1) {
            toolbarImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.car_intro_toolbar_2));
        }

        toolbar.setTitle(String.valueOf(getResources().getString(R.string.intro_sign_up_button)));
        toolbar.setNavigationIcon(ContextCompat.getDrawable(getContext(), R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new BackButtonEvent());
            }
        });

        signUpBtn.setOnClickListener(this);
        signUpLabel.setVisibility(View.VISIBLE);
        loading.smoothToHide();
        loading.setVisibility(View.GONE);
        ivSuccess.setVisibility(View.GONE);
    }

    private void tryToRegister() {
        String email = emailField.getText().toString();
        String firstName = firstNameField.getText().toString();
        String lastName = lastNameField.getText().toString();
        String phone = phoneField.getText().toString();
        String password = passwordField.getText().toString();
        String passwordAgain = passwordAgainField.getText().toString();

        if (areFieldsFilled(email, firstName, lastName, phone, password, passwordAgain)) {
            if(doesPasswordsMatch(password, passwordAgain)) {
                signUpLabel.setVisibility(View.GONE);
                ivSuccess.setVisibility(View.GONE);
                loading.setVisibility(View.VISIBLE);
                loading.smoothToShow();

                fillUserModel(email, firstName, lastName, phone, password);
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    new RegisterUser().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_USERS_ROUTE);
                }
                else {
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(750);
                            }
                            catch (InterruptedException e) {
                                Log.i(RentalEngine.RENTAL_LOG_TAG, "SignUpFragment thread Exception: " + e.toString());
                            }
                            finally {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showErrorMessage(getResources().getString(R.string.snackbar_no_internet_error_message));
                                    }
                                });
                            }
                        }
                    };
                    timer.start();
                }
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(500);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "SignUpFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(getResources().getString(R.string.snackbar_passwords_do_not_match));
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    private boolean areFieldsFilled(String email, String firstName, String lastName, String phone, String password, String passwordAgain) {
        if (!email.equalsIgnoreCase("") &&
                !firstName.equalsIgnoreCase("") &&
                !lastName.equalsIgnoreCase("") &&
                !phone.equalsIgnoreCase("") &&
                !password.equalsIgnoreCase("") &&
                !passwordAgain.equalsIgnoreCase("")) {
            return true;
        }
        if (email.equalsIgnoreCase("")) {
            playYoyo(emailField);
        }
        if (firstName.equalsIgnoreCase("")) {
            playYoyo(firstNameField);
        }
        if (lastName.equalsIgnoreCase("")) {
            playYoyo(lastNameField);
        }
        if (phone.equalsIgnoreCase("")) {
            playYoyo(phoneField);
        }
        if (password.equalsIgnoreCase("")) {
            playYoyo(passwordField);
        }
        if (passwordAgain.equalsIgnoreCase("")) {
            playYoyo(passwordAgainField);
        }
        return false;
    }

    private boolean doesPasswordsMatch(String firstPassword, String secondPassword) {
        if (firstPassword.equals(secondPassword)) {
            return true;
        }
        return false;
    }

    private void fillUserModel(String email, String firstName, String lastName, String phone, String password) {
        if (userModel == null) {
            userModel = new User();
        }

        userModel.setEmail(email);
        userModel.setFirstName(firstName);
        userModel.setLastName(lastName);
        userModel.setPhone(phone);
        userModel.setPassword(password);
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private void openUploadCardIdFragment() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "SignUpFragment thread Exception: " + e.toString());
                }
                finally {
                    UploadCardIdFragment uploadCardIdFragment = new UploadCardIdFragment();
                    EventBus.getDefault().post(new OpenNewFragmentEvent(uploadCardIdFragment, null));
                }
            }
        };
        timer.start();
    }

    private void storeUserData() {
        EventBus.getDefault().post(new StoreUserDataEvent(userModel));
    }

    public void setSelectedPage(int mSelectedPage) {
        selectedPage = mSelectedPage;
    }

    private void showErrorMessage(String errorMessage) {
        loading.smoothToHide();
        loading.setVisibility(View.GONE);
        signUpLabel.setVisibility(View.VISIBLE);
        ivSuccess.setVisibility(View.GONE);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
        isLoading = false;
    }

    public InputStream getUserRegistrationInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = new FormBody.Builder()
                    .addEncoded("first_name", userModel.getFirstName())
                    .addEncoded("last_name", userModel.getLastName())
                    .addEncoded("email", userModel.getEmail())
                    .addEncoded("password", userModel.getPassword())
                    .addEncoded("phone", userModel.getPhone())
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class RegisterUser extends AsyncTask<String, String, String> {
        private UserResponse mUserResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUserRegistrationInputStream(uri[0]);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mUserResponse = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "SignUpFragment RegisterUser doInBackground Exception: " + e.toString());
                mUserResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mUserResponse != null) {
                switch (mUserResponse.getResponseStatus()) {
                    case 0:
                        signUpLabel.setVisibility(View.GONE);
                        loading.smoothToHide();
                        loading.setVisibility(View.GONE);
                        ivSuccess.setVisibility(View.VISIBLE);

                        if (mUserResponse.getResponseData() != null) {
                            userModel.setId(mUserResponse.getResponseData().getId());
                        }
                        else {
                            userModel.setId(-1);
                        }

                        storeUserData();
                        openUploadCardIdFragment();
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_register_failed)));
                        break;
                    case 2:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_email_already_exists)));
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_register_failed)));
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_register_failed)));
            }
        }
    }
}