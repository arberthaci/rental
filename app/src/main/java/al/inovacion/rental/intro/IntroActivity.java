package al.inovacion.rental.intro;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.concurrent.TimeUnit;

import al.inovacion.rental.data.MSharedPreferences;
import al.inovacion.rental.data.event.CloseSplashFragmentEvent;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.BackButtonEvent;
import al.inovacion.rental.data.event.OpenHomeActivityEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.event.StoreUserDataEvent;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.home.HomeActivity;
import io.fabric.sdk.android.Fabric;

import al.inovacion.rental.R;
import al.inovacion.rental.engine.RentalEngine;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Arbër Thaçi on 17-03-20.
 * Email: arberlthaci@gmail.com
 */

public class IntroActivity extends AppCompatActivity {

    private String currentFragmentTag = "empty";
    private CoordinatorLayout coordinatorLayout;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_intro);

        initializeComponents();

        if (savedInstanceState == null) {
            openSplashFragment();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (!currentFragmentTag.equalsIgnoreCase("empty") && currentFragmentTag != RentalEngine.SPLASH_FRAGMENT_TAG) {
            if (currentFragmentTag == RentalEngine.UPLOAD_CARD_ID_FRAGMENT_TAG) {
                openHomeActivity();
            }
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void openSplashFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .add(R.id.intro_fragment_container, SplashFragment.newInstance(), RentalEngine.SPLASH_FRAGMENT_TAG)
                .commit();
    }

    private void initializeComponents() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.intro_fragment_coordinator);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ShowSnackBarMessageEvent event) {
        if (event != null) {
            snackbar = Snackbar.make(coordinatorLayout, event.getMessage(), Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(DismissSnackBarEvent event) {
        if (event != null && snackbar != null) {
            snackbar.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CloseKeyboardEvent event) {
        if (event != null) {
            closeKeyboard();
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onMessage(CurrentFragmentTagEvent event) {
        if (event != null) {
            currentFragmentTag = event.getTag();
        }
    }

    private void openIntroFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .add(R.id.intro_fragment_container, IntroFragment.newInstance(), RentalEngine.INTRO_FRAGMENT_TAG)
                .commitAllowingStateLoss();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CloseSplashFragmentEvent event) {
        MSharedPreferences mSharedPreferences = new MSharedPreferences(this, RentalEngine.RENTAL_SHARED_PREF);
        if (mSharedPreferences.getInt(RentalEngine.PREF_ID_KEY) != 0) {
            openHomeActivity();
        }
        else {
            closeFragmentByTag(RentalEngine.SPLASH_FRAGMENT_TAG);
            openIntroFragment();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OpenNewFragmentEvent event) {
        if (event != null) {
            openNewFragment(event.getFragment());
        }
    }

    private void openNewFragment(Fragment mFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        transaction.replace(R.id.intro_fragment_container, mFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void closeFragmentByTag(String mTag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(mTag);
        if (fragment != null) {
            fragmentManager.beginTransaction()
                    .remove(fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .commit();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OpenHomeActivityEvent event) {
        if (event != null) {
            if (event.getUser() != null) {
                storeUserData(event.getUser());
            }
            openHomeActivity();
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onMessage(StoreUserDataEvent event) {
        if (event != null) {
            storeUserData(event.getUser());
        }
    }

    private void storeUserData(User mUser) {
        MSharedPreferences mSharedPreferences = new MSharedPreferences(this, RentalEngine.RENTAL_SHARED_PREF);
        mSharedPreferences.putInt(RentalEngine.PREF_ID_KEY, mUser.getId());
        mSharedPreferences.putString(RentalEngine.PREF_FIRST_NAME_KEY, mUser.getFirstName());
        mSharedPreferences.putString(RentalEngine.PREF_LAST_NAME_KEY, mUser.getLastName());
        mSharedPreferences.putString(RentalEngine.PREF_EMAIL_KEY, mUser.getEmail());
        mSharedPreferences.putString(RentalEngine.PREF_PHONE_KEY, mUser.getPhone());
        if (mUser.getUser_profile() != null) {
            if (mUser.getUser_profile().getAvatar() != null) {
                mSharedPreferences.putString(RentalEngine.PREF_AVATAR, mUser.getUser_profile().getAvatar());
            }
            if (mUser.getUser_profile().getIdCard() != null) {
                mSharedPreferences.putString(RentalEngine.PREF_CARD_ID, mUser.getUser_profile().getIdCard());
            }
            mSharedPreferences.putInt(RentalEngine.PREF_CARD_ID_STATUS, mUser.getUser_profile().getStatusId());
        }
    }

    private void openHomeActivity() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(homeIntent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(BackButtonEvent event) {
        if (event != null) {
            closeKeyboard();
            new OnBackPressedCustom().execute();
        }
    }

    private class OnBackPressedCustom extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... uri) {
            try {
                TimeUnit.MILLISECONDS.sleep(75);
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "IntroActivity OnBackPressedCustom doInBackground Exception: " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            onBackPressed();
        }
    }
}
