package al.inovacion.rental.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import al.inovacion.rental.data.event.CloseSplashFragmentEvent;
import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import io.fabric.sdk.android.Fabric;

import al.inovacion.rental.R;
import al.inovacion.rental.engine.RentalEngine;

/**
 * Created by Arbër Thaçi on 17-03-21.
 * Email: arberlthaci@gmail.com
 */

public class SplashFragment extends Fragment {

    public SplashFragment() {}

    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new CurrentFragmentTagEvent(RentalEngine.SPLASH_FRAGMENT_TAG));
        startCountdown();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //initializeComponents(view);
        //afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void startCountdown() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "SplashFragment thread Exception: " + e.toString());
                }
                finally {
                    EventBus.getDefault().post(new CloseSplashFragmentEvent());
                }
            }
        };
        timer.start();
    }
}
