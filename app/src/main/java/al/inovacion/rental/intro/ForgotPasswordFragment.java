package al.inovacion.rental.intro;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.BackButtonEvent;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.UserResponse;
import al.inovacion.rental.engine.RentalEngine;
import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Arbër Thaçi on 17-05-31.
 * Email: arberlthaci@gmail.com
 */

public class ForgotPasswordFragment extends Fragment implements View.OnClickListener {

    private ImageView backgroundImages;
    private ImageView backBtn;
    private EditText emailField;
    private LinearLayout resetBtn;
    private TextView resetLabel;
    private AVLoadingIndicatorView loading;
    private ImageView ivSuccess;

    private int selectedPage = 0;
    private boolean isLoading = false;

    public ForgotPasswordFragment() {}


    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new CurrentFragmentTagEvent(RentalEngine.FORGOT_PASSWORD_FRAGMENT_TAG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivForgotPasswordBack:
                EventBus.getDefault().post(new BackButtonEvent());
                break;
            case R.id.introForgotPasswordBtn:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                if (!isLoading) {
                    tryToResetPassword();
                }
                break;
        }
    }

    public void setSelectedPage(int mSelectedPage) {
        selectedPage = mSelectedPage;
    }

    private void initializeComponents(View view) {
        backgroundImages = (ImageView) view.findViewById(R.id.ivForgotPasswordBackground);
        backBtn = (ImageView) view.findViewById(R.id.ivForgotPasswordBack);
        emailField = (EditText) view.findViewById(R.id.etIntroForgotPasswordEmail);
        resetBtn = (LinearLayout) view.findViewById(R.id.introForgotPasswordBtn);
        resetLabel = (TextView) view.findViewById(R.id.tvForgotPasswordLabel);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.introLoading);
        ivSuccess = (ImageView) view.findViewById(R.id.introSuccess);
    }

    private void afterInitialization() {
        if (selectedPage == 0) {
            backgroundImages.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.car_intro_1));
        }
        else if (selectedPage == 1) {
            backgroundImages.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.car_intro_2));
        }

        backBtn.setOnClickListener(this);
        resetBtn.setOnClickListener(this);
        resetLabel.setVisibility(View.VISIBLE);
        loading.smoothToHide();
        loading.setVisibility(View.GONE);
        ivSuccess.setVisibility(View.GONE);
    }

    private void tryToResetPassword() {
        String email = emailField.getText().toString();

        if (isEmailFilled(email)) {
            resetLabel.setVisibility(View.GONE);
            ivSuccess.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);
            loading.smoothToShow();

            if (RentalEngine.isNetworkAvailable(getActivity())) {
                new ResetPassword().execute(email, RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_RESET_PASSWORD_ROUTE);
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(750);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "ForgotPasswordFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(getResources().getString(R.string.snackbar_no_internet_error_message));
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    private boolean isEmailFilled(String email) {
        if (!email.equalsIgnoreCase("")) {
            return true;
        }
        else {
            playYoyo(emailField);
            return false;
        }
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private void showErrorMessage(String errorMessage) {
        loading.smoothToHide();
        loading.setVisibility(View.GONE);
        resetLabel.setVisibility(View.VISIBLE);
        ivSuccess.setVisibility(View.GONE);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
        isLoading = false;
    }

    public InputStream getUserResetPasswordnputStream(String url, String email) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = new FormBody.Builder()
                    .addEncoded("email", email)
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class ResetPassword extends AsyncTask<String, String, String> {
        private UserResponse mUserResponse;
        private String email;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
            email = "";
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                email = uri[0];
                InputStream inputStream = getUserResetPasswordnputStream(uri[1], email);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mUserResponse = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ForgotPasswordFragment ResetPassword doInBackground Exception: " + e.toString());
                mUserResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mUserResponse != null) {
                switch (mUserResponse.getResponseStatus()) {
                    case 0:
                        resetLabel.setVisibility(View.GONE);
                        loading.smoothToHide();
                        loading.setVisibility(View.GONE);
                        ivSuccess.setVisibility(View.VISIBLE);
                        resetBtn.setClickable(false);
                        EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_reset_password_success)), false));
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_reset_password_failed)));
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_log_in_failed)));
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_log_in_failed)));
            }
        }
    }
}
