package al.inovacion.rental.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import org.greenrobot.eventbus.EventBus;

import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.RentalSliderView;
import io.fabric.sdk.android.Fabric;

import al.inovacion.rental.R;

/**
 * Created by Arbër Thaçi on 17-03-21.
 * Email: arberlthaci@gmail.com
 */

public class IntroFragment extends Fragment implements View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    private SliderLayout introSlider;
    private LinearLayout logInButton;
    private LinearLayout signUpButton;
    private int selectedPage = 0;

    public IntroFragment() {}

    public static IntroFragment newInstance() {
        return new IntroFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new CurrentFragmentTagEvent(RentalEngine.INTRO_FRAGMENT_TAG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_intro, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llIntroLogIn:
                LogInFragment logInFragment = new LogInFragment();
                logInFragment.setSelectedPage(selectedPage);
                EventBus.getDefault().post(new OpenNewFragmentEvent(logInFragment, null));
                break;
            case R.id.llIntroSignUp:
                SignUpFragment signUpFragment = new SignUpFragment();
                signUpFragment.setSelectedPage(selectedPage);
                EventBus.getDefault().post(new OpenNewFragmentEvent(signUpFragment, null));
                break;
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getContext(), slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        selectedPage = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    private void initializeComponents(View view) {
        introSlider = (SliderLayout) view.findViewById(R.id.introSlider);
        logInButton = (LinearLayout) view.findViewById(R.id.llIntroLogIn);
        signUpButton = (LinearLayout) view.findViewById(R.id.llIntroSignUp);
    }

    private void afterInitialization() {
        RentalSliderView carIntro = new RentalSliderView(getContext());
        carIntro.image(R.drawable.car_intro_slider_1).setScaleType(BaseSliderView.ScaleType.CenterCrop);
        introSlider.addSlider(carIntro);
        carIntro = new RentalSliderView(getContext());
        carIntro.image(R.drawable.car_intro_slider_2).setScaleType(BaseSliderView.ScaleType.CenterCrop);
        introSlider.addSlider(carIntro);
        introSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
        introSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        introSlider.setCustomAnimation(new DescriptionAnimation());
        introSlider.setDuration(4000);
        introSlider.addOnPageChangeListener(this);

        logInButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
    }
}
