package al.inovacion.rental.intro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.data.MSharedPreferences;
import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.OpenHomeActivityEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.UserResponse;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Arbër Thaçi on 17-03-29.
 * Email: arberlthaci@gmail.com
 */

public class UploadCardIdFragment extends Fragment implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageView chooseBtn;
    private ImageView takeAPictureBtn;
    private TextView cardIdLabel;
    private TextView cardIdInfo;
    private ImageView cardIdImage;
    private TextView cardIdTitle;
    private CircularProgressButton laterBtn;
    private CircularProgressButton uploadBtn;

    private int userId = -1;
    private Uri mUri;
    private String mFileName = "";
    private String mPath = "";
    private int px;
    private int py;

    public UploadCardIdFragment() {}

    public static UploadCardIdFragment newInstance() {
        return new UploadCardIdFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new CurrentFragmentTagEvent(RentalEngine.UPLOAD_CARD_ID_FRAGMENT_TAG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_upload_card_id, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeComponents(view);
        afterInitialization();

        MSharedPreferences mSharedPreferences = new MSharedPreferences(getContext(), RentalEngine.RENTAL_SHARED_PREF);
        userId = mSharedPreferences.getInt(RentalEngine.PREF_ID_KEY);

        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
        py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivIntroUploadChooseImageFromGallery:
                chooseAnImageFromGallery();
                break;
            case R.id.ivIntroUploadTakeAPicture:
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
                }
                else {
                    takeAPicture();
                }
                break;
            case R.id.cpbIntroUploadLater:
                openHomeActivity();
                break;
            case R.id.cpbIntroUploadUpload:
                tryToUploadCardId();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeAPicture();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RentalEngine.PICK_IMAGE_REQUEST:
                    if (data != null && data.getData() != null) {
                        mUri = data.getData();
                        mPath = RentalEngine.getRealPathFromURI(getContext(), mUri);
                        mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                        float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                        Picasso.with(getActivity())
                                .load(mUri)
                                .resize(px, py)
                                .centerCrop()
                                .rotate(tempRotation)
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.error_loading)
                                .into(cardIdImage);
                        cardIdTitle.setText(mFileName);
                        enableViews(true);
                    }
                    break;
                case RentalEngine.REQUEST_IMAGE_CAPTURE:
                    mPath = mUri.getPath();
                    float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                    Picasso.with(getActivity())
                            .load(mUri)
                            .resize(px, py)
                            .centerCrop()
                            .rotate(tempRotation)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error_loading)
                            .into(cardIdImage);
                    cardIdTitle.setText(mFileName);
                    enableViews(true);
                    break;
            }
            uploadBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            laterBtn.clearAnimation();
            uploadBtn.clearAnimation();
        }
    };

    private void initializeComponents (View view) {
        toolbar = (Toolbar) view.findViewById(R.id.uploadCardIdToolbar);
        chooseBtn = (ImageView) view.findViewById(R.id.ivIntroUploadChooseImageFromGallery);
        takeAPictureBtn = (ImageView) view.findViewById(R.id.ivIntroUploadTakeAPicture);
        cardIdLabel = (TextView) view.findViewById(R.id.tvCardIdLabel);
        cardIdInfo = (TextView) view.findViewById(R.id.tvCardIdInfo);
        cardIdImage = (ImageView) view.findViewById(R.id.ivIntroCardId);
        cardIdTitle = (TextView) view.findViewById(R.id.tvCardIdTitle);
        laterBtn = (CircularProgressButton) view.findViewById(R.id.cpbIntroUploadLater);
        uploadBtn = (CircularProgressButton) view.findViewById(R.id.cpbIntroUploadUpload);
    }

    private void afterInitialization () {
        toolbar.setTitle(String.valueOf(getResources().getString(R.string.intro_upload_card_id_string)));

        enableViews(false);

        chooseBtn.setOnClickListener(this);
        takeAPictureBtn.setOnClickListener(this);

        laterBtn.setIndeterminateProgressMode(true);
        laterBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        laterBtn.setOnClickListener(this);

        ViewTreeObserver vtoLater = laterBtn.getViewTreeObserver();
        vtoLater.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    laterBtn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    laterBtn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

        uploadBtn.setIndeterminateProgressMode(true);
        uploadBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        uploadBtn.setOnClickListener(this);

        ViewTreeObserver vtoUpload = uploadBtn.getViewTreeObserver();
        vtoUpload.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    uploadBtn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    uploadBtn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    private void enableViews(boolean enable) {
        if (enable) {
            cardIdLabel.setVisibility(View.VISIBLE);
            cardIdInfo.setVisibility(View.GONE);
            cardIdImage.setVisibility(View.VISIBLE);
            cardIdTitle.setVisibility(View.VISIBLE);
        }
        else {
            cardIdLabel.setVisibility(View.GONE);
            cardIdInfo.setVisibility(View.VISIBLE);
            cardIdImage.setVisibility(View.GONE);
            cardIdTitle.setVisibility(View.GONE);
        }
        uploadBtn.setEnabled(enable);
    }

    private void enableActionsButton(boolean enable) {
        chooseBtn.setEnabled(enable);
        takeAPictureBtn.setEnabled(enable);
        laterBtn.setEnabled(enable);
    }

    private void chooseAnImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RentalEngine.PICK_IMAGE_REQUEST);
    }

    private void takeAPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mUri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);

        startActivityForResult(intent, RentalEngine.REQUEST_IMAGE_CAPTURE);
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), String.valueOf(getResources().getString(R.string.app_image_folder_name)));

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        mFileName = "IMG_"+ timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + mFileName);
    }

    private void openHomeActivity() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "UploadCardIdFragment thread Exception: " + e.toString());
                }
                finally {
                    EventBus.getDefault().post(new OpenHomeActivityEvent(null));
                }
            }
        };
        timer.start();
    }

    private void showErrorMessage(String errorMessage) {
        uploadBtn.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        uploadBtn.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private void tryToUploadCardId() {
        uploadBtn.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
        enableActionsButton(false);

        if (RentalEngine.isNetworkAvailable(getActivity())) {
            new UploadCardId().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_USERS_ROUTE + "/" + userId);
        }
        else {
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(750);
                    }
                    catch (InterruptedException e) {
                        Log.i(RentalEngine.RENTAL_LOG_TAG, "UploadCardIdFragment thread Exception: " + e.toString());
                    }
                    finally {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showErrorMessage(getResources().getString(R.string.snackbar_no_internet_error_message));
                                enableActionsButton(true);
                            }
                        });
                    }
                }
            };
            timer.start();
        }
    }

    public InputStream getUploadCardIdInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(mPath));
            File tempFile = new File(mPath);

            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("id_card", mFileName, RequestBody.create(MEDIA_TYPE, tempFile))
                    .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UploadCardId extends AsyncTask<String, String, String> {
        private UserResponse mUserResponse;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUploadCardIdInputStream(uri[0]);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mUserResponse = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "UploadCardIdFragment UploadCardId doInBackground Exception: " + e.toString());
                mUserResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mUserResponse != null) {
                switch (mUserResponse.getResponseStatus()) {
                    case 0:
                        if (mUserResponse.getResponseData() != null &&
                                mUserResponse.getResponseData().getUser_profile() != null &&
                                mUserResponse.getResponseData().getUser_profile().getIdCard() != null) {

                            MSharedPreferences mSharedPreferences = new MSharedPreferences(getContext(), RentalEngine.RENTAL_SHARED_PREF);
                            mSharedPreferences.putString(RentalEngine.PREF_CARD_ID, mUserResponse.getResponseData().getUser_profile().getIdCard());

                            uploadBtn.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                            uploadBtn.setAnimationListener(onAnimationEndListener);
                            uploadBtn.setEnabled(false);

                            openHomeActivity();
                        }
                        else {
                            showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_upload_image_failed)));
                            enableActionsButton(true);
                        }
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_upload_image_failed)));
                        enableActionsButton(true);
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_upload_image_failed)));
                        enableActionsButton(true);
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_upload_image_failed)));
                enableActionsButton(true);
            }
        }
    }
}
