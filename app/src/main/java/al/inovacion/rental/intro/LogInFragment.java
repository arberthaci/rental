package al.inovacion.rental.intro;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import al.inovacion.rental.data.event.BackButtonEvent;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.CurrentFragmentTagEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.OpenHomeActivityEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.data.models.api.UserResponse;
import io.fabric.sdk.android.Fabric;

import al.inovacion.rental.R;
import al.inovacion.rental.engine.RentalEngine;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Arbër Thaçi on 17-03-21.
 * Email: arberlthaci@gmail.com
 */

public class LogInFragment extends Fragment implements View.OnClickListener {

    private ImageView backgroundImages;
    private ImageView backBtn;
    private EditText emailField;
    private EditText passwordField;
    private LinearLayout loginBtn;
    private TextView loginLabel;
    private AVLoadingIndicatorView loading;
    private ImageView ivSuccess;
    private TextView signUpBtn;
    private TextView forgotPasswordBtn;

    private User userModel;
    private int selectedPage = 0;
    private boolean isLoading = false;

    public LogInFragment() { }

    public static LogInFragment newInstance() {
        return new LogInFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new CurrentFragmentTagEvent(RentalEngine.LOG_IN_FRAGMENT_TAG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_log_in, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLogInBack:
                EventBus.getDefault().post(new BackButtonEvent());
                break;
            case R.id.introLogInBtn:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                if (!isLoading) {
                    tryToLogin();
                }
                break;
            case R.id.tvIntroLogInSignUp:
                SignUpFragment signUpFragment = new SignUpFragment();
                signUpFragment.setSelectedPage(selectedPage);
                EventBus.getDefault().post(new OpenNewFragmentEvent(signUpFragment, null));
                break;
            case R.id.tvIntroLogInForgotPassword:
                ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
                forgotPasswordFragment.setSelectedPage(selectedPage);
                EventBus.getDefault().post(new OpenNewFragmentEvent(forgotPasswordFragment, null));
                break;
        }
    }

    private void initializeComponents(View view) {
        backgroundImages = (ImageView) view.findViewById(R.id.ivLogInBackground);
        backBtn = (ImageView) view.findViewById(R.id.ivLogInBack);
        emailField = (EditText) view.findViewById(R.id.etIntroLogInEmail);
        passwordField = (EditText) view.findViewById(R.id.etIntroLogInPassword);
        loginBtn = (LinearLayout) view.findViewById(R.id.introLogInBtn);
        loginLabel = (TextView) view.findViewById(R.id.tvLogInLabel);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.introLoading);
        ivSuccess = (ImageView) view.findViewById(R.id.introSuccess);
        signUpBtn = (TextView) view.findViewById(R.id.tvIntroLogInSignUp);
        forgotPasswordBtn = (TextView) view.findViewById(R.id.tvIntroLogInForgotPassword);
    }

    private void afterInitialization() {
        if (selectedPage == 0) {
            backgroundImages.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.car_intro_1));
        }
        else if (selectedPage == 1) {
            backgroundImages.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.car_intro_2));
        }

        backBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
        forgotPasswordBtn.setOnClickListener(this);
        loginLabel.setVisibility(View.VISIBLE);
        loading.smoothToHide();
        loading.setVisibility(View.GONE);
        ivSuccess.setVisibility(View.GONE);
    }

    private void tryToLogin() {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();

        if (areFieldsFilled(email, password)) {
            loginLabel.setVisibility(View.GONE);
            ivSuccess.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);
            loading.smoothToShow();

            fillUserModel(email, password);

            if (RentalEngine.isNetworkAvailable(getActivity())) {
                new LogInUser().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_LOG_IN_ROUTE);
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(750);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "LogInFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(getResources().getString(R.string.snackbar_no_internet_error_message));
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    private boolean areFieldsFilled(String email, String password) {
        if (!email.equalsIgnoreCase("") && !password.equalsIgnoreCase("")) {
            return true;
        }
        if (email.equalsIgnoreCase("")) {
            playYoyo(emailField);
        }
        if (password.equalsIgnoreCase("")) {
            playYoyo(passwordField);
        }
        return false;
    }

    private void fillUserModel(String email, String password) {
        if (userModel == null) {
            userModel = new User();
        }

        userModel.setEmail(email);
        userModel.setPassword(password);
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private void openHomeActivity(final User mUser) {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(750);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "LogInFragment thread Exception: " + e.toString());
                }
                finally {
                    EventBus.getDefault().post(new OpenHomeActivityEvent(mUser));
                }
            }
        };
        timer.start();
    }

    public void setSelectedPage(int mSelectedPage) {
        selectedPage = mSelectedPage;
    }

    private void showErrorMessage(String errorMessage) {
        loading.smoothToHide();
        loading.setVisibility(View.GONE);
        loginLabel.setVisibility(View.VISIBLE);
        ivSuccess.setVisibility(View.GONE);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
        isLoading = false;
    }

    public InputStream getUserLogInInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = new FormBody.Builder()
                    .addEncoded("email", userModel.getEmail())
                    .addEncoded("password", userModel.getPassword())
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class LogInUser extends AsyncTask<String, String, String> {
        private UserResponse mUserResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUserLogInInputStream(uri[0]);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mUserResponse = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "LogInFragment LogInUser doInBackground Exception: " + e.toString());
                mUserResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mUserResponse != null) {
                switch (mUserResponse.getResponseStatus()) {
                    case 0:
                        if (mUserResponse.getResponseData() != null) {
                            loginLabel.setVisibility(View.GONE);
                            loading.smoothToHide();
                            loading.setVisibility(View.GONE);
                            ivSuccess.setVisibility(View.VISIBLE);
                            openHomeActivity(mUserResponse.getResponseData());
                        }
                        else {
                            showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_log_in_good_credentials_but_failed)));
                        }
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_log_in_bad_credentials)));
                        break;
                    case 2:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_user_not_activated)));
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_log_in_failed)));
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_log_in_failed)));
            }
        }
    }
}
