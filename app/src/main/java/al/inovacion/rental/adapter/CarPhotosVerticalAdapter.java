package al.inovacion.rental.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.CheckRecyclerViewEvent;
import al.inovacion.rental.data.models.app.ImageObject;
import al.inovacion.rental.engine.RentalEngine;

/**
 * Created by Arbër Thaçi on 17-04-20.
 * Email: arberlthaci@gmail.com
 */

public class CarPhotosVerticalAdapter extends RecyclerView.Adapter<CarPhotosVerticalAdapter.CarPhotosViewHolder> {
    private ArrayList<ImageObject> itemsList = new ArrayList<>();
    private ArrayList<ImageObject> itemsPendingRemoval = new ArrayList<>();
    private HashMap<ImageObject, Runnable> pendingRunnables = new HashMap<>();

    private Context mContext;
    private Handler handler = new Handler();

    private static final int PENDING_REMOVAL_TIMEOUT = 3000;
    private int px;
    private int py;

    public class CarPhotosViewHolder extends RecyclerView.ViewHolder {
        LinearLayout undoRow;
        TextView undoBtn;

        LinearLayout normalRow;
        TextView tvPosition;
        ImageView ivCarPhoto;
        TextView tvTitle;
        ImageView ivStatus;

        public CarPhotosViewHolder(View view) {
            super(view);
            undoRow = (LinearLayout) view.findViewById(R.id.singleLYCImageUndo);
            undoBtn = (TextView) view.findViewById(R.id.undoBtn);

            normalRow = (LinearLayout) view.findViewById(R.id.singleLYCImageNormal);
            tvPosition = (TextView) view.findViewById(R.id.imagePosition);
            ivCarPhoto = (ImageView) view.findViewById(R.id.imagePhoto);
            tvTitle = (TextView) view.findViewById(R.id.imageTitle);
            ivStatus = (ImageView) view.findViewById(R.id.imageStatus);

            mContext = view.getContext();
            px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, mContext.getResources().getDisplayMetrics());
            py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, mContext.getResources().getDisplayMetrics());
        }
    }

    public CarPhotosVerticalAdapter(ArrayList<ImageObject> itemsList) {
        this.itemsList = itemsList;
    }

    @Override
    public CarPhotosVerticalAdapter.CarPhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_lyc_image, parent, false);
        return new CarPhotosVerticalAdapter.CarPhotosViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarPhotosVerticalAdapter.CarPhotosViewHolder holder, final int position) {
        final ImageObject imageObject = itemsList.get(position);

        if (itemsPendingRemoval.contains(imageObject)) {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_A700));
            holder.normalRow.setVisibility(View.GONE);
            holder.undoRow.setVisibility(View.VISIBLE);
            holder.undoBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Runnable pendingRemovalRunnable = pendingRunnables.get(imageObject);
                    pendingRunnables.remove(imageObject);
                    if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(imageObject);
                    notifyItemChanged(itemsList.indexOf(imageObject));
                }
            });
        }
        else {
            holder.itemView.setBackgroundColor(Color.WHITE);
            holder.normalRow.setVisibility(View.VISIBLE);
            holder.undoRow.setVisibility(View.GONE);
            holder.undoBtn.setOnClickListener(null);

            holder.tvPosition.setText(String.valueOf(position + 1)  + ".");
            Picasso.with(mContext)
                    .load(imageObject.getImageUri())
                    .resize(px, py)
                    .centerCrop()
                    .rotate(imageObject.getImageRotation())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.error_loading)
                    .into(holder.ivCarPhoto);
            holder.tvTitle.setText(imageObject.getImageTitle());
            switch (imageObject.getImageStatus()) {
                case RentalEngine.IMAGE_STATUS_NEUTRAL:
                    holder.ivStatus.setVisibility(View.GONE);
                    break;
                case RentalEngine.IMAGE_STATUS_SUCCESS:
                    holder.ivStatus.setVisibility(View.VISIBLE);
                    holder.ivStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_success));
                    break;
                case RentalEngine.IMAGE_STATUS_FAILED:
                    holder.ivStatus.setVisibility(View.VISIBLE);
                    holder.ivStatus.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_failed));
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if(itemsList != null && itemsList.size() != 0) {
            return itemsList.size();
        }
        return 0;
    }

    public void pendingRemoval(int position) {
        final ImageObject imageObject = itemsList.get(position);
        if (!itemsPendingRemoval.contains(imageObject)) {
            itemsPendingRemoval.add(imageObject);
            notifyItemChanged(position);
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(itemsList.indexOf(imageObject));
                }
            };
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(imageObject, pendingRemovalRunnable);
        }
    }

    public void add(ImageObject imageObject) {
        itemsList.add(imageObject);
        notifyItemInserted(itemsList.size()-1);
    }

    private void remove(int position) {
        ImageObject imageObject = itemsList.get(position);
        if (itemsPendingRemoval.contains(imageObject)) {
            itemsPendingRemoval.remove(imageObject);
        }
        if (itemsList.contains(imageObject)) {
            itemsList.remove(position);
            EventBus.getDefault().post(new CheckRecyclerViewEvent());
            notifyItemRemoved(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        if (position < itemsList.size()) {
            ImageObject imageObject = itemsList.get(position);
            return itemsPendingRemoval.contains(imageObject);
        }
        return false;
    }
}
