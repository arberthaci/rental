package al.inovacion.rental.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.CheckRecyclerViewEvent;
import al.inovacion.rental.data.event.DeleteCarPhotoEvent;
import al.inovacion.rental.data.event.OpenFrescoImageViewEvent;
import al.inovacion.rental.data.models.app.ImageObject;
import al.inovacion.rental.engine.RentalEngine;

import static android.R.string.ok;

/**
 * Created by Arbër Thaçi on 17-04-29.
 * Email: arberlthaci@gmail.com
 */

public class CarPhotosHorizontalAdapter extends RecyclerView.Adapter<CarPhotosHorizontalAdapter.CarPhotosHorizontalViewHolder> {
    private ArrayList<ImageObject> itemsList = new ArrayList<>();
    private Context mContext;

    private int px;
    private int py;

    public class CarPhotosHorizontalViewHolder extends RecyclerView.ViewHolder {
        LinearLayout row;
        ImageView deleteBtn;
        ImageView infoBtn;
        ImageView ivCarThumbnail;
        TextView tvCarPhotoName;
        ImageView imageStatus;

        CarPhotosHorizontalViewHolder(View view) {
            super(view);
            row = (LinearLayout) view.findViewById(R.id.singleThumbnail);
            deleteBtn = (ImageView) view.findViewById(R.id.ivDeletePhoto);
            infoBtn = (ImageView) view.findViewById(R.id.ivInfo);
            ivCarThumbnail = (ImageView) view.findViewById(R.id.carThumbnail);
            tvCarPhotoName = (TextView) view.findViewById(R.id.carPhotoName);
            imageStatus = (ImageView) view.findViewById(R.id.imageStatus);

            mContext = view.getContext();
            px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, mContext.getResources().getDisplayMetrics());
            py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 175, mContext.getResources().getDisplayMetrics());
        }
    }

    public CarPhotosHorizontalAdapter(ArrayList<ImageObject> itemsList) {
        this.itemsList = itemsList;
    }

    public void setItemsList(ArrayList<ImageObject> itemsList) {
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }

    public void add(ImageObject imageObject) {
        itemsList.add(imageObject);
        notifyItemInserted(itemsList.size()-1);
    }

    public void replace(int documentType, ImageObject imageObject) {
        int position = -1;
        for(int i=0; i<itemsList.size(); i++) {
            if (itemsList.get(i).getDocumentType() == documentType) {
                position = i;
                break;
            }
        }
        if (position != -1) {
            imageObject.setDocumentType(documentType);
            itemsList.set(position, imageObject);
            notifyItemChanged(position);
        }
        else {
            add(imageObject);
        }
    }

    public void remove(int position) {
        itemsList.remove(position);
        EventBus.getDefault().post(new CheckRecyclerViewEvent());
        notifyDataSetChanged();
    }

    @Override
    public CarPhotosHorizontalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_bc_car_photo, parent, false);
        return new CarPhotosHorizontalViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarPhotosHorizontalViewHolder holder, final int position) {
        final ImageObject imageObject = itemsList.get(position);

        holder.deleteBtn.setVisibility(View.GONE);
        holder.infoBtn.setVisibility(View.GONE);
        if (RentalEngine.getViewCarMode() == RentalEngine.USER_CARS && imageObject.getDocumentType() != RentalEngine.CAR_DOCUMENT_KOLAUDIMI && imageObject.getDocumentType() != RentalEngine.CAR_DOCUMENT_INSURANCE && imageObject.getDocumentType() != RentalEngine.CAR_DOCUMENT_LICENSE) {
            holder.deleteBtn.setVisibility(View.VISIBLE);
            holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imageObject.getImageStatus() == RentalEngine.IMAGE_STATUS_FAILED) {
                        remove(position);
                    }
                    else {
                        EventBus.getDefault().post(new DeleteCarPhotoEvent(imageObject.getId(), position));
                    }
                }
            });

            if (imageObject.getType() == RentalEngine.SERVER_IMAGE || (imageObject.getType() == RentalEngine.LOCAL_IMAGE && imageObject.getImageStatus() == RentalEngine.IMAGE_STATUS_SUCCESS)) {
                if (imageObject.getStatusId() != RentalEngine.APPROVED) {
                    holder.infoBtn.setVisibility(View.VISIBLE);
                    holder.infoBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showInfoDialog(imageObject.getStatusId());
                        }
                    });
                }
            }
        }

        if (imageObject.getType() == RentalEngine.LOCAL_IMAGE) {
            Picasso.with(mContext)
                    .load(imageObject.getImageUri())
                    .resize(px, py)
                    .centerCrop()
                    .rotate(imageObject.getImageRotation())
                    .placeholder(R.drawable.car_photo_not_found)
                    .error(R.drawable.car_photo_not_found)
                    .into(holder.ivCarThumbnail);
        }
        else if (imageObject.getType() == RentalEngine.SERVER_IMAGE) {
            Picasso.with(mContext)
                    .load(imageObject.getImageThumbnailPath())
                    .resize(px, py)
                    .centerCrop()
                    .placeholder(R.drawable.car_photo_not_found)
                    .error(R.drawable.car_photo_not_found)
                    .into(holder.ivCarThumbnail);
        }

        if (imageObject.getImageStatus() == RentalEngine.IMAGE_STATUS_FAILED) {
            holder.tvCarPhotoName.setVisibility(View.GONE);
            holder.imageStatus.setVisibility(View.VISIBLE);
        }
        else {
            holder.imageStatus.setVisibility(View.GONE);
            String photoName = imageObject.getImageTitle();
            if (imageObject.getType() == RentalEngine.SERVER_IMAGE) {
                photoName = photoName.substring(photoName.lastIndexOf('-') + 1);
            }
            holder.tvCarPhotoName.setVisibility(View.VISIBLE);
            holder.tvCarPhotoName.setText(photoName);
        }


        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new OpenFrescoImageViewEvent(itemsList, position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if(itemsList != null && itemsList.size() != 0) {
            return itemsList.size();
        }
        return 0;
    }

    private void showInfoDialog(int statusId) {
        String content = mContext.getResources().getString(R.string.bc_car_photo_info_label);
        switch (statusId) {
            case RentalEngine.WAITING_APPROVAL:
                content += mContext.getResources().getString(R.string.ds_waiting_approval);
                break;
            case RentalEngine.APPROVED:
                content += mContext.getResources().getString(R.string.ds_approved);
                break;
            case RentalEngine.REJECTED:
                content += mContext.getResources().getString(R.string.ds_rejected);
                break;
            default:
                content = mContext.getResources().getString(R.string.ds_car_photos_not_confirmed);
                break;
        }
        new MaterialDialog.Builder(mContext)
                .title(R.string.app_name)
                .content(content)
                .positiveText(ok)
                .show();
    }
}
