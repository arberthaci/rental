package al.inovacion.rental.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.OpenViewCarEvent;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.engine.RentalEngine;

/**
 * Created by Arbër Thaçi on 17-04-23.
 * Email: arberlthaci@gmail.com
 */

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.CarViewHolder> {
    private Context mContext;

    private ArrayList<Car> itemsList = new ArrayList<>();
    private int mode;
    private int px;
    private int py;

    public class CarViewHolder extends RecyclerView.ViewHolder {
        LinearLayout row;
        TextView tvCarPrice;
        LinearLayout llCarNameDashboard;
        TextView tvCarNameDashboard;
        LinearLayout actionBtn;
        ImageView actionIcon;
        LinearLayout inactiveInfo;
        ImageView ivCarPhoto;
        TextView tvCarMainInfo;
        LinearLayout llCarDetailedInfo;
        TextView tvCarAddress;
        TextView tvCarAvailability;
        TextView tvCarBookings;
        TextView tvCarRating;

        CarViewHolder(View view) {
            super(view);
            row = (LinearLayout) view.findViewById(R.id.singleCar);
            tvCarPrice = (TextView) view.findViewById(R.id.carPrice);
            llCarNameDashboard = (LinearLayout) view.findViewById(R.id.llCarNameDashboard);
            tvCarNameDashboard = (TextView) view.findViewById(R.id.tvCarNameDashboard);
            actionBtn = (LinearLayout) view.findViewById(R.id.actionBtn);
            actionIcon = (ImageView) view.findViewById(R.id.actionIcon);
            inactiveInfo = (LinearLayout) view.findViewById(R.id.inactiveInfo);
            ivCarPhoto = (ImageView) view.findViewById(R.id.carPhoto);
            tvCarMainInfo = (TextView) view.findViewById(R.id.carMainInfo);
            llCarDetailedInfo = (LinearLayout) view.findViewById(R.id.carDetailedInfo);
            tvCarAddress = (TextView) view.findViewById(R.id.carAddress);
            tvCarAvailability = (TextView) view.findViewById(R.id.carAvailability);
            tvCarBookings = (TextView) view.findViewById(R.id.carBookings);
            tvCarRating = (TextView) view.findViewById(R.id.carRating);

            mContext = view.getContext();
            px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 400, mContext.getResources().getDisplayMetrics());
            py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, mContext.getResources().getDisplayMetrics());
        }
    }

    public CarsAdapter(ArrayList<Car> itemsList, int mode) {
        this.itemsList = itemsList;
        this.mode = mode;
    }

    public void setItemsList(ArrayList<Car> itemsList) {
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }

    public void add(Car mCar) {
        itemsList.add(mCar);
        notifyDataSetChanged();
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_bc_car, parent, false);
        return new CarViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarViewHolder holder, final int position) {
        final Car carObject = itemsList.get(position);

        String manufacturer = carObject.getManufacturer();
        String model = carObject.getModel();
        String year = carObject.getProductionYear();

        if (mode == RentalEngine.CARS_TO_BOOK) {
            holder.llCarNameDashboard.setVisibility(View.GONE);
            holder.tvCarPrice.setVisibility(View.VISIBLE);
            holder.tvCarPrice.setText(carObject.getDailyPrice() + " " + mContext.getResources().getString(R.string.bc_price_unit_label));
            holder.actionBtn.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_700));
            holder.actionIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fire));
        }
        else if (mode == RentalEngine.USER_CARS) {
            if (carObject.isActive()) {
                holder.inactiveInfo.setVisibility(View.GONE);
            }
            else {
                holder.inactiveInfo.setVisibility(View.VISIBLE);
            }
            holder.tvCarPrice.setVisibility(View.GONE);
            holder.llCarNameDashboard.setVisibility(View.VISIBLE);
            holder.actionBtn.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
            holder.actionIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_edit_1));
            holder.tvCarNameDashboard.setText(manufacturer + " " + model + " " + year);
        }

        String carPhotoUrl = "error";
        if (carObject.getPhotos() != null && carObject.getPhotos().size() > 0) {
            carPhotoUrl = carObject.getPhotos().get(0).getPath();
        }
        Picasso.with(mContext)
                .load(carPhotoUrl)
                .resize(px, py)
                .centerCrop()
                .placeholder(R.drawable.car_photo_not_found)
                .error(R.drawable.car_photo_not_found)
                .into(holder.ivCarPhoto);

        if (mode == RentalEngine.CARS_TO_BOOK) {
            holder.tvCarMainInfo.setVisibility(View.VISIBLE);
            holder.llCarDetailedInfo.setVisibility(View.VISIBLE);

            holder.tvCarMainInfo.setText(manufacturer + " " + model + " " + year);
            holder.tvCarAddress.setText(carObject.getAddress());
            holder.tvCarAvailability.setText("15 Maj");
            holder.tvCarBookings.setText("4 here");
            holder.tvCarRating.setText("3.7");
        }
        else if (mode == RentalEngine.USER_CARS) {
            holder.tvCarMainInfo.setVisibility(View.GONE);
            holder.llCarDetailedInfo.setVisibility(View.GONE);
        }

        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new OpenViewCarEvent(carObject, mode));
            }
        });
    }

    @Override
    public int getItemCount() {
        if(itemsList != null && itemsList.size() != 0) {
            return itemsList.size();
        }
        return 0;
    }
}