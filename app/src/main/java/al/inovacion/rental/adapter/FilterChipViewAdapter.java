package al.inovacion.rental.adapter;


import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.plumillonforge.android.chipview.ChipViewAdapter;

import al.inovacion.rental.R;
import al.inovacion.rental.data.models.app.Filter;

/**
 * Created by Arbër Thaçi on 17-05-10.
 * Email: arberlthaci@gmail.com
 */

public class FilterChipViewAdapter extends ChipViewAdapter {

    public FilterChipViewAdapter(Context context) {
        super(context);
    }

    @Override
    public int getLayoutRes(int position) {
        return R.layout.single_item_filter;
    }

    @Override
    public int getBackgroundColor(int position) {
        return 0;
    }

    @Override
    public int getBackgroundColorSelected(int position) {
        return 0;
    }

    @Override
    public int getBackgroundRes(int position) {
        return 0;
    }

    @Override
    public void onLayout(View view, int position) {
        Filter filter = (Filter) getChip(position);
        ((TextView) view.findViewById(android.R.id.text1)).setTextColor(getColor(R.color.colorPrimaryDark));
        ((TextView) view.findViewById(android.R.id.text1)).setText(filter.getText());
    }
}
