package al.inovacion.rental.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.data.models.api.Availability;
import al.inovacion.rental.engine.RentalEngine;

/**
 * Created by Arbër Thaçi on 17-05-21.
 * Email: arberlthaci@gmail.com
 */

public class AvailabilitiesAdapter extends RecyclerView.Adapter<AvailabilitiesAdapter.AvailabilitiesViewHolder> {
    private ArrayList<Availability> itemsList = new ArrayList<>();
    private Context mContext;

    public class AvailabilitiesViewHolder extends RecyclerView.ViewHolder {
        LinearLayout row;
        //ImageView deleteBtn;
        TextView tvStartDate;
        TextView tvStartTime;
        TextView tvEndDate;
        TextView tvEndTime;

        AvailabilitiesViewHolder(View view) {
            super(view);
            row = (LinearLayout) view.findViewById(R.id.singleAvailability);
            //deleteBtn = (ImageView) view.findViewById(R.id.ivDeletePhoto);
            tvStartDate = (TextView) view.findViewById(R.id.availabilityStartDate);
            tvStartTime = (TextView) view.findViewById(R.id.availabilityStartTime);
            tvEndDate = (TextView) view.findViewById(R.id.availabilityEndDate);
            tvEndTime = (TextView) view.findViewById(R.id.availabilityEndTime);

            mContext = view.getContext();
        }
    }

    public AvailabilitiesAdapter(ArrayList<Availability> itemsList) {
        this.itemsList = itemsList;
    }

    public void setItemsList(ArrayList<Availability> itemsList) {
        this.itemsList = itemsList;
        notifyDataSetChanged();
    }

    public void add(Availability availabilityObject) {
        itemsList.add(availabilityObject);
        notifyItemInserted(itemsList.size()-1);
    }

    /*public void remove(int position) {
        itemsList.remove(position);
        EventBus.getDefault().post(new CheckRecyclerViewEvent());
        notifyDataSetChanged();
    }*/

    @Override
    public AvailabilitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_bc_availability, parent, false);
        return new AvailabilitiesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AvailabilitiesViewHolder holder, final int position) {
        final Availability availabilityObject = itemsList.get(position);

        DateTime startDate = new DateTime(availabilityObject.getStartDate());
        DateTime endDate = new DateTime(availabilityObject.getEndDate());

        holder.tvStartDate.setText(startDate.getDayOfMonth() + " " + RentalEngine.getMonth(startDate.getMonthOfYear()-1) + " " + startDate.getYear());
        holder.tvStartTime.setText(String.format(Locale.ENGLISH,"%02d", startDate.getHourOfDay()) + ":" + String.format(Locale.ENGLISH,"%02d", startDate.getMinuteOfHour()));
        holder.tvEndDate.setText(endDate.getDayOfMonth() + " " + RentalEngine.getMonth(endDate.getMonthOfYear()-1) + " " + endDate.getYear());
        holder.tvEndTime.setText(String.format(Locale.ENGLISH,"%02d", endDate.getHourOfDay()) + ":" + String.format(Locale.ENGLISH,"%02d", endDate.getMinuteOfHour()));
    }

    @Override
    public int getItemCount() {
        if(itemsList != null && itemsList.size() != 0) {
            return itemsList.size();
        }
        return 0;
    }
}