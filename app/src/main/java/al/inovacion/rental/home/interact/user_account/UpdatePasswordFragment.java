package al.inovacion.rental.home.interact.user_account;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.BackButtonEvent;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.UserResponse;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Arbër Thaçi on 17-04-17.
 * Email: arberlthaci@gmail.com
 */

public class UpdatePasswordFragment extends Fragment implements View.OnClickListener {

    private EditText oldPasswordField;
    private EditText newPasswordField;
    private EditText confirmPasswordField;
    private CircularProgressButton updateBtn;

    String oldPassword = "";
    String newPassword = "";
    String confirmPassword = "";

    public UpdatePasswordFragment() {}

    public static UpdatePasswordFragment newInstance() {
        return new UpdatePasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.UPDATE_PASSWORD_FRAGMENT_TAG, getResources().getString(R.string.account_toolbar_update_password)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().post(new CloseKeyboardEvent());
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cpbUpdatePasswordUpdate:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                tryToUpdatePassword();
                break;
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            updateBtn.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        oldPasswordField = (EditText) view.findViewById(R.id.etAccountOldPassword);
        newPasswordField = (EditText) view.findViewById(R.id.etAccountNewPassword);
        confirmPasswordField = (EditText) view.findViewById(R.id.etAccountConfirmPassword);
        updateBtn = (CircularProgressButton) view.findViewById(R.id.cpbUpdatePasswordUpdate);
    }

    private void afterInitialization() {
        updateBtn.setIndeterminateProgressMode(true);
        updateBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        updateBtn.setOnClickListener(this);

        ViewTreeObserver vtoUpload = updateBtn.getViewTreeObserver();
        vtoUpload.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    updateBtn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    updateBtn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    private void callOnSuccessCPB() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "UpdatePasswordFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateBtn.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                            updateBtn.setAnimationListener(onAnimationEndListener);
                            updateBtn.setEnabled(false);
                            closeUpdatePasswordFragment();
                        }
                    });
                }
            }
        };
        timer.start();
    }

    private void closeUpdatePasswordFragment() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "UpdatePasswordFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new BackButtonEvent());
                        }
                    });
                }
            }
        };
        timer.start();
    }

    private void showErrorMessage(String errorMessage) {
        final String message = errorMessage;
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(750);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "UploadPasswordFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateBtn.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
                            updateBtn.setEnabled(true);
                            EventBus.getDefault().post(new ShowSnackBarMessageEvent(message, false));
                        }
                    });
                }
            }
        };
        timer.start();
    }

    private boolean areFieldsFilled() {
        if (!oldPassword.equalsIgnoreCase("") && !newPassword.equalsIgnoreCase("") && !confirmPassword.equalsIgnoreCase("")) {
            return true;
        }
        if (oldPassword.equalsIgnoreCase("")) {
            playYoyo(oldPasswordField);
        }
        if (newPassword.equalsIgnoreCase("")) {
            playYoyo(newPasswordField);
        }
        if (confirmPassword.equalsIgnoreCase("")) {
            playYoyo(confirmPasswordField);
        }
        return false;
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private void tryToUpdatePassword() {
        oldPassword = oldPasswordField.getText().toString();
        newPassword = newPasswordField.getText().toString();
        confirmPassword = confirmPasswordField.getText().toString();

        if (areFieldsFilled()) {
            updateBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            updateBtn.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);

            if (RentalEngine.isNetworkAvailable(getActivity())) {
                new UpdatePassword().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_USERS_ROUTE + "/" + RentalEngine.getUser().getId() + RentalEngine.RENTAL_API_UPDATE_PASSWORD_ROUTE);
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    public InputStream getUpdatePasswordInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("old_password", oldPassword)
                    .addFormDataPart("password", newPassword)
                    .addFormDataPart("password_confirmation", confirmPassword)
                    .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UpdatePassword extends AsyncTask<String, String, String> {
        private UserResponse mUserResponse;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUpdatePasswordInputStream(uri[0]);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mUserResponse = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "UploadPasswordFragment UpdatePassword doInBackground Exception: " + e.toString());
                mUserResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mUserResponse != null) {
                switch (mUserResponse.getResponseStatus()) {
                    case 0:
                        if (mUserResponse.getResponseData() != null) {
                            callOnSuccessCPB();
                        }
                        else {
                            showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
                        }
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_password_failed_due_to_wrong_old_password)));
                        break;
                    case 2:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_passwords_do_not_match)));
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
            }
        }
    }
}
