package al.inovacion.rental.home.interact.list_your_car;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.OpenViewCarEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.data.models.api.CarResponse;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.string.ok;

/**
 * Created by Arbër Thaçi on 17-04-25.
 * Email: arberlthaci@gmail.com
 */

public class ListYourCarFifthFragment extends Fragment implements View.OnClickListener {

    private EditText priceField;
    private ImageView ivInfo;
    private TextView warrantyField;
    private CheckBox cbSubscribe;
    private CheckBox cbAgree;
    private CircularProgressButton finishButton;

    private int carId = -1;
    private String dailyPrice = "";
    private String warrantyPrice = "";
    private Car mCar;

    public ListYourCarFifthFragment() {}

    public static ListYourCarFifthFragment newInstance() {
        return new ListYourCarFifthFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.LIST_YOUR_CAR_FIFTH_STEP_FRAGMENT_TAG, getResources().getString(R.string.lyc_toolbar_fifth_step)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_your_car_fifth, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLYCFSWarrantyInfo:
                hideSoftKeyboard(getActivity());
                showWarrantyInfo();
                break;
            case R.id.cpbLYCFSFinish:
                hideSoftKeyboard(getActivity());
                tryToFinishListing();
                break;
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            finishButton.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        priceField = (EditText) view.findViewById(R.id.etLYCFSPrice);
        ivInfo = (ImageView) view.findViewById(R.id.ivLYCFSWarrantyInfo);
        warrantyField = (TextView) view.findViewById(R.id.tvLYCFSWarranty);
        cbSubscribe = (CheckBox) view.findViewById(R.id.lycCBSubscribe);
        cbAgree = (CheckBox) view.findViewById(R.id.lycCBAgree);
        finishButton = (CircularProgressButton) view.findViewById(R.id.cpbLYCFSFinish);
    }

    private void afterInitialization() {
        priceField.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try {
                    if (s.toString().equalsIgnoreCase("")) {
                        warrantyField.setText(getResources().getString(R.string.lyc_warranty_label));
                    }
                    else {
                        int dailyPrice = Integer.parseInt(s.toString());
                        warrantyField.setText(String.valueOf(dailyPrice * (RentalEngine.WARRANTY_PERCANTAGE / 100)));
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }
        });

        ivInfo.setOnClickListener(this);
        finishButton.setIndeterminateProgressMode(true);
        finishButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        finishButton.setOnClickListener(this);

        ViewTreeObserver vto = finishButton.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    finishButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    finishButton.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    private void showWarrantyInfo() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.app_name)
                .content(R.string.lyc_warranty_formula)
                .positiveText(ok)
                .show();
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        priceField.clearFocus();
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private boolean areFieldsFilled() {
        if (!dailyPrice.equalsIgnoreCase("") && cbAgree.isChecked() /*&& startYear != -1 && endYear != -1*/) {
            return true;
        }
        if (dailyPrice.equalsIgnoreCase("")) {
            playYoyo(priceField);
        }
        if (!cbAgree.isChecked()) {
            playYoyo(cbAgree);
        }
        return false;
    }

    private void showErrorMessage(String errorMessage) {
        finishButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        finishButton.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private void tryToFinishListing() {
        dailyPrice = priceField.getText().toString();
        warrantyPrice = warrantyField.getText().toString();
        if (areFieldsFilled()) {
            finishButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            finishButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
            if (warrantyPrice.equalsIgnoreCase(getResources().getString(R.string.lyc_warranty_label))) {
                warrantyPrice = "0";
            }

            if (RentalEngine.isNetworkAvailable(getActivity())) {
                callUploadCarDataApi();
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(750);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFifthFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
                                    //enableActionsButton(true);
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    private void callOnSuccessCPB() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(750);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFifthFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finishButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                            finishButton.setAnimationListener(onAnimationEndListener);
                            finishButton.setEnabled(false);
                            showSuccessInfo();
                        }
                    });
                }
            }
        };
        timer.start();
    }

    private void showSuccessInfo() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.app_name)
                .content(R.string.lyc_car_created_successfully)
                .positiveText(R.string.account_confirm_option)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        openViewYourCarFragment(mCar);
                    }
                })
                .show();
    }

    private void openViewYourCarFragment(final Car carObject) {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(1250);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFifthFragment thread Exception: " + e.toString());
                }
                finally {
                    EventBus.getDefault().post(new OpenViewCarEvent(carObject, RentalEngine.USER_CARS));
                }
            }
        };
        timer.start();
    }

    private void callUploadCarDataApi() {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + carId;
        new UploadCar().execute(urlApi);
    }

    public InputStream getUploadCarDataInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = new FormBody.Builder()
                    .addEncoded("daily_price", String.valueOf(dailyPrice))
                    .addEncoded("warranty_price", String.valueOf(warrantyPrice))
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .put(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UploadCar extends AsyncTask<String, String, String> {
        private CarResponse mCarResponse;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUploadCarDataInputStream(uri[0]);
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                Gson gson = gsonBuilder.create();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarResponse = gson.fromJson(reader, CarResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFifthFragment UploadCar doInBackground Exception: " + e.toString());
                mCarResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mCarResponse != null && mCarResponse.getResponseStatus() == 0) {
                mCar = mCarResponse.getResponseData();
                callOnSuccessCPB();
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
            }
        }
    }
}