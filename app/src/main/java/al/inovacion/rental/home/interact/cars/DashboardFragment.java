package al.inovacion.rental.home.interact.cars;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;

import al.inovacion.rental.R;
import al.inovacion.rental.adapter.CarsAdapter;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.StopRefreshingEvent;
import al.inovacion.rental.data.event.SwipeToRefreshEvent;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.data.models.app.HttpResponse;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.home.interact.list_your_car.ListYourCarFragment;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Arbër Thaçi on 17-03-24.
 * Email: arberlthaci@gmail.com
 */

public class DashboardFragment extends Fragment implements View.OnClickListener {

    private LinearLayout noInternetView;
    private LinearLayout noDataView;
    private LinearLayout errorView;
    private AVLoadingIndicatorView loading;
    private RecyclerView rvCars;
    private FloatingActionButton fab;

    private CarsAdapter adapter;
    private ArrayList<Car> carsList = new ArrayList<>();

    private static final int NO_INTERNET_MODE = 1;
    private static final int NO_DATA_MODE = 2;
    private static final int ERROR_MODE = 3;
    private static final int DATA_AVAILABLE = 4;

    public DashboardFragment() {}

    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.DASHBOARD_FRAGMENT_TAG, getResources().getString(R.string.menu_item_dashboard)));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().post(new CloseKeyboardEvent());
        initializeComponents(view);
        afterInitialization();

        if (RentalEngine.isNetworkAvailable(getActivity())) {
            callGetUserCars();
        } else {
            loading.smoothToHide();
            hideViews(NO_INTERNET_MODE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabListYourCar:
                EventBus.getDefault().post(new OpenNewFragmentEvent(new ListYourCarFragment(), RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG));
        }
    }

    private void initializeComponents(View view) {
        noDataView = (LinearLayout) view.findViewById(R.id.llNoData);
        noInternetView = (LinearLayout) view.findViewById(R.id.llNoInternet);
        errorView = (LinearLayout) view.findViewById(R.id.llError);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.dLoading);
        rvCars = (RecyclerView) view.findViewById(R.id.rvDCars);
        fab = (FloatingActionButton) view.findViewById(R.id.fabListYourCar);
    }

    private void afterInitialization() {
        hideViews(-1);

        fab.setOnClickListener(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCars.setLayoutManager(mLayoutManager);
        rvCars.setItemAnimator(new DefaultItemAnimator());
        rvCars.setNestedScrollingEnabled(false);
        rvCars.setHasFixedSize(false);

        adapter = new CarsAdapter(carsList, RentalEngine.USER_CARS);
        rvCars.setAdapter(adapter);
    }

    private void callGetUserCars() {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_USERS_ROUTE + "/" + RentalEngine.getUser().getId() + RentalEngine.RENTAL_API_CARS_ROUTE;
        new HttpGetCarsAsync().execute(urlApi);
    }

    private void hideViews(int mode) {
        rvCars.setVisibility(View.GONE);
        noDataView.setVisibility(View.GONE);
        noInternetView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        fab.setVisibility(View.GONE);

        if (mode == NO_INTERNET_MODE) {
            noInternetView.setVisibility(View.VISIBLE);
        }
        else if (mode == NO_DATA_MODE) {
            noDataView.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }
        else if (mode == ERROR_MODE) {
            errorView.setVisibility(View.VISIBLE);
        }
        else if (mode == DATA_AVAILABLE) {
            rvCars.setVisibility(View.VISIBLE);
        }
    }

    private void fillCarList() {
        adapter.setItemsList(carsList);
        loading.smoothToHide();
        hideViews(DATA_AVAILABLE);
    }

    private void refreshRecyclerView() {
        if (RentalEngine.isNetworkAvailable(getActivity())) {
            if (rvCars.getVisibility() == View.GONE) {
                hideViews(-1);
                loading.smoothToShow();
            }
            callGetUserCars();
        }
        else {
            loading.smoothToShow();
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(1000);
                    }
                    catch (InterruptedException e) {
                        Log.i(RentalEngine.RENTAL_LOG_TAG, "DashboardFragment Swipe thread Exception: " + e.toString());
                    }
                    finally {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new StopRefreshingEvent());
                                loading.smoothToHide();
                                noInternetView.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            };
            timer.start();
            hideViews(-1);
        }
    }

    private class HttpGetCarsAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private ArrayList<Car> tempCars;

        @Override
        protected String doInBackground(String... uri) {
            try {
                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                    Gson gson = gsonBuilder.create();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    Type collectionType = new TypeToken<Collection<Car>>() {}.getType();
                    tempCars = gson.fromJson(reader, collectionType);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "DashboardFragment HttpGetCarsAsync doInBackground Exception: " + e.toString());
                tempCars = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            EventBus.getDefault().post(new StopRefreshingEvent());
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (tempCars != null && tempCars.size() != 0 && adapter != null) {
                    carsList = tempCars;
                    fillCarList();
                }
                else {
                    hideViews(NO_DATA_MODE);
                    loading.smoothToHide();
                }
            }
            else {
                hideViews(ERROR_MODE);
                loading.smoothToHide();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(SwipeToRefreshEvent event) {
        if (event != null) {
            refreshRecyclerView();
        }
    }
}
