package al.inovacion.rental.home.interact.cars;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.stfalcon.frescoimageviewer.ImageViewer;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.adapter.AvailabilitiesAdapter;
import al.inovacion.rental.adapter.CarPhotosHorizontalAdapter;
import al.inovacion.rental.data.event.CheckRecyclerViewEvent;
import al.inovacion.rental.data.event.DeleteCarPhotoEvent;
import al.inovacion.rental.data.event.OpenFrescoImageViewEvent;
import al.inovacion.rental.data.event.RefreshToolbarImageEvent;
import al.inovacion.rental.data.event.SetDefaultCarImageEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.Availability;
import al.inovacion.rental.data.models.api.CarDocument;
import al.inovacion.rental.data.models.app.DocumentInfo;
import al.inovacion.rental.data.models.app.HttpResponse;
import al.inovacion.rental.data.models.app.ImageObject;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.data.models.api.CarPhoto;
import al.inovacion.rental.data.models.api.CarPhotoResponse;
import al.inovacion.rental.data.models.api.CarResponse;
import al.inovacion.rental.data.models.api.City;
import al.inovacion.rental.engine.Mail;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import al.inovacion.rental.home.HomeActivity;
import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Arbër Thaçi on 17-04-27.
 * Email: arberlthaci@gmail.com
 */

public class ViewCarFragment extends Fragment implements View.OnClickListener {

    private AVLoadingIndicatorView loading;
    private LinearLayout llCarData;
    private TextView tvManufacturer;
    private TextView tvModel;
    private TextView tvProductionYear;
    private TextView tvOdometer;
    private TextView tvTransmission;
    private TextView tvBodyType;
    private TextView tvAirConditioner;
    private TextView tvAddress;
    private LinearLayout llChangeAddress;
    private ImageView ivChangeAddress;
    private AVLoadingIndicatorView loadingAddress;
    private TextView tvDailyPrice;
    private LinearLayout llDailyPrice;
    private ImageView ivChangeDailyPrice;
    private AVLoadingIndicatorView loadingDailyPrice;
    private TextView tvWarrantyPrice;
    private LinearLayout llWarrantyPrice;
    private ImageView ivChangeWarrantyPrice;
    private AVLoadingIndicatorView loadingWarrantyPrice;
    private TextView tvActiveStatus;
    private LinearLayout llActiveStatus;
    private ImageView ivChangeActiveStatus;
    private AVLoadingIndicatorView loadingActiveStatus;
    /*private CardView ownerData;
    private TextView tvFirstName;
    private TextView tvLastName;*/
    private TextView tvViewProfileBtn;
    private TextView tvCarPhotosInfo;
    private ImageView ivVCAddCarPhotos;
    private RecyclerView rvCarPhotos;
    private CardView carDocumentation;
    private TextView tvCarDocumentationInfo;
    private ImageView ivVCInfoCarDocuments;
    private ImageView ivVCAddCarDocumentation;
    private RecyclerView rvVCCarDocuments;
    private CardView carAvailabilities;
    private TextView tvAvailabilitiesInfo;
    private ImageView ivVCAddAvailability;
    private RecyclerView rvVCAvailabilities;
    private LinearLayout llAddAvailabilityInfo;
    private TextView tvAddAvailabilityInfo;
    private TextView tvStartDate;
    private TextView tvEndDate;
    private View positive;
    private MaterialDialog progressDialog;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private CircularProgressButton bookButton;

    private int mode = -1;
    private int selectedDocumentType = -1;
    private CarPhotosHorizontalAdapter photosAdapter;
    private CarPhotosHorizontalAdapter documentationAdapter;
    private AvailabilitiesAdapter availabilitiesAdapter;
    private ArrayList<String> mAddresses = new ArrayList<>();
    private ArrayList<ImageObject> photoImages = new ArrayList<>();
    private ArrayList<ImageObject> documentationImages = new ArrayList<>();
    private ArrayList<DocumentInfo> documentsInfo = new ArrayList<>();
    private ArrayList<Availability> availabilitiesList = new ArrayList<>();
    private Car selectedCar;
    //private User carOwner;
    private Uri mUri;
    private String mFileName = "";
    private String mPath = "";
    private Uri mDocUri;
    private String mDocFileName = "";
    private String mDocPath = "";
    private int startDay = -1;
    private int startMonth = -1;
    private int startYear = -1;
    private int startHour = -1;
    private int startMinute = -1;
    private DateTime dtStartDate;
    private DateTime dtEndDate;
    private String startDate = "";
    private String endDate = "";
    private boolean isStartDateSet = false;
    private boolean isEndDateSet = false;
    private static final String ADDRESS_CHANGED = "address";
    private static final String DAILY_PRICE_CHANGED = "daily_price";
    private static final String WARRANTY_PRICE_CHANGED = "warranty_price";
    private static final String DOCUMENT_CHANGED = "car_docs";
    private static final String AVAILABILITY_ADDED = "car_availability";

    public ViewCarFragment() {}

    public static ViewCarFragment newInstance() {
        return new ViewCarFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.VIEW_CAR_FRAGMENT_TAG, getResources().getString(R.string.bc_toolbar_second_step)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_car, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivVCChangeAddress:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    openChangeAddressDialog();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            case R.id.ivVCChangeDailyPrice:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    openChangeDailyPriceDialog();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            case R.id.ivVCChangeWarrantyPrice:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    openChangeWarrantyPriceDialog();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            case R.id.ivVCChangeActiveStatus:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    openChangeActiveStatusDialog();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            /*case R.id.tvVCOwnerViewProfileBtn:
                if (RentalEngine.getCarOwner() != null) {
                    CarOwnerFragment carOwnerFragment = new CarOwnerFragment();
                    EventBus.getDefault().post(new OpenNewFragmentEvent(carOwnerFragment, RentalEngine.CAR_OWNER_FRAGMENT_TAG));
                }
                break;*/
            case R.id.ivVCAddCarPhotos:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    selectedDocumentType = -1;
                    showDialogToAddPhotos();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            case R.id.ivVCInfoCarDocuments:
                showDocumentsInfo();
                break;
            case R.id.ivVCAddCarDocuments:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    showChooseDocumentType();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            case R.id.ivVCAddAvailability:
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    isStartDateSet = false;
                    isEndDateSet = false;
                    startDay = -1;
                    startMonth = -1;
                    startYear = -1;
                    startHour = -1;
                    startMinute = -1;
                    showAddAvailabilityDialog();
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
                break;
            case R.id.ivAddAvailabilityStartDate:
                showAvailabilityDatePicker(true);
                break;
            case R.id.ivAddAvailabilityEndDate:
                showAvailabilityDatePicker(false);
                break;
            case R.id.cpbVCBook:
                //new SendEmail().execute();
                tryToBookTheCar();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeAPicture();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            int totalSelectedImages = 1;
            switch (requestCode) {
                case RentalEngine.PICK_IMAGE_REQUEST:
                    if (data != null) {
                        if (data.getData() != null) {
                            if (selectedDocumentType == -1) {
                                if (!hasLimitExceed(totalSelectedImages)) {
                                    mUri = data.getData();
                                    mPath = RentalEngine.getRealPathFromURI(getContext(), mUri);
                                    mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                                    float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                                    prepareProgressDialog(1);
                                    photosAdapter.add(new ImageObject(mUri, mPath, mFileName, tempRotation));
                                    new UploadCarImage().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + selectedCar.getId() + RentalEngine.RENTAL_API_ADD_CAR_PHOTO_ROUTE, String.valueOf(photoImages.size()-1));
                                }
                            }
                            else {
                                mDocUri = data.getData();
                                mDocPath = RentalEngine.getRealPathFromURI(getContext(), mDocUri);
                                mDocFileName = mDocPath.substring(mDocPath.lastIndexOf("/")+1);
                                float tempDocRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mDocUri);

                                prepareProgressDialog(1);
                                documentationAdapter.replace(selectedDocumentType, new ImageObject(mDocUri, mDocPath, mDocFileName, tempDocRotation));
                                callUploadCarDataApi(DOCUMENT_CHANGED, getDocumentTypeParam(selectedDocumentType));
                            }
                        }
                        else if (data.getClipData() != null) {
                            totalSelectedImages = data.getClipData().getItemCount();
                            if (!hasLimitExceed(totalSelectedImages)) {
                                prepareProgressDialog(totalSelectedImages);
                                for (int i = 0; i < totalSelectedImages; i++) {
                                    ClipData.Item mItem = data.getClipData().getItemAt(i);
                                    mUri = mItem.getUri();
                                    mPath = RentalEngine.getRealPathFromURI(getContext(), mUri);
                                    mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                                    float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                                    photosAdapter.add(new ImageObject(mUri, mPath, mFileName, tempRotation));
                                    new UploadCarImage().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + selectedCar.getId() + RentalEngine.RENTAL_API_ADD_CAR_PHOTO_ROUTE, String.valueOf(photoImages.size()-1));
                                }
                            }
                        }
                    }
                    break;
                case RentalEngine.REQUEST_IMAGE_CAPTURE:
                    if (selectedDocumentType == -1) {
                        if (!hasLimitExceed(totalSelectedImages)) {
                            mPath = mUri.getPath();
                            float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                            prepareProgressDialog(1);
                            photosAdapter.add(new ImageObject(mUri, mPath, mFileName, tempRotation));
                            new UploadCarImage().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + selectedCar.getId() + RentalEngine.RENTAL_API_ADD_CAR_PHOTO_ROUTE, String.valueOf(photoImages.size() - 1));
                        }
                    }
                    else {
                        mDocPath = mDocUri.getPath();
                        float tempDocRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mDocUri);

                        prepareProgressDialog(1);
                        documentationAdapter.replace(selectedDocumentType, new ImageObject(mDocUri, mDocPath, mDocFileName, tempDocRotation));
                        callUploadCarDataApi(DOCUMENT_CHANGED, getDocumentTypeParam(selectedDocumentType));

                    }
                    break;
            }
            checkCarPhotoCardView();
            ((HomeActivity) getActivity()).setOnActivityResult(true);
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            bookButton.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.vcLoading);
        llCarData = (LinearLayout) view.findViewById(R.id.llCarData);
        tvManufacturer = (TextView) view.findViewById(R.id.tvVCManufacturerValue);
        tvModel = (TextView) view.findViewById(R.id.tvVCModelValue);
        tvProductionYear = (TextView) view.findViewById(R.id.tvVCProductionYearValue);
        tvOdometer = (TextView) view.findViewById(R.id.tvVCOdometerValue);
        tvTransmission = (TextView) view.findViewById(R.id.tvVCTransmissionValue);
        tvBodyType = (TextView) view.findViewById(R.id.tvVCBodyTypeValue);
        tvAirConditioner = (TextView) view.findViewById(R.id.tvVCAirConditionerValue);
        tvAddress = (TextView) view.findViewById(R.id.tvVCAddressValue);
        llChangeAddress = (LinearLayout) view.findViewById(R.id.llVCChangeAddress);
        ivChangeAddress = (ImageView) view.findViewById(R.id.ivVCChangeAddress);
        loadingAddress = (AVLoadingIndicatorView) view.findViewById(R.id.vcLoadingAddress);
        llDailyPrice = (LinearLayout) view.findViewById(R.id.llVCChangeDailyPrice);
        tvDailyPrice = (TextView) view.findViewById(R.id.tvVCDailyPriceValue);
        ivChangeDailyPrice = (ImageView) view.findViewById(R.id.ivVCChangeDailyPrice);
        loadingDailyPrice = (AVLoadingIndicatorView) view.findViewById(R.id.vcLoadingDailyPrice);
        llWarrantyPrice = (LinearLayout) view.findViewById(R.id.llVCChangeWarrantyPrice);
        tvWarrantyPrice = (TextView) view.findViewById(R.id.tvVCWarrantyPriceValue);
        ivChangeWarrantyPrice = (ImageView) view.findViewById(R.id.ivVCChangeWarrantyPrice);
        loadingWarrantyPrice = (AVLoadingIndicatorView) view.findViewById(R.id.vcLoadingWarrantyPrice);
        llActiveStatus = (LinearLayout) view.findViewById(R.id.llVCActiveStatus);
        tvActiveStatus = (TextView) view.findViewById(R.id.tvVCActiveStatusValue);
        ivChangeActiveStatus = (ImageView) view.findViewById(R.id.ivVCChangeActiveStatus);
        loadingActiveStatus = (AVLoadingIndicatorView) view.findViewById(R.id.vcLoadingActiveStatus);
        /*ownerData = (CardView) view.findViewById(R.id.cvVCOwnerData);
        tvFirstName = (TextView) view.findViewById(R.id.tvVCOwnerFirstNameValue);
        tvLastName = (TextView) view.findViewById(R.id.tvVCOwnerLastNameValue);*/
        tvViewProfileBtn = (TextView) view.findViewById(R.id.tvVCOwnerViewProfileBtn);
        tvCarPhotosInfo = (TextView) view.findViewById(R.id.tvVCCarPhotosInfo);
        ivVCAddCarPhotos = (ImageView) view.findViewById(R.id.ivVCAddCarPhotos);
        rvCarPhotos = (RecyclerView) view.findViewById(R.id.rvVCCarPhotos);
        carDocumentation = (CardView) view.findViewById(R.id.cvVCCarDocumentation);
        tvCarDocumentationInfo = (TextView) view.findViewById(R.id.tvVCCarDocumentInfo);
        ivVCInfoCarDocuments = (ImageView) view.findViewById(R.id.ivVCInfoCarDocuments);
        ivVCAddCarDocumentation = (ImageView) view.findViewById(R.id.ivVCAddCarDocuments);
        rvVCCarDocuments = (RecyclerView) view.findViewById(R.id.rvVCCarDocuments);
        carAvailabilities = (CardView) view.findViewById(R.id.cvVCCarAvailabilities);
        tvAvailabilitiesInfo = (TextView) view.findViewById(R.id.tvVCAvailabilitiesInfo);
        ivVCAddAvailability = (ImageView) view.findViewById(R.id.ivVCAddAvailability);
        rvVCAvailabilities = (RecyclerView) view.findViewById(R.id.rvVCAvailabilities);
        bookButton = (CircularProgressButton) view.findViewById(R.id.cpbVCBook);
    }

    private void afterInitialization() {
        mode = RentalEngine.getViewCarMode();
        selectedCar = RentalEngine.getSelectedCar();
        checkViews();
        loadAndShowData();

        ivChangeAddress.setOnClickListener(this);
        ivChangeDailyPrice.setOnClickListener(this);
        ivChangeWarrantyPrice.setOnClickListener(this);
        ivChangeActiveStatus.setOnClickListener(this);
        ivVCAddCarPhotos.setOnClickListener(this);
        ivVCInfoCarDocuments.setOnClickListener(this);
        ivVCAddCarDocumentation.setOnClickListener(this);
        ivVCAddAvailability.setOnClickListener(this);
        tvViewProfileBtn.setOnClickListener(this);
        bookButton.setOnClickListener(this);
    }

    private void checkViews() {
        if (mode == RentalEngine.CARS_TO_BOOK) {
            llChangeAddress.setVisibility(View.INVISIBLE);
            llDailyPrice.setVisibility(View.INVISIBLE);
            llWarrantyPrice.setVisibility(View.INVISIBLE);
            llActiveStatus.setVisibility(View.GONE);
            //ownerData.setVisibility(View.VISIBLE);
            ivVCAddCarPhotos.setVisibility(View.GONE);
            carDocumentation.setVisibility(View.GONE);
            carAvailabilities.setVisibility(View.GONE);
            bookButton.setVisibility(View.VISIBLE);

            prepareCarPhotosRecyclerView();
            loadCarData();
            loadCarPhotos();
        }
        else if (mode == RentalEngine.USER_CARS) {
            llChangeAddress.setVisibility(View.VISIBLE);
            llDailyPrice.setVisibility(View.VISIBLE);
            llWarrantyPrice.setVisibility(View.VISIBLE);
            llActiveStatus.setVisibility(View.VISIBLE);
            //ownerData.setVisibility(View.GONE);
            ivVCAddCarPhotos.setVisibility(View.VISIBLE);
            carDocumentation.setVisibility(View.VISIBLE);
            carAvailabilities.setVisibility(View.VISIBLE);
            bookButton.setVisibility(View.GONE);

            prepareCarPhotosRecyclerView();
            prepareCarDocumentsRecyclerView();
            prepareAvailabilitiesRecyclerView();
            loadCarData();
            loadCarPhotos();
            loadCarDocumentation();
            loadAvailabilities();
        }
    }

    private void prepareCarPhotosRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvCarPhotos.setLayoutManager(layoutManager);
        rvCarPhotos.setItemAnimator(new DefaultItemAnimator());
        rvCarPhotos.setNestedScrollingEnabled(false);
        rvCarPhotos.setHasFixedSize(false);

        photosAdapter = new CarPhotosHorizontalAdapter(photoImages);
        rvCarPhotos.setAdapter(photosAdapter);
    }

    private void prepareCarDocumentsRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvVCCarDocuments.setLayoutManager(layoutManager);
        rvVCCarDocuments.setItemAnimator(new DefaultItemAnimator());
        rvVCCarDocuments.setNestedScrollingEnabled(false);
        rvVCCarDocuments.setHasFixedSize(false);

        documentationAdapter = new CarPhotosHorizontalAdapter(documentationImages);
        rvVCCarDocuments.setAdapter(documentationAdapter);
    }

    private void prepareAvailabilitiesRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvVCAvailabilities.setLayoutManager(layoutManager);
        rvVCAvailabilities.setItemAnimator(new DefaultItemAnimator());
        rvVCAvailabilities.setNestedScrollingEnabled(false);
        rvVCAvailabilities.setHasFixedSize(false);

        availabilitiesAdapter = new AvailabilitiesAdapter(availabilitiesList);
        rvVCAvailabilities.setAdapter(availabilitiesAdapter);
    }

    private void loadCarData() {
        tvManufacturer.setText(selectedCar.getManufacturer());
        tvModel.setText(selectedCar.getModel());
        tvProductionYear.setText(selectedCar.getProductionYear());
        tvOdometer.setText(selectedCar.getOdometer());
        tvTransmission.setText(selectedCar.getTransmission());
        tvBodyType.setText(selectedCar.getBodyType());
        if (selectedCar.isHasAc()) {
            tvAirConditioner.setText(getResources().getString(R.string.home_yes_option));
        }
        else {
            tvAirConditioner.setText(getResources().getString(R.string.home_no_option));
        }
        if (selectedCar.getAddress() != null) {
            tvAddress.setText(selectedCar.getAddress());
        }
        if (!selectedCar.getDailyPrice().equalsIgnoreCase("0")) {
            tvDailyPrice.setText(selectedCar.getDailyPrice() + " " + getResources().getString(R.string.bc_price_unit_label));
        }
        if (!selectedCar.getWarrantyPrice().equalsIgnoreCase("0")) {
            tvWarrantyPrice.setText(selectedCar.getWarrantyPrice() + " " + getResources().getString(R.string.bc_price_unit_label));
        }
        if (selectedCar.isActive()) {
            tvActiveStatus.setText(getResources().getString(R.string.bc_active));
        }
        else {
            tvActiveStatus.setText(getResources().getString(R.string.bc_inactive));
        }
    }

    private void loadCarPhotos() {
        if (selectedCar.getPhotos() != null && selectedCar.getPhotos().size() != 0) {
            for(CarPhoto iPhoto : selectedCar.getPhotos()) {
                photoImages.add(new ImageObject(iPhoto.getId(), iPhoto.getPath(), iPhoto.getThumbnailPath(), iPhoto.getName(), -1, iPhoto.getStatusId()));
            }
            photosAdapter.setItemsList(photoImages);
        }
        checkCarPhotoCardView();
    }

    private void loadCarDocumentation() {
        documentationImages.clear();
        if (selectedCar.getDocuments() != null && selectedCar.getDocuments().size() != 0) {
            for(CarDocument iDocument : selectedCar.getDocuments()) {
                if (iDocument.getDocument() != null) {
                    documentationImages.add(new ImageObject(iDocument.getId(), iDocument.getDocument(), iDocument.getDocument(), iDocument.getName(), iDocument.getDocumentTypeId(), -1));
                }
            }
            documentationAdapter.setItemsList(documentationImages);
        }
        checkCarDocumentationCardView();
    }

    private void loadAvailabilities() {
        availabilitiesList.clear();
        if (selectedCar.getAvailabilities() != null && selectedCar.getAvailabilities().size() != 0) {
            availabilitiesList = selectedCar.getAvailabilities();
            availabilitiesAdapter.setItemsList(availabilitiesList);
        }
        checkAvailabilitiesCardView();
    }

    private void checkCarPhotoCardView() {
        if (photoImages.size() == 0) {
            rvCarPhotos.setVisibility(View.GONE);
            tvCarPhotosInfo.setVisibility(View.VISIBLE);
        }
        else {
            tvCarPhotosInfo.setVisibility(View.GONE);
            rvCarPhotos.setVisibility(View.VISIBLE);
        }
    }

    private void checkCarDocumentationCardView() {
        if (documentationImages.size() == 0) {
            rvVCCarDocuments.setVisibility(View.GONE);
            tvCarDocumentationInfo.setVisibility(View.VISIBLE);
        }
        else {
            tvCarDocumentationInfo.setVisibility(View.GONE);
            rvVCCarDocuments.setVisibility(View.VISIBLE);
        }
        checkDocumentsWarningInfo();
    }

    private void checkAvailabilitiesCardView() {
        if (availabilitiesList.size() == 0) {
            rvVCAvailabilities.setVisibility(View.GONE);
            tvAvailabilitiesInfo.setVisibility(View.VISIBLE);
        }
        else {
            tvAvailabilitiesInfo.setVisibility(View.GONE);
            rvVCAvailabilities.setVisibility(View.VISIBLE);
        }
    }

    /*private void loadUserData() {
        if (carOwner != null) {
            tvFirstName.setText(carOwner.getFirstName());
            tvLastName.setText(carOwner.getLastName());
        }
        showAllData();
    }*/

    private void loadAndShowData() {
        if (RentalEngine.isNetworkAvailable(getActivity())) {
            if (mode == RentalEngine.CARS_TO_BOOK) {
                //callGetUserApi(selectedCar.getUserId());
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment thread Exception: " + e.toString());
                        } finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showAllData();
                                }
                            });
                        }
                    }
                };
                timer.start();
            } else if (mode == RentalEngine.USER_CARS) {
                callGetCitiesApi();
            }
        }
        else {
            showAllData();
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)), false));
        }
    }

    private void showAllData() {
        llCarData.setVisibility(View.VISIBLE);
        loading.smoothToHide();
    }

    private boolean hasLimitExceed(int totalSelectedImages) {
        if (totalSelectedImages + photoImages.size() > RentalEngine.MAXIMUM_IMAGES_ALLOWED) {
            Toast.makeText(getContext(), String.valueOf(getResources().getString(R.string.snackbar_maximum_allowed_image_exceed)), Toast.LENGTH_LONG).show();
            return true;
        }
        else {
            return false;
        }
    }

    private void checkDocumentsWarningInfo() {
        documentsInfo.clear();
        if (selectedCar.getDocuments() != null && selectedCar.getDocuments().size() != 0) {
            for(CarDocument iDocument : selectedCar.getDocuments()) {
                documentsInfo.add(getDocumentInfo(iDocument.getStatusId(), iDocument.getDocumentTypeId()));
            }
            documentationAdapter.setItemsList(documentationImages);
        }

        ivVCInfoCarDocuments.setVisibility(View.GONE);
        for (DocumentInfo iDocumentInfo : documentsInfo) {
            if (iDocumentInfo.isWarning()) {
                ivVCInfoCarDocuments.setVisibility(View.VISIBLE);
            }
        }
    }

    private DocumentInfo getDocumentInfo(int statusId, int documentType) {
        DocumentInfo info = new DocumentInfo();
        if (statusId == RentalEngine.APPROVED) {
            info.setWarning(false);
            info.setWarningMessage(getDocumentType(documentType) + getResources().getString(R.string.ds_approved));
        }
        else {
            info.setWarning(true);
            String warningMessage = "";
            switch (statusId) {
                case RentalEngine.ACTIVE:
                    warningMessage = getDocumentType(documentType) + getResources().getString(R.string.ds_active);
                    break;
                case RentalEngine.INACTIVE:
                    warningMessage = getDocumentType(documentType) + getResources().getString(R.string.ds_inactive);
                    break;
                case RentalEngine.CAR_KOLAUDIMI_NOT_UPLOADED:
                    warningMessage = getResources().getString(R.string.ds_car_kolaudimi_not_uploaded);
                    break;
                case RentalEngine.CAR_KOLAUDIMI_NOT_CONFIRMED:
                    warningMessage = getResources().getString(R.string.ds_car_kolaudimi_not_confirmed);
                    break;
                case RentalEngine.CAR_KOLAUDIMI_EXPIRED:
                    warningMessage = getResources().getString(R.string.ds_car_koladimi_expired);
                    break;
                case RentalEngine.CAR_LEJE_QARKULLIMI_NOT_CONFIRMED:
                    warningMessage = getResources().getString(R.string.ds_car_leje_qarkullimi_not_confirmed);
                    break;
                case RentalEngine.CAR_LEJE_QARKULLIMI_NOT_UPLOADED:
                    warningMessage = getResources().getString(R.string.ds_car_leje_qarkullimi_not_uploaded);
                    break;
                case RentalEngine.CAR_LEJE_QARKULLIMI_EXPIRED:
                    warningMessage = getResources().getString(R.string.ds_car_leje_qarkullimi_expired);
                    break;
                case RentalEngine.CAR_INSURANCE_NOT_UPLOADED:
                    warningMessage = getResources().getString(R.string.ds_car_insurance_not_uploaded);
                    break;
                case RentalEngine.CAR_INSURANCE_NOT_CONFIRMED:
                    warningMessage = getResources().getString(R.string.ds_car_insurance_not_confirmed);
                    break;
                case RentalEngine.CAR_INSURANCE_EXPIRED:
                    warningMessage = getResources().getString(R.string.ds_car_insurance_expired);
                    break;
                case RentalEngine.WAITING_APPROVAL:
                    warningMessage = getDocumentType(documentType) + getResources().getString(R.string.ds_waiting_approval);
                    break;
                case RentalEngine.REJECTED:
                    warningMessage = getDocumentType(documentType) + getResources().getString(R.string.ds_rejected);
                    break;
            }
            info.setWarningMessage(warningMessage);
        }
        return info;
    }

    private String getDocumentType(int documentType) {
        switch (documentType) {
            case RentalEngine.CAR_DOCUMENT_KOLAUDIMI:
                return getResources().getString(R.string.ds_pre_kolaudimi);
            case RentalEngine.CAR_DOCUMENT_INSURANCE:
                return getResources().getString(R.string.ds_pre_insurance);
            case RentalEngine.CAR_DOCUMENT_LICENSE:
                return getResources().getString(R.string.ds_pre_license);
            default:
                return "Error: ";
        }
    }

    private String getDocumentTypeParam(int documentType) {
        switch (documentType) {
            case RentalEngine.CAR_DOCUMENT_KOLAUDIMI:
                return RentalEngine.CAR_DOCUMENT_KOLAUDIMI_PARAM;
            case RentalEngine.CAR_DOCUMENT_INSURANCE:
                return RentalEngine.CAR_DOCUMENT_INSURANCE_PARAM;
            case RentalEngine.CAR_DOCUMENT_LICENSE:
                return RentalEngine.CAR_DOCUMENT_LICENSE_PARAM;
            default:
                return null;
        }
    }

    private void showDocumentsInfo() {
        boolean wrapInScrollView = true;
        MaterialDialog mMaterialDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.documents_info_dialog_title)
                .customView(R.layout.documents_info_dialog, wrapInScrollView)
                .positiveText("OK")
                .build();

        View view = mMaterialDialog.getCustomView();
        if (view != null) {
            TextView tvInfoFirst = (TextView) view.findViewById(R.id.tvInfoFirst);
            ImageView ivInfoFirst = (ImageView) view.findViewById(R.id.ivInfoFirst);
            if (documentsInfo!= null && documentsInfo.size()>0) {
                if (documentsInfo.get(0).isWarning()) {
                    ivInfoFirst.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_failed));
                }
                else {
                    ivInfoFirst.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_success));
                }
                tvInfoFirst.setText(documentsInfo.get(0).getWarningMessage());
            }

            TextView tvInfoSecond = (TextView) view.findViewById(R.id.tvInfoSecond);
            ImageView ivInfoSecond = (ImageView) view.findViewById(R.id.ivInfoSecond);
            if (documentsInfo!= null && documentsInfo.size()>1) {
                if (documentsInfo.get(1).isWarning()) {
                    ivInfoSecond.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_failed));
                }
                else {
                    ivInfoSecond.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_success));
                }
                tvInfoSecond.setText(documentsInfo.get(1).getWarningMessage());
            }

            TextView tvInfoThird = (TextView) view.findViewById(R.id.tvInfoThird);
            ImageView ivInfoThird = (ImageView) view.findViewById(R.id.ivInfoThird);
            if (documentsInfo!= null && documentsInfo.size()>2) {
                if (documentsInfo.get(2).isWarning()) {
                    ivInfoThird.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_failed));
                }
                else {
                    ivInfoThird.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_success));
                }
                tvInfoThird.setText(documentsInfo.get(2).getWarningMessage());
            }
        }

        mMaterialDialog.show();
    }

    /*private void callGetUserApi(int userId) {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_USERS_ROUTE + "/" + userId;
        new HttpGetUserAsync().execute(urlApi);
    }*/

    /*private class HttpGetUserAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private UserResponse mUser;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    Gson gson = new Gson();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    mUser = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment HttpGetUserAsync doInBackground Exception: " + e.toString());
                mUser = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (mUser != null) {
                    carOwner = mUser.getResponseData();
                    RentalEngine.setCarOwner(carOwner);
                }
            }
            loadUserData();
        }
    }*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OpenFrescoImageViewEvent event) {
        if (event != null) {
            new ImageViewer.Builder<>(getActivity(), event.getPhotoImages())
                    .setFormatter(new ImageViewer.Formatter<ImageObject>() {
                        @Override
                        public String format(ImageObject mImageObject) {
                            String url = "";
                            if (mImageObject.getType() == RentalEngine.LOCAL_IMAGE) {
                                url = mImageObject.getImageUri().toString();
                            }
                            else if (mImageObject.getType() == RentalEngine.SERVER_IMAGE) {
                                url = mImageObject.getImagePath();
                            }
                            return url;
                        }
                    })
                    .setStartPosition(event.getSelectedPosition())
                    .show();

        }
    }

    private void openChangeAddressDialog() {
        if (mAddresses != null && mAddresses.size() != 0) {
            new MaterialDialog.Builder(getContext())
                    .title(R.string.bc_address_dialog_title)
                    .items(mAddresses)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            selectedCar.setAddress(String.valueOf(text));
                            callUploadCarDataApi(ADDRESS_CHANGED, null);
                        }
                    })
                    .show();
        }
        else {
            if (RentalEngine.isNetworkAvailable(getActivity())) {
                callGetCitiesApi();
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)), false));
            }
        }
    }

    private void openChangeDailyPriceDialog(){
        new MaterialDialog.Builder(getContext())
                .title(R.string.bc_daily_price_dialog_title)
                .content(R.string.bc_daily_price_dialog_content)
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input(selectedCar.getDailyPrice(), selectedCar.getDailyPrice(), new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        selectedCar.setDailyPrice(String.valueOf(input));
                        callUploadCarDataApi(DAILY_PRICE_CHANGED, null);
                    }
                }).show();
    }

    private void openChangeWarrantyPriceDialog(){
        new MaterialDialog.Builder(getContext())
                .title(R.string.bc_warranty_price_dialog_title)
                .content(R.string.bc_warranty_price_dialog_content)
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .input(selectedCar.getWarrantyPrice(), selectedCar.getWarrantyPrice(), new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        selectedCar.setWarrantyPrice(String.valueOf(input));
                        callUploadCarDataApi(WARRANTY_PRICE_CHANGED, null);
                    }
                }).show();
    }

    private void openChangeActiveStatusDialog(){
        final boolean isActive = selectedCar.isActive();
        String title = "";
        String content = "";
        if (isActive) {
            title = getResources().getString(R.string.bc_deactivate_dialog_title);
            content = getResources().getString(R.string.bc_deactivate_dialog_content);
        }
        else {
            title = getResources().getString(R.string.bc_activate_dialog_title);
            content = getResources().getString(R.string.bc_activate_dialog_content);
        }
        new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .positiveText(R.string.bc_confirm_option)
                .negativeText(R.string.account_cancel_option)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        callChangeCarStatusApi(isActive);
                    }
                })
                .show();
    }

    private void callUploadCarDataApi(String mode, String documentType) {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + selectedCar.getId();
        new UploadCar(mode, documentType).execute(urlApi);
    }

    private void callChangeCarStatusApi(boolean isActive) {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + selectedCar.getId() + RentalEngine.RENTAL_API_CHANGE_STATUS_ROUTE;
        new ChangeCarStatus(isActive).execute(urlApi);
    }

    private void callGetCitiesApi() {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CITIES_ROUTE + RentalEngine.RENTAL_API_COUNTRY_ALBANIA;
        new HttpGetCitiesAsync().execute(urlApi);
    }

    private class HttpGetCitiesAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private ArrayList<City> tempCities;

        @Override
        protected String doInBackground(String... uri) {
            try {
                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    Gson gson = new Gson();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    Type collectionType = new TypeToken<Collection<City>>() {
                    }.getType();
                    tempCities = gson.fromJson(reader, collectionType);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment HttpGetCitiesAsync doInBackground Exception: " + e.toString());
                tempCities = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (tempCities != null && tempCities.size() != 0) {
                    for (City iCity : tempCities) {
                        mAddresses.add(iCity.getName());
                    }
                }
            }
            showAllData();
        }
    }

    public InputStream getUploadCarDataInputStream(String url, String mode, String documentTypeParam, ImageObject imageObject) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody;
            if (documentTypeParam == null) {
                if (mode.equalsIgnoreCase(AVAILABILITY_ADDED)) {
                    formBody = new FormBody.Builder()
                            .addEncoded("available_start_date", startDate)
                            .addEncoded("available_end_date", endDate)
                            .addEncoded(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                            .build();
                }
                else {
                    formBody = new FormBody.Builder()
                            .addEncoded("address", selectedCar.getAddress())
                            .addEncoded("daily_price", selectedCar.getDailyPrice())
                            .addEncoded("warranty_price", selectedCar.getWarrantyPrice())
                            .addEncoded(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                            .build();
                }
            }
            else {
                MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(imageObject.getImagePath()));
                File tempFile = new File(imageObject.getImagePath());

                formBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart(documentTypeParam, imageObject.getImageTitle(), RequestBody.create(MEDIA_TYPE, tempFile))
                        .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                        .build();
            }

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UploadCar extends AsyncTask<String, String, String> {
        private String mode;
        private String documentType;
        private CarResponse mCarResponse;

        private UploadCar(String mode, String documentType) {
            this.mode = mode;
            this.documentType = documentType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            switch (mode) {
                case ADDRESS_CHANGED:
                    ivChangeAddress.setVisibility(View.GONE);
                    loadingAddress.setVisibility(View.VISIBLE);
                    loadingAddress.smoothToShow();
                    break;
                case DAILY_PRICE_CHANGED:
                    ivChangeDailyPrice.setVisibility(View.GONE);
                    loadingDailyPrice.setVisibility(View.VISIBLE);
                    loadingDailyPrice.smoothToShow();
                    break;
                case WARRANTY_PRICE_CHANGED:
                    ivChangeWarrantyPrice.setVisibility(View.GONE);
                    loadingWarrantyPrice.setVisibility(View.VISIBLE);
                    loadingWarrantyPrice.smoothToShow();
                    break;
            }
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUploadCarDataInputStream(uri[0], mode, documentType, new ImageObject(mDocUri, mDocPath, mDocFileName, 0));
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                Gson gson = gsonBuilder.create();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarResponse = gson.fromJson(reader, CarResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment UploadCar doInBackground Exception: " + e.toString());
                mCarResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mCarResponse != null && mCarResponse.getResponseStatus() == 0) {
                if (documentType == null) {
                    if (mode.equalsIgnoreCase(AVAILABILITY_ADDED)) {
                        EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_availability_added_successfully), false));
                        selectedCar.setAvailabilities(mCarResponse.getResponseData().getAvailabilities());
                        loadAvailabilities();
                    }
                    else {
                        loadCarData();
                    }
                }
                else {
                    if (mCarResponse.getResponseData() != null) {
                        selectedCar.setDocuments(mCarResponse.getResponseData().getDocuments());
                        loadCarDocumentation();
                    }
                    else {
                        EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_update_failed), false));
                    }
                }
            }
            else {
                if (documentType != null && selectedDocumentType != 1) {
                    int iterator = -1;
                    for(int i=0; i<documentationImages.size(); i++) {
                        if (documentationImages.get(i).getDocumentType() == selectedDocumentType) {
                            iterator = i;
                            break;
                        }
                    }
                    documentationImages.get(iterator).setImageStatus(RentalEngine.IMAGE_STATUS_FAILED);
                    documentationAdapter.notifyItemChanged(iterator);
                }
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_update_failed), false));
            }

            if (documentType == null) {
                loadingAddress.smoothToHide();
                loadingAddress.setVisibility(View.GONE);
                ivChangeAddress.setVisibility(View.VISIBLE);
                loadingDailyPrice.smoothToHide();
                loadingDailyPrice.setVisibility(View.GONE);
                ivChangeDailyPrice.setVisibility(View.VISIBLE);
                loadingWarrantyPrice.smoothToHide();
                loadingWarrantyPrice.setVisibility(View.GONE);
                ivChangeWarrantyPrice.setVisibility(View.VISIBLE);
            }
             else {
                progressDialog.incrementProgress(1);
                if (progressDialog.getCurrentProgress() == progressDialog.getMaxProgress()) {
                    progressDialog.setContent(R.string.bc_uploading_photo_dialog_content_completed);
                    progressDialog.setCancelable(true);
                }
            }
        }
    }

    private void showAvailabilityDatePicker(final boolean firstTime) {
        if (llAddAvailabilityInfo != null) {
            llAddAvailabilityInfo.setVisibility(View.GONE);
        }
        if (startYear == -1) {
            Calendar c = Calendar.getInstance();
            startYear = c.get(Calendar.YEAR);
            startMonth = c.get(Calendar.MONTH);
            startDay = c.get(Calendar.DAY_OF_MONTH);
        }
        datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        DateTime tempDate = new DateTime().withYear(year).withMonthOfYear(monthOfYear+1).withDayOfMonth(dayOfMonth);
                        boolean isBeforeNow = false;
                        boolean isAfterOneYear = false;

                        if (tempDate.plusSeconds(15).isBeforeNow()) {
                            isBeforeNow = true;
                            tvAddAvailabilityInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_date_invalid)));
                            llAddAvailabilityInfo.setVisibility(View.VISIBLE);
                        }
                        if (tempDate.minusYears(1).isAfterNow()) {
                            isAfterOneYear = true;
                            tvAddAvailabilityInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_date_one_year_exceeded)));
                            llAddAvailabilityInfo.setVisibility(View.VISIBLE);
                        }

                        if (!isBeforeNow && !isAfterOneYear) {
                            if (firstTime) {
                                dtStartDate = tempDate;
                                startYear = year;
                                startMonth = monthOfYear;
                                startDay = dayOfMonth;
                                if (isEndDateSet) {
                                    if (dtStartDate.isBefore(dtEndDate)) {
                                        showAvailabilityTimePicker(firstTime);
                                    }
                                    else {
                                        tvAddAvailabilityInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_start_date_invalid)));
                                        llAddAvailabilityInfo.setVisibility(View.VISIBLE);
                                    }
                                }
                                else {
                                    showAvailabilityTimePicker(firstTime);
                                }
                            }
                            else {
                                dtEndDate = tempDate.withHourOfDay(23).withMinuteOfHour(59);
                                if (isStartDateSet) {
                                    if (dtStartDate.isBefore(dtEndDate)) {
                                        showAvailabilityTimePicker(firstTime);
                                    }
                                    else {
                                        tvAddAvailabilityInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_end_date_invalid)));
                                        llAddAvailabilityInfo.setVisibility(View.VISIBLE);
                                    }
                                }
                                else {
                                    showAvailabilityTimePicker(firstTime);
                                }
                            }
                        }
                    }
                }, startYear, startMonth, startDay);
        datePickerDialog.show();
    }

    private void showAvailabilityTimePicker(final boolean firstTime) {
        if (startHour == -1) {
            Calendar c = Calendar.getInstance();
            startHour = c.get(Calendar.HOUR_OF_DAY);
            startMinute = c.get(Calendar.MINUTE);
        }
        timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (firstTime) {
                            dtStartDate = dtStartDate.withHourOfDay(hourOfDay).withMinuteOfHour(minute);
                            if (isEndDateSet) {
                                if (dtStartDate.plusMinutes(1).isBefore(dtEndDate)) {
                                    confirmDateTime(firstTime, hourOfDay, minute);
                                }
                                else {
                                    tvAddAvailabilityInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_start_date_invalid)));
                                    llAddAvailabilityInfo.setVisibility(View.VISIBLE);
                                }
                            }
                            else {
                                confirmDateTime(firstTime, hourOfDay, minute);
                            }
                        }
                        else {
                            dtEndDate = dtEndDate.withHourOfDay(hourOfDay).withMinuteOfHour(minute);
                            if (isStartDateSet) {
                                if (dtStartDate.plusMinutes(1).isBefore(dtEndDate)) {
                                    confirmDateTime(firstTime, hourOfDay, minute);
                                }
                                else {
                                    tvAddAvailabilityInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_end_date_invalid)));
                                    llAddAvailabilityInfo.setVisibility(View.VISIBLE);
                                }
                            }
                            else {
                                confirmDateTime(firstTime, hourOfDay, minute);
                            }
                        }

                        if (isStartDateSet && isEndDateSet) {
                            positive.setEnabled(true);
                        }
                    }
                },
                startHour, startMinute, DateFormat.is24HourFormat(getContext())
        );
        timePickerDialog.show();
    }

    public void confirmDateTime(boolean firstTime, int hourOfDay, int minute) {
        if (firstTime) {
            startHour = hourOfDay;
            startMinute = minute;
            if (tvStartDate != null) {
                tvStartDate.setText(getDateTimeToString(dtStartDate));
            }
            startDate = dtStartDate.getYear() + "-" + String.format(Locale.ENGLISH,"%02d", dtStartDate.getMonthOfYear()) + "-" + String.format(Locale.ENGLISH,"%02d", dtStartDate.getDayOfMonth()) + " " + dtStartDate.getHourOfDay() + ":" + dtStartDate.getMinuteOfHour() + ":00";
            isStartDateSet = true;
        }
        else {
            if (tvEndDate != null) {
                tvEndDate.setText(getDateTimeToString(dtEndDate));
            }
            endDate = dtEndDate.getYear() + "-" + String.format(Locale.ENGLISH,"%02d", dtEndDate.getMonthOfYear()) + "-" + String.format(Locale.ENGLISH,"%02d", dtEndDate.getDayOfMonth()) + " " + dtEndDate.getHourOfDay() + ":" + dtEndDate.getMinuteOfHour() + ":00";
            isEndDateSet = true;
        }
    }

    private String getDateTimeToString(DateTime mDateTime) {
        return String.valueOf(mDateTime.getDayOfMonth() + " " + RentalEngine.getMonth(mDateTime.getMonthOfYear()-1) + " " + mDateTime.getYear() + ", " + mDateTime.getHourOfDay() + ":" + mDateTime.getMinuteOfHour());
    }

    private void showAddAvailabilityDialog() {
        boolean wrapInScrollView = true;
        MaterialDialog mMaterialDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.add_availability_dialog_title)
                .customView(R.layout.add_availability_dialog, wrapInScrollView)
                .negativeText(R.string.account_cancel_option)
                .positiveText(R.string.add_availability_dialog_confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        callUploadCarDataApi(AVAILABILITY_ADDED, null);
                    }
                })
                .build();

        positive = mMaterialDialog.getActionButton(DialogAction.POSITIVE);
        positive.setEnabled(false);

        View view = mMaterialDialog.getCustomView();
        if (view != null) {
            llAddAvailabilityInfo = (LinearLayout) view.findViewById(R.id.llAddAvailabilityInfo);
            tvAddAvailabilityInfo = (TextView) view.findViewById(R.id.tvAddAvailabilityInfo);
            ImageView ivAddStartDate = (ImageView) view.findViewById(R.id.ivAddAvailabilityStartDate);
            tvStartDate = (TextView) view.findViewById(R.id.tvAddAvailabilityStartDate);
            ImageView ivAddEndDate = (ImageView) view.findViewById(R.id.ivAddAvailabilityEndDate);
            tvEndDate = (TextView) view.findViewById(R.id.tvAddAvailabilityEndDate);

            ivAddStartDate.setOnClickListener(this);
            ivAddEndDate.setOnClickListener(this);
        }

        mMaterialDialog.show();
    }

    public InputStream getChangeCarStatusDataInputStream(String url, String bodyParamValue) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = new FormBody.Builder()
                    .addEncoded("action", bodyParamValue)
                    .addEncoded(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class ChangeCarStatus extends AsyncTask<String, String, String> {
        private boolean isActive;
        private String bodyParamValue;
        private CarResponse mCarResponse;

        private ChangeCarStatus(boolean isActive) {
            this.isActive = isActive;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ivChangeActiveStatus.setVisibility(View.GONE);
            loadingActiveStatus.setVisibility(View.VISIBLE);
            loadingActiveStatus.smoothToShow();

            if (isActive) {
                bodyParamValue = "deactivate";
            }
            else {
                bodyParamValue = "activate";
            }
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getChangeCarStatusDataInputStream(uri[0], bodyParamValue);
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                Gson gson = gsonBuilder.create();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarResponse = gson.fromJson(reader, CarResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment ChangeCarStatus doInBackground Exception: " + e.toString());
                mCarResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mCarResponse != null && mCarResponse.getResponseStatus() == 0) {
                boolean newIsActive = mCarResponse.getResponseData().isActive();
                selectedCar.setActive(newIsActive);
                if (newIsActive) {
                    tvActiveStatus.setText(getResources().getString(R.string.bc_active));
                }
                else {
                    tvActiveStatus.setText(getResources().getString(R.string.bc_inactive));
                }
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_update_failed), false));
            }

            loadingActiveStatus.smoothToHide();
            loadingActiveStatus.setVisibility(View.GONE);
            ivChangeActiveStatus.setVisibility(View.VISIBLE);
        }
    }

    private void showChooseDocumentType() {
        selectedDocumentType = -1;
        new MaterialDialog.Builder(getContext())
                .title(R.string.bc_vc_add_car_documents_title)
                .content(R.string.bc_vc_add_car_documents_content)
                .items(R.array.document_types_arrays)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        selectedDocumentType = which + 2;
                        showDialogToAddPhotos();
                        return true;
                    }
                })
                .positiveText(R.string.account_confirm_option)
                .negativeText(R.string.account_cancel_option)
                .show();
    }

    private void showDialogToAddPhotos() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.bc_add_photo_dialog_title)
                .items(R.array.account_change_photo_arrays)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            requestToAddPhoto(RentalEngine.REQUEST_IMAGE_CAPTURE);
                        }
                        else if (which == 1) {
                            requestToAddPhoto(RentalEngine.PICK_IMAGE_REQUEST);
                        }
                        return true;
                    }
                })
                .positiveText(R.string.account_confirm_option)
                .negativeText(R.string.account_cancel_option)
                .show();
    }

    private void requestToAddPhoto(int requestCode) {
        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
            }
            else {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
            }
            else {
                takeAPicture();
            }
        }
    }

    private void chooseImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (selectedDocumentType == -1) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RentalEngine.PICK_IMAGE_REQUEST);
    }

    private void takeAPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (selectedDocumentType == -1) {
            mUri = Uri.fromFile(getOutputMediaFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);
        }
        else {
            mDocUri = Uri.fromFile(getOutputMediaFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mDocUri);
        }

        startActivityForResult(intent, RentalEngine.REQUEST_IMAGE_CAPTURE);
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), String.valueOf(getResources().getString(R.string.app_image_folder_name)));

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String filename = "IMG_"+ timeStamp + ".jpg";
        if (selectedDocumentType == -1) {
            mFileName = filename;
        }
        else {
            mDocFileName = filename;
        }
        return new File(mediaStorageDir.getPath() + File.separator + filename);
    }

    private void prepareProgressDialog(int totalFiles) {
        progressDialog = new MaterialDialog.Builder(getContext())
                .title(R.string.bc_uploading_photo_dialog_title)
                .content(R.string.bc_uploading_photo_dialog_content_pending)
                .progress(false, totalFiles, true)
                .cancelable(false)
                .show();
    }

    public InputStream getUploadCarPhotoInputStream(String url, ImageObject imageObject) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(imageObject.getImagePath()));
            File tempFile = new File(imageObject.getImagePath());

            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("photo", imageObject.getImageTitle(), RequestBody.create(MEDIA_TYPE, tempFile))
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UploadCarImage extends AsyncTask<String, String, String> {
        private CarPhotoResponse mCarPhotoResponse;
        private int iterator;

        @Override
        protected String doInBackground(String... uri) {
            try {
                iterator = Integer.parseInt(uri[1]);
                InputStream inputStream = getUploadCarPhotoInputStream(uri[0], photoImages.get(iterator));
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarPhotoResponse = gson.fromJson(reader, CarPhotoResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment UploadCarImage doInBackground Exception: " + e.toString());
                mCarPhotoResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            boolean success = false;
            if (mCarPhotoResponse != null && mCarPhotoResponse.getResponseStatus() == 0) {
                success = true;
                if (iterator == 0) {
                    EventBus.getDefault().post(new RefreshToolbarImageEvent(photoImages.get(iterator).getImageUri().toString()));
                }
            }

            if (success) {
                photoImages.get(iterator).setImageStatus(RentalEngine.IMAGE_STATUS_SUCCESS);
                photoImages.get(iterator).setStatusId(mCarPhotoResponse.getResponseData().getStatusId());
                photoImages.get(iterator).setId(mCarPhotoResponse.getResponseData().getId());
            }
            else {
                photoImages.get(iterator).setImageStatus(RentalEngine.IMAGE_STATUS_FAILED);
            }
            photosAdapter.notifyItemChanged(iterator);

            progressDialog.incrementProgress(1);
            if (progressDialog.getCurrentProgress() == progressDialog.getMaxProgress()) {
                progressDialog.setContent(R.string.bc_uploading_photo_dialog_content_completed);
                progressDialog.setCancelable(true);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CheckRecyclerViewEvent event) {
        if (event != null) {
            checkCarPhotoCardView();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(DeleteCarPhotoEvent event) {
        if (event != null) {
            if (photoImages.size() > RentalEngine.MINIMUM_IMAGES_ALLOWED) {
                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    new DeleteCarImage().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_PHOTOS_ROUTE + "/" + event.getPhotoId() + RentalEngine.RENTAL_API_DELETE_PHOTO_ROUTE, String.valueOf(event.getPosition()));
                }
                else {
                    EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_no_internet_error_message), false));
                }
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_minimum_allowed_image_touched)), false));
            }
        }
    }

    public InputStream getDeleteCarImageInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .delete()
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class DeleteCarImage extends AsyncTask<String, String, String> {
        private CarPhotoResponse mCarPhotoResponse;
        private int position;

        @Override
        protected String doInBackground(String... uri) {
            try {
                position = Integer.parseInt(uri[1]);
                InputStream inputStream = getDeleteCarImageInputStream(uri[0]);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarPhotoResponse = gson.fromJson(reader, CarPhotoResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment DeleteCarImage doInBackground Exception: " + e.toString());
                mCarPhotoResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mCarPhotoResponse != null && mCarPhotoResponse.getResponseStatus() == 0) {
                photosAdapter.remove(position);
                if (position == 0) {
                    if (photoImages.size() > 0) {
                        if (photoImages.get(0).getImageStatus() != RentalEngine.IMAGE_STATUS_FAILED) {
                            if (photoImages.get(0).getType() == RentalEngine.LOCAL_IMAGE) {
                                EventBus.getDefault().post(new RefreshToolbarImageEvent(photoImages.get(0).getImageUri().toString()));
                            }
                            else if (photoImages.get(0).getType() == RentalEngine.SERVER_IMAGE) {
                                EventBus.getDefault().post(new RefreshToolbarImageEvent(photoImages.get(0).getImagePath()));
                            }
                        }
                    }
                    else {
                        EventBus.getDefault().post(new SetDefaultCarImageEvent());
                    }
                }
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(getResources().getString(R.string.snackbar_delete_image_failed), false));
            }
        }
    }

    private class SendEmail extends AsyncTask<String, String, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bookButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            bookButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            boolean sent;

            Mail mMail = new Mail(getResources().getString(R.string.email_username), getResources().getString(R.string.email_password), 1);
            String[] toArr = {"akilrajdho@gmail.com"};
            mMail.setTo(toArr);
            mMail.setFrom(getResources().getString(R.string.email_username));
            mMail.setSubject(getResources().getString(R.string.email_subject));
            mMail.setBody("Nje... Dy... Tre... Prova...");

            try {
                if (mMail.send()) {
                    sent = true;
                }
                else {
                    sent = false;
                }
            }
            catch (Exception e) {
                sent = false;
            }
            return sent;
        }

        @Override
        protected void onPostExecute(Boolean sent) {
            super.onPostExecute(sent);

            if (sent) {
                bookButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                bookButton.setAnimationListener(onAnimationEndListener);
                bookButton.setEnabled(false);
                EventBus.getDefault().post(new ShowSnackBarMessageEvent("Email-i u dergua me sukses!", false));
            }
            else {
                bookButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
                bookButton.setEnabled(true);
                EventBus.getDefault().post(new ShowSnackBarMessageEvent("Dergimi i emailit deshtoi!", false));
            }
        }
    }

    private void showErrorMessage(String errorMessage) {
        bookButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        bookButton.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private void tryToBookTheCar() {
        if (RentalEngine.isNetworkAvailable(getActivity())) {
            new BookTheCar().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE);
        }
        else {
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(750);
                    }
                    catch (InterruptedException e) {
                        Log.i(RentalEngine.RENTAL_LOG_TAG, "ViewCarFragment thread Exception: " + e.toString());
                    }
                    finally {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
                            }
                        });
                    }
                }
            };
            timer.start();
        }
    }

    private class BookTheCar extends AsyncTask<String, String, String> {
        private boolean booked = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            bookButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            bookButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                /**
                 GET https://www.skrill.com/app/
                 pay.pl?action=prepare&email=merchant@host.com&password=6b4c1ba48880bcd3341dbaeb68b2
                 647f&amount=1.2&currency=EUR&bnf_email=beneficiary@domain.com&subject=some_subject&
                 note=some_note&frn_trn_id=111
                 **/
                String url = "https://www.skrill.com/app/pay.pl?action=prepare&email=merchant@host.com&password=6b4c1ba48880bcd3341dbaeb68b2647f&amount=1.2&currency=EUR&bnf_email=beneficiary@domain.com&subject=some_subject&note=some_note";
                HttpResponse tempResponse = RentalEngine.retrieveStream(url);
                XmlToJson xmlToJson = new XmlToJson.Builder(tempResponse.getInputStream(), null).build();
                String jsonString = xmlToJson.toString();
                booked = true;
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (booked) {
                bookButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                bookButton.setAnimationListener(onAnimationEndListener);
                bookButton.setEnabled(false);
                EventBus.getDefault().post(new ShowSnackBarMessageEvent("Rezervimi u krye me sukses!", false));
            }
            else {
                bookButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
                bookButton.setEnabled(true);
                EventBus.getDefault().post(new ShowSnackBarMessageEvent("Rezervimi i makines deshtoi!", false));
            }
        }
    }
}
