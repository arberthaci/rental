package al.inovacion.rental.home.interact.user_account;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.data.MSharedPreferences;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.OnIddleCPBEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowFabButtonEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.event.UpdateMenuItemsEvent;
import al.inovacion.rental.data.event.UpdateNavHeaderEvent;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.data.models.api.UserProfile;
import al.inovacion.rental.data.models.api.UserResponse;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.string.ok;
import static android.app.Activity.RESULT_OK;

/**
 * Created by Arbër Thaçi on 17-03-24.
 * Email: arberlthaci@gmail.com
 */

public class AccountFragment extends Fragment implements View.OnClickListener {

    private AVLoadingIndicatorView loading;
    private LinearLayout accountProfile;
    private TextView emailField;
    private EditText firstNameField;
    private EditText lastNameField;
    private EditText phoneField;
    private ImageView changePasswordBtn;
    private ImageView chooseImageFromGalleryBtn;
    private ImageView takeAPictureBtn;
    private TextView tvCardIdInfo;
    private ImageView cardId;
    private ImageView ivCardIdInfo;
    private CircularProgressButton updateBtn;
    private TextWatcher textWatcher;

    private String firstName = "";
    private String lastName = "";
    private String phone = "";
    private Uri mUri;
    private static Uri avatarUri;
    private String mFileName = "";
    private static String avatarFileName = "";
    private String mPath = "";
    private static String avatarPath = "";
    private int px;
    private int py;

    public AccountFragment() {}

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.ACCOUNT_FRAGMENT_TAG, getResources().getString(R.string.menu_item_account)));
        EventBus.getDefault().post(new ShowFabButtonEvent());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
        py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());

        EventBus.getDefault().post(new CloseKeyboardEvent());
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAccountChangePassword:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                UpdatePasswordFragment updatePasswordFragment = new UpdatePasswordFragment();
                EventBus.getDefault().post(new OpenNewFragmentEvent(updatePasswordFragment, RentalEngine.UPDATE_PASSWORD_FRAGMENT_TAG));
                break;
            case R.id.ivAccountCardIdInfo:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                if (RentalEngine.getUser().getUser_profile() != null) {
                    showCardIdInfo(RentalEngine.getUser().getUser_profile().getStatusId());
                }
                else {
                    showCardIdInfo(0);
                }
                break;
            case R.id.ivAccountChooseImageFromGallery:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
                }
                else {
                    chooseImagesFromGallery();
                }
                break;
            case R.id.ivAccountTakeAPicture:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
                }
                else {
                    takeAPicture();
                }
                break;
            case R.id.cpbAccountUpdate:
                EventBus.getDefault().post(new CloseKeyboardEvent());
                tryToUpdateUser();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeAPicture();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RentalEngine.PICK_IMAGE_REQUEST:
                    if (data != null && data.getData() != null) {
                        mUri = data.getData();
                        mPath = RentalEngine.getRealPathFromURI(getContext(), mUri);
                        mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                        float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);
                        Picasso.with(getActivity())
                                .load(mUri)
                                .resize(px, py)
                                .centerCrop()
                                .rotate(tempRotation)
                                .placeholder(R.drawable.loading)
                                .error(R.drawable.error_loading)
                                .into(cardId);

                        tvCardIdInfo.setVisibility(View.GONE);
                        cardId.setVisibility(View.VISIBLE);
                    }
                    break;
                case RentalEngine.REQUEST_IMAGE_CAPTURE:
                    mPath = mUri.getPath();
                    float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);
                    Picasso.with(getActivity())
                            .load(mUri)
                            .resize(px, py)
                            .centerCrop()
                            .rotate(tempRotation)
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.error_loading)
                            .into(cardId);

                    tvCardIdInfo.setVisibility(View.GONE);
                    cardId.setVisibility(View.VISIBLE);
                    break;
            }
            callOnIddleCPB();
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            updateBtn.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.aLoading);
        accountProfile = (LinearLayout) view.findViewById(R.id.accountProfile);
        emailField = (TextView) view.findViewById(R.id.tvAccountEmail);
        firstNameField = (EditText) view.findViewById(R.id.etAccountFirstName);
        lastNameField = (EditText) view.findViewById(R.id.etAccountLastName);
        phoneField = (EditText) view.findViewById(R.id.etAccountPhone);
        changePasswordBtn = (ImageView) view.findViewById(R.id.ivAccountChangePassword);
        chooseImageFromGalleryBtn = (ImageView) view.findViewById(R.id.ivAccountChooseImageFromGallery);
        takeAPictureBtn = (ImageView) view.findViewById(R.id.ivAccountTakeAPicture);
        tvCardIdInfo = (TextView) view.findViewById(R.id.tvAccountCardIdInfo);
        cardId = (ImageView) view.findViewById(R.id.ivAccountCardId);
        ivCardIdInfo = (ImageView) view.findViewById(R.id.ivAccountCardIdInfo);
        updateBtn = (CircularProgressButton) view.findViewById(R.id.cpbAccountUpdate);
    }

    private void afterInitialization() {
        fillUIFields(RentalEngine.getUser());

        if (RentalEngine.getUser().getUser_profile() != null && RentalEngine.getUser().getUser_profile().getIdCard() != null && !RentalEngine.getUser().getUser_profile().getIdCard().equalsIgnoreCase("")) {
            tvCardIdInfo.setVisibility(View.GONE);
            cardId.setVisibility(View.VISIBLE);

            Picasso.with(getActivity())
                    .load(RentalEngine.getUser().getUser_profile().getIdCard())
                    .resize(px, py)
                    .centerCrop()
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.error_loading)
                    .into(cardId);
        }
        else {
            tvCardIdInfo.setVisibility(View.VISIBLE);
            cardId.setVisibility(View.GONE);
        }

        changePasswordBtn.setOnClickListener(this);
        ivCardIdInfo.setOnClickListener(this);
        chooseImageFromGalleryBtn.setOnClickListener(this);
        takeAPictureBtn.setOnClickListener(this);

        updateBtn.setIndeterminateProgressMode(true);
        updateBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        updateBtn.setOnClickListener(this);

        ViewTreeObserver vtoUpload = updateBtn.getViewTreeObserver();
        vtoUpload.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    updateBtn.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    updateBtn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

        textWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                callOnIddleCPB();
            }
        };
        firstNameField.addTextChangedListener(textWatcher);
        lastNameField.addTextChangedListener(textWatcher);
        phoneField.addTextChangedListener(textWatcher);

        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "AccountFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading.smoothToHide();
                            accountProfile.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        };
        timer.start();
    }

    private void fillUIFields(User user) {
        emailField.setText(user.getEmail());
        firstNameField.setText(user.getFirstName());
        lastNameField.setText(user.getLastName());
        phoneField.setText(user.getPhone());
        if (user.getUser_profile() != null) {
            checkCardId(user.getUser_profile());
        }
    }

    private void checkCardId(UserProfile mUserProfile) {
        if (mUserProfile.getIdCard() != null && mUserProfile.getStatusId() == RentalEngine.APPROVED) {
            ivCardIdInfo.setVisibility(View.INVISIBLE);
        }
        else {
            ivCardIdInfo.setVisibility(View.VISIBLE);
        }
    }

    private void showCardIdInfo(int statusId) {
        String content = getResources().getString(R.string.account_card_id_info_label);
        switch (statusId) {
            case 0:
                content = getResources().getString(R.string.ds_id_not_uploaded);
                break;
            case RentalEngine.ID_NOT_CONFIRMED:
                content = getResources().getString(R.string.ds_id_not_confirmed);
                break;
            case RentalEngine.ID_NOT_UPLOADED:
                content = getResources().getString(R.string.ds_id_not_uploaded);
                break;
            case RentalEngine.ID_EXPIRED:
                content = getResources().getString(R.string.ds_id_expired);
                break;
            case RentalEngine.WAITING_APPROVAL:
                content += getResources().getString(R.string.ds_waiting_approval);
                break;
            case RentalEngine.APPROVED:
                content += getResources().getString(R.string.ds_approved);
                break;
            case RentalEngine.REJECTED:
                content += getResources().getString(R.string.ds_rejected);
                break;
            default:
                content = getResources().getString(R.string.ds_id_not_uploaded);
                break;
        }
        new MaterialDialog.Builder(getContext())
                .title(R.string.app_name)
                .content(content)
                .positiveText(ok)
                .show();
    }

    public static void setAvatarData(Uri uri, String filename, String path) {
        avatarUri = uri;
        avatarFileName = filename;
        avatarPath = path;
    }

    private void chooseImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RentalEngine.PICK_IMAGE_REQUEST);
    }

    private void takeAPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mUri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);

        startActivityForResult(intent, RentalEngine.REQUEST_IMAGE_CAPTURE);
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), String.valueOf(getResources().getString(R.string.app_image_folder_name)));

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        mFileName = "IMG_"+ timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + mFileName);
    }

    private void enableActionsButton(boolean enable) {
        changePasswordBtn.setEnabled(enable);
        chooseImageFromGalleryBtn.setEnabled(enable);
        takeAPictureBtn.setEnabled(enable);
    }

    private void showErrorMessage(String errorMessage) {
        updateBtn.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        updateBtn.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private boolean areFieldsFilled() {
        if (!firstName.equalsIgnoreCase("") && !lastName.equalsIgnoreCase("") && !phone.equalsIgnoreCase("")) {
            return true;
        }
        if (firstName.equalsIgnoreCase("")) {
            playYoyo(firstNameField);
        }
        if (lastName.equalsIgnoreCase("")) {
            playYoyo(lastNameField);
        }
        if (phone.equalsIgnoreCase("")) {
            playYoyo(phoneField);
        }
        return false;
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private boolean userNeedUpdate(User user) {
        if (!firstName.equalsIgnoreCase(user.getFirstName()) ||
                !lastName.equalsIgnoreCase(user.getLastName()) ||
                !phone.equalsIgnoreCase(user.getPhone()) ||
                mUri != null ||
                avatarUri != null) {
            return true;
        }
        return false;
    }

    private void callOnSuccessCPB() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(750);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "AccountFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateBtn.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                            updateBtn.setAnimationListener(onAnimationEndListener);
                            updateBtn.setEnabled(false);
                        }
                    });
                }
            }
        };
        timer.start();
    }

    private void callOnIddleCPB() {
        updateBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        updateBtn.setEnabled(true);
    }

    private void tryToUpdateUser() {
        firstName = firstNameField.getText().toString();
        lastName = lastNameField.getText().toString();
        phone = phoneField.getText().toString();

        if (areFieldsFilled()) {
            if (userNeedUpdate(RentalEngine.getUser())) {
                updateBtn.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
                updateBtn.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
                enableActionsButton(false);

                if (RentalEngine.isNetworkAvailable(getActivity())) {
                    new UpdateUser().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_USERS_ROUTE + "/" + RentalEngine.getUser().getId());
                }
                else {
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(750);
                            } catch (InterruptedException e) {
                                Log.i(RentalEngine.RENTAL_LOG_TAG, "AccountFragment thread Exception: " + e.toString());
                            } finally {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
                                        enableActionsButton(true);
                                    }
                                });
                            }
                        }
                    };
                    timer.start();
                }
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_nothing_to_update)), false));
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    private void storeUserData(User mUser) {
        MSharedPreferences mSharedPreferences = new MSharedPreferences(getContext(), RentalEngine.RENTAL_SHARED_PREF);
        mSharedPreferences.putInt(RentalEngine.PREF_ID_KEY, mUser.getId());
        mSharedPreferences.putString(RentalEngine.PREF_FIRST_NAME_KEY, mUser.getFirstName());
        mSharedPreferences.putString(RentalEngine.PREF_LAST_NAME_KEY, mUser.getLastName());
        mSharedPreferences.putString(RentalEngine.PREF_EMAIL_KEY, mUser.getEmail());
        mSharedPreferences.putString(RentalEngine.PREF_PHONE_KEY, mUser.getPhone());
        if (mUser.getUser_profile() != null) {
            if (mUser.getUser_profile().getIdCard() != null) {
                mSharedPreferences.putString(RentalEngine.PREF_CARD_ID, mUser.getUser_profile().getIdCard());
                mSharedPreferences.putInt(RentalEngine.PREF_CARD_ID_STATUS, mUser.getUser_profile().getStatusId());
                if (mUser.getUser_profile().getStatusId() == RentalEngine.APPROVED) {
                    EventBus.getDefault().post(new UpdateMenuItemsEvent(RentalEngine.MENU_ITEM_ACCOUNT_INDICATOR));
                }
            }
            if (mUser.getUser_profile().getAvatar() != null) {
                mSharedPreferences.putString(RentalEngine.PREF_AVATAR, mUser.getUser_profile().getAvatar());
            }
        }
        EventBus.getDefault().post(new UpdateNavHeaderEvent());
    }

    private RequestBody preparePlainDataFormBody() {
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("first_name", firstName)
                .addFormDataPart("last_name", lastName)
                .addFormDataPart("phone", phone)
                .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                .build();
    }

    private RequestBody prepareCardIdFormBody() {
        MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(mPath));
        File tempFile = new File(mPath);

        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("first_name", firstName)
                .addFormDataPart("last_name", lastName)
                .addFormDataPart("phone", phone)
                .addFormDataPart("id_card", mFileName, RequestBody.create(MEDIA_TYPE, tempFile))
                .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                .build();
    }

    private RequestBody prepareUserProfileFormBody() {
        MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(avatarPath));
        File tempFile = new File(avatarPath);

        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("first_name", firstName)
                .addFormDataPart("last_name", lastName)
                .addFormDataPart("phone", phone)
                .addFormDataPart("avatar", avatarFileName, RequestBody.create(MEDIA_TYPE, tempFile))
                .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                .build();
    }

    private RequestBody prepareFullUserDataFormBody() {
        MediaType ID_CARD_MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(mPath));
        File idCardFile = new File(mPath);
        MediaType AVATAR_MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(avatarPath));
        File avatarFile = new File(avatarPath);

        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("first_name", firstName)
                .addFormDataPart("last_name", lastName)
                .addFormDataPart("phone", phone)
                .addFormDataPart("id_card", mFileName, RequestBody.create(ID_CARD_MEDIA_TYPE, idCardFile))
                .addFormDataPart("avatar", avatarFileName, RequestBody.create(AVATAR_MEDIA_TYPE, avatarFile))
                .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                .build();
    }

    public InputStream getUpdateUserInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            RequestBody formBody = null;
            if (mUri == null && avatarUri == null) {
                formBody = preparePlainDataFormBody();
            }
            else {
                if (mUri != null && avatarUri == null) {
                    formBody = prepareCardIdFormBody();
                }
                else if (mUri == null && avatarUri != null) {
                    formBody = prepareUserProfileFormBody();
                }
                else if (mUri != null && avatarUri != null){
                    formBody = prepareFullUserDataFormBody();
                }
            }

            if (formBody != null) {
                Request request = new Request.Builder()
                        .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                        .url(url)
                        .post(formBody)
                        .build();

                Response response = client.newCall(request).execute();
                mInputStream = response.body().byteStream();
            }
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UpdateUser extends AsyncTask<String, String, String> {
        private UserResponse mUserResponse;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getUpdateUserInputStream(uri[0]);
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mUserResponse = gson.fromJson(reader, UserResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "AccountFragment UpdateUser doInBackground Exception: " + e.toString());
                mUserResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mUserResponse != null) {
                switch (mUserResponse.getResponseStatus()) {
                    case 0:
                        if (mUserResponse.getResponseData() != null) {
                            storeUserData(mUserResponse.getResponseData());
                            callOnSuccessCPB();
                        }
                        else {
                            showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
                        }
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
            }
            enableActionsButton(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OnIddleCPBEvent event) {
        if (event != null) {
            callOnIddleCPB();
        }
    }
}
