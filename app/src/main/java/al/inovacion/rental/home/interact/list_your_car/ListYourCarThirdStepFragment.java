package al.inovacion.rental.home.interact.list_your_car;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.adapter.CarPhotosVerticalAdapter;
import al.inovacion.rental.data.event.CheckRecyclerViewEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.CarPhotoResponse;
import al.inovacion.rental.data.models.app.ImageObject;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Arbër Thaçi on 17-03-31.
 * Email: arberlthaci@gmail.com
 */

public class ListYourCarThirdStepFragment extends Fragment implements View.OnClickListener {

    private LinearLayout chooseBtn;
    private LinearLayout takeAPictureBtn;
    private RecyclerView rvCarImages;
    private TextView tvInfo;
    private CircularProgressButton nextButton;

    private CarPhotosVerticalAdapter adapter;
    private ArrayList<ImageObject> imageObjects = new ArrayList<>();
    private Uri mUri;
    private String mFileName = "";
    private String mPath = "";
    private int carId = -1;
    private int px;
    private int py;
    private int itemsToBeUploaded = 0;
    private int itemsUploading = 0;
    private int totalUploadedItems = 0;
    private boolean isUploading = false;

    public ListYourCarThirdStepFragment() {}

    public static ListYourCarThirdStepFragment newInstance() {
        return new ListYourCarThirdStepFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.LIST_YOUR_CAR_THIRD_STEP_FRAGMENT_TAG, getResources().getString(R.string.lyc_toolbar_third_step)));
        checkRecyclerView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_your_car_third_step, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());

        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llLYCTSUploadChoose:
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
                }
                else {
                    chooseImagesFromGallery();
                }
                break;
            case R.id.llLYCTSUploadTakeAPicture:
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
                }
                else {
                    takeAPicture();
                }
                break;
            case R.id.cpbLYCTSNext:
                tryToListUploadCarImages();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeAPicture();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            int totalSelectedImages = 1;
            switch (requestCode) {
                case RentalEngine.PICK_IMAGE_REQUEST:
                    if (data != null) {
                        if (data.getData() != null) {
                            if (!hasLimitExceed(totalSelectedImages)) {
                                mUri = data.getData();
                                mPath = RentalEngine.getRealPathFromURI(getContext(), mUri);
                                mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                                float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                                adapter.add(new ImageObject(mUri, mPath, mFileName, tempRotation));
                            }
                        }
                        else if (data.getClipData() != null) {
                            totalSelectedImages = data.getClipData().getItemCount();
                            if (!hasLimitExceed(totalSelectedImages)) {
                                for (int i = 0; i < totalSelectedImages; i++) {
                                    ClipData.Item mItem = data.getClipData().getItemAt(i);
                                    mUri = mItem.getUri();
                                    mPath = RentalEngine.getRealPathFromURI(getContext(), mUri);
                                    mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                                    float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                                    adapter.add(new ImageObject(mUri, mPath, mFileName, tempRotation));
                                }
                            }
                        }
                    }
                    break;
                case RentalEngine.REQUEST_IMAGE_CAPTURE:
                    if (!hasLimitExceed(totalSelectedImages)) {
                        mPath = mUri.getPath();
                        float tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUri);

                        adapter.add(new ImageObject(mUri, mPath, mFileName, tempRotation));
                    }
                    break;
            }
            nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            nextButton.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        chooseBtn = (LinearLayout) view.findViewById(R.id.llLYCTSUploadChoose);
        takeAPictureBtn = (LinearLayout) view.findViewById(R.id.llLYCTSUploadTakeAPicture);
        rvCarImages = (RecyclerView) view.findViewById(R.id.rvLYCTSCarImages);
        tvInfo = (TextView) view.findViewById(R.id.tvLYCTSInfo);
        nextButton = (CircularProgressButton) view.findViewById(R.id.cpbLYCTSNext);
    }

    private void afterInitialization() {
        chooseBtn.setOnClickListener(this);
        takeAPictureBtn.setOnClickListener(this);

        nextButton.setIndeterminateProgressMode(true);
        nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        nextButton.setOnClickListener(this);

        ViewTreeObserver vto = nextButton.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    nextButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    nextButton.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCarImages.setLayoutManager(mLayoutManager);
        rvCarImages.setItemAnimator(new DefaultItemAnimator());
        rvCarImages.setNestedScrollingEnabled(false);
        rvCarImages.setHasFixedSize(false);

        setUpItemTouchHelper();
        RentalEngine.setUpAnimationDecoratorHelper(getContext(), rvCarImages);

        adapter = new CarPhotosVerticalAdapter(imageObjects);
        rvCarImages.setAdapter(adapter);
        checkRecyclerView();
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = ContextCompat.getDrawable(getContext(), R.drawable.background_red_board_shape);
                xMark = ContextCompat.getDrawable(getActivity(), R.drawable.ic_clear_24dp);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.global_unit_normal);
                initiated = true;
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                if (isUploading || adapter.isPendingRemoval(position)) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                adapter.pendingRemoval(swipedPosition);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                if (viewHolder.getAdapterPosition() == -1) {
                    return;
                }

                if (!initiated) {
                    init();
                }

                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(rvCarImages);
    }

    private void chooseImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RentalEngine.PICK_IMAGE_REQUEST);
    }

    private void takeAPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mUri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);

        startActivityForResult(intent, RentalEngine.REQUEST_IMAGE_CAPTURE);
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), String.valueOf(getResources().getString(R.string.app_image_folder_name)));

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        mFileName = "IMG_"+ timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + mFileName);
    }

    private boolean hasLimitExceed(int totalSelectedImages) {
        if (totalSelectedImages + imageObjects.size() > RentalEngine.MAXIMUM_IMAGES_ALLOWED) {
            Toast.makeText(getContext(), String.valueOf(getResources().getString(R.string.snackbar_maximum_allowed_image_exceed)), Toast.LENGTH_LONG).show();
            return true;
        }
        else {
            return false;
        }
    }

    private void checkRecyclerView() {
        if (rvCarImages != null && tvInfo != null) {
            if (imageObjects != null && imageObjects.size() > 0) {
                rvCarImages.setVisibility(View.VISIBLE);
                tvInfo.setVisibility(View.GONE);
            } else {
                rvCarImages.setVisibility(View.GONE);
                tvInfo.setVisibility(View.VISIBLE);
            }
        }
    }

    private void enableActionsButton(boolean enable) {
        chooseBtn.setEnabled(enable);
        takeAPictureBtn.setEnabled(enable);
    }

    private void showErrorMessage(String errorMessage) {
        nextButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        nextButton.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private void openListYourCarFourthFragment() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarThirdStepFragment thread Exception: " + e.toString());
                }
                finally {
                    ListYourCarFourthStepFragment listYourCarFourthStepFragment = new ListYourCarFourthStepFragment();
                    listYourCarFourthStepFragment.setCarId(carId);
                    EventBus.getDefault().post(new OpenNewFragmentEvent(listYourCarFourthStepFragment, RentalEngine.LIST_YOUR_CAR_FOURTH_STEP_FRAGMENT_TAG));
                }
            }
        };
        timer.start();
    }

    private void tryToListUploadCarImages() {
        if (imageObjects.size() >= RentalEngine.MINIMUM_IMAGES_ALLOWED) {
            nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            nextButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
            enableActionsButton(false);

            if (RentalEngine.isNetworkAvailable(getActivity())) {
                itemsToBeUploaded = 0;
                for (int i=0; i<imageObjects.size(); i++) {
                    if (imageObjects.get(i).getImageStatus() != RentalEngine.IMAGE_STATUS_SUCCESS) {
                        itemsToBeUploaded++;
                    }
                }

                if (itemsToBeUploaded == 0) {
                    nextButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                    nextButton.setAnimationListener(onAnimationEndListener);
                    nextButton.setEnabled(true);
                    enableActionsButton(true);
                }
                else {
                    isUploading = true;
                    itemsUploading = 0;
                    totalUploadedItems = 0;
                    for (int i = 0; i < imageObjects.size(); i++) {
                        if (imageObjects.get(i).getImageStatus() != RentalEngine.IMAGE_STATUS_SUCCESS) {
                            new UploadCarImage().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + carId + RentalEngine.RENTAL_API_ADD_CAR_PHOTO_ROUTE, String.valueOf(i));
                        }
                    }
                }
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(750);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarThirdStepFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
                                    enableActionsButton(true);
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_minimum_allowed_image_not_meet)), false));
        }
    }

    public InputStream getUploadCardIdInputStream(String url, ImageObject imageObject) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(imageObject.getImagePath()));
            File tempFile = new File(imageObject.getImagePath());

            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("photo", imageObject.getImageTitle(), RequestBody.create(MEDIA_TYPE, tempFile))
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UploadCarImage extends AsyncTask<String, String, String> {
        private CarPhotoResponse mCarPhotoResponse;
        private int iterator;

        @Override
        protected String doInBackground(String... uri) {
            try {
                iterator = Integer.parseInt(uri[1]);
                InputStream inputStream = getUploadCardIdInputStream(uri[0], imageObjects.get(iterator));
                Gson gson = new Gson();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarPhotoResponse = gson.fromJson(reader, CarPhotoResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarThirdStepFragment UploadCardId doInBackground Exception: " + e.toString());
                mCarPhotoResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            boolean success = false;
            itemsUploading++;
            if (mCarPhotoResponse != null && mCarPhotoResponse.getResponseStatus() == 0) {
                success = true;
                totalUploadedItems++;
            }

            if (success) {
                imageObjects.get(iterator).setImageStatus(RentalEngine.IMAGE_STATUS_SUCCESS);
            }
            else {
                imageObjects.get(iterator).setImageStatus(RentalEngine.IMAGE_STATUS_FAILED);
            }
            adapter.notifyItemChanged(iterator);

            if (itemsUploading == itemsToBeUploaded) {
                isUploading = false;
                if (totalUploadedItems == itemsToBeUploaded) {
                    nextButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                    nextButton.setAnimationListener(onAnimationEndListener);
                    nextButton.setEnabled(false);
                    openListYourCarFourthFragment();
                }
                else {
                    showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_upload_image_failed)));
                    enableActionsButton(true);
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CheckRecyclerViewEvent event) {
        if (event != null) {
            checkRecyclerView();
        }
    }
}
