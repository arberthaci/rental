package al.inovacion.rental.home.interact.list_your_car;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.data.models.api.CarResponse;
import al.inovacion.rental.data.models.api.City;
import al.inovacion.rental.data.models.app.HttpResponse;
import al.inovacion.rental.data.models.edmunds.Make;
import al.inovacion.rental.data.models.edmunds.Makes;
import al.inovacion.rental.data.models.edmunds.Model;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Arbër Thaçi on 17-03-30.
 * Email: arberlthaci@gmail.com
 */

public class ListYourCarSecondStepFragment extends Fragment implements View.OnClickListener {

    private TextView addressDropDown;
    private AVLoadingIndicatorView addressLoading;
    private TextView makeDropDown;
    private AVLoadingIndicatorView makeLoading;
    private TextView modelDropDown;
    private View modelBottomLine;
    private TextView yearDropDown;
    private View yearBottomLine;
    private TextView odometerDropDown;
    private View odometerBottomLine;
    private TextView transmissionDropDown;
    private View transmissionBottomLine;
    private TextView numberOfDoorsDropDown;
    private View numberOfDoorsBottomLine;
    private RadioButton airConditionerTrue;
    private RadioButton airConditionerFalse;
    private CircularProgressButton nextButton;

    private ArrayList<City> cities = new ArrayList<>();
    private ArrayList<String> addressesList = new ArrayList<>();
    private Makes mMakes;
    private ArrayList<Make> makes = new ArrayList<>();
    private ArrayList<String> makesList = new ArrayList<>();
    private Make mMake;
    private ArrayList<Model> models = new ArrayList<>();
    private ArrayList<String> modelsList = new ArrayList<>();
    private Model mModel;
    private ArrayList<String> yearsList = new ArrayList<>();
    private ArrayList<String> odometersList = new ArrayList<>();
    private ArrayList<String> transmissionsList = new ArrayList<>();
    private ArrayList<String> numberOfDoorsList = new ArrayList<>();

    private Car carModel;
    private int addressChosen = -1;
    private int makeChosen = -1;
    private String modelChosen = "-1";
    private String yearChosen = "-1";
    private int odometerChosen = -1;
    private int transmissionChosen = -1;
    private int numberOfDoorsChosen = -1;
    private boolean hasAirConditioner = false;

    private boolean addressesReceived = true;
    private boolean makesReceived = true;

    private static final String FIRST_TIME = "first_time";
    private static final String SECOND_TIME = "second_time";

    public ListYourCarSecondStepFragment() {}

    public static ListYourCarSecondStepFragment newInstance() {
        return new ListYourCarSecondStepFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.LIST_YOUR_CAR_SECOND_STEP_FRAGMENT_TAG, getResources().getString(R.string.lyc_toolbar_second_step)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_your_car_second_step, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();

        loadCitiesAndMakesLists(FIRST_TIME);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new DismissSnackBarEvent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLYCAddressValue:
                addressDropDownClicked();
                break;
            case R.id.tvLYCMakeValue:
                makesDropDownClicked();
                break;
            case R.id.tvLYCModelValue:
                if (modelsList != null && modelsList.size() != 0) {
                    openModelDropdown();
                }
                break;
            case R.id.tvLYCYearValue:
                if (yearsList != null && yearsList.size() != 0) {
                    openYearDropdown();
                }
                break;
            case R.id.tvLYCOdometerValue:
                if (odometersList != null && odometersList.size() != 0) {
                    openOdometerDropdown();
                }
                break;
            case R.id.tvLYCTransmissionValue:
                if (transmissionsList != null && transmissionsList.size() != 0) {
                    openTransmissionDropdown();
                }
                break;
            case R.id.tvLYCNumberOfDoorsValue:
                if (numberOfDoorsList != null && numberOfDoorsList.size() != 0) {
                    openNumberOfDoorsDropdown();
                }
                break;
            case R.id.rbLYCAirConditionerTrue:
                hasAirConditioner = true;
                break;
            case R.id.rbLYCAirConditionerFalse:
                hasAirConditioner = false;
                break;
            case R.id.cpbLYCSSNext:
                tryToListYourCar();
                break;
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            nextButton.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        addressDropDown = (TextView) view.findViewById(R.id.tvLYCAddressValue);
        addressLoading = (AVLoadingIndicatorView) view.findViewById(R.id.lLYCAddressLoading);
        makeDropDown = (TextView) view.findViewById(R.id.tvLYCMakeValue);
        makeLoading = (AVLoadingIndicatorView) view.findViewById(R.id.lLYCMakeLoading);
        modelDropDown = (TextView) view.findViewById(R.id.tvLYCModelValue);
        modelBottomLine = view.findViewById(R.id.vLYCModelBottomLine);
        yearDropDown = (TextView) view.findViewById(R.id.tvLYCYearValue);
        yearBottomLine = view.findViewById(R.id.vLYCYearBottomLine);
        odometerDropDown = (TextView) view.findViewById(R.id.tvLYCOdometerValue);
        odometerBottomLine = view.findViewById(R.id.vLYCOdometerBottomLine);
        transmissionDropDown = (TextView) view.findViewById(R.id.tvLYCTransmissionValue);
        transmissionBottomLine = view.findViewById(R.id.vLYCTransmissionBottomLine);
        numberOfDoorsDropDown = (TextView) view.findViewById(R.id.tvLYCNumberOfDoorsValue);
        numberOfDoorsBottomLine = view.findViewById(R.id.vLYCNumberOfDoorsBottomLine);
        airConditionerTrue = (RadioButton) view.findViewById(R.id.rbLYCAirConditionerTrue);
        airConditionerFalse = (RadioButton) view.findViewById(R.id.rbLYCAirConditionerFalse);
        nextButton = (CircularProgressButton) view.findViewById(R.id.cpbLYCSSNext);
    }

    private void afterInitialization() {
        prepareDrowndowns();

        addressLoading.smoothToHide();
        makeLoading.smoothToHide();

        nextButton.setIndeterminateProgressMode(true);
        nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        nextButton.setOnClickListener(this);

        ViewTreeObserver vto = nextButton.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    nextButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    nextButton.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    private void prepareDrowndowns() {
        addressDropDown.setOnClickListener(this);
        makeDropDown.setOnClickListener(this);
        modelDropDown.setOnClickListener(this);
        modelDropDown.setClickable(false);
        yearDropDown.setOnClickListener(this);
        yearDropDown.setClickable(false);
        odometerDropDown.setOnClickListener(this);
        odometerDropDown.setClickable(false);
        transmissionDropDown.setOnClickListener(this);
        transmissionDropDown.setClickable(false);
        numberOfDoorsDropDown.setOnClickListener(this);
        numberOfDoorsDropDown.setClickable(false);
        airConditionerTrue.setOnClickListener(this);
        airConditionerFalse.setOnClickListener(this);
    }

    private void loadCitiesAndMakesLists(String loadingMode) {
        if (RentalEngine.isNetworkAvailable(getActivity())) {
            callGetCitiesApi(loadingMode);

            mMakes = RentalEngine.getMakes();
            if (mMakes != null) {
                initializeMakes(mMakes.getMakes(), FIRST_TIME);
            }
            else {
                callGetMakesApi(loadingMode);
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)), false));
        }
    }

    private void addressDropDownClicked() {
        if (addressesList != null && addressesList.size() != 0) {
            openAddressDropdown();
        }
        else if (addressesReceived) {
            if (RentalEngine.isNetworkAvailable(getActivity())) {
                if (addressLoading != null) {
                    addressLoading.setVisibility(View.VISIBLE);
                    addressLoading.smoothToShow();
                }
                callGetCitiesApi(SECOND_TIME);
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)), false));
            }
        }
    }

    private void makesDropDownClicked() {
        if (makesList != null && makesList.size() != 0) {
            openMakeDropdown();
        }
        else if (makesReceived) {
            if (RentalEngine.isNetworkAvailable(getActivity())) {
                if (makeLoading != null) {
                    makeLoading.setVisibility(View.VISIBLE);
                    makeLoading.smoothToShow();
                }
                callGetMakesApi(SECOND_TIME);
            }
            else {
                EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)), false));
            }
        }
    }

    private void openAddressDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_address_label)
                .items(addressesList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        addressChosen = which;
                        addressDropDown.setText(text);
                    }
                })
                .show();
    }

    private void openMakeDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_make_label)
                .items(makesList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        mMake = makes.get(which);
                        makeChosen = mMake.getId();
                        makeDropDown.setText(text);
                        modelDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_model)));
                        modelChosen = "-1";
                        enableDropDown(modelDropDown, modelBottomLine);
                        disableDropDown(yearDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_year)), yearBottomLine);
                        disableDropDown(odometerDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_odometer)), odometerBottomLine);
                        disableDropDown(transmissionDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_transmission)), transmissionBottomLine);
                        disableDropDown(numberOfDoorsDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_number_of_doors)), numberOfDoorsBottomLine);
                        if (mMake != null) {
                            initializeModelsSpinner(mMake.getModels());
                        }
                    }
                })
                .show();
    }

    private void openModelDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_model_label)
                .items(modelsList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        mModel = models.get(which);
                        modelChosen = mModel.getId();
                        modelDropDown.setText(text);
                        yearDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_year)));
                        yearChosen = "-1";
                        enableDropDown(yearDropDown, yearBottomLine);
                        disableDropDown(odometerDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_odometer)), odometerBottomLine);
                        disableDropDown(transmissionDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_transmission)), transmissionBottomLine);
                        disableDropDown(numberOfDoorsDropDown, String.valueOf(getResources().getString(R.string.lyc_choose_number_of_doors)), numberOfDoorsBottomLine);
                        if (mModel != null) {
                            initializeYearsSpinner();
                            initializeOdometerSpinner();
                            initializeTransmissionSpinner();
                            initializeNumberOfDoorsSpinner();
                        }
                    }
                })
                .show();
    }

    private void openYearDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_year_label)
                .items(yearsList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        yearChosen = yearsList.get(which);
                        yearDropDown.setText(text);
                        odometerDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_odometer)));
                        odometerChosen = -1;
                        enableDropDown(odometerDropDown, odometerBottomLine);
                        transmissionDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_transmission)));
                        transmissionChosen = -1;
                        enableDropDown(transmissionDropDown, transmissionBottomLine);
                        numberOfDoorsDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_number_of_doors)));
                        numberOfDoorsChosen = -1;
                        enableDropDown(numberOfDoorsDropDown, numberOfDoorsBottomLine);
                    }
                })
                .show();
    }

    private void openOdometerDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_odometer_label)
                .items(odometersList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        odometerChosen = which;
                        odometerDropDown.setText(text);
                        transmissionDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_transmission)));
                        transmissionChosen = -1;
                        numberOfDoorsDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_number_of_doors)));
                        numberOfDoorsChosen = -1;
                    }
                })
                .show();
    }

    private void openTransmissionDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_transmission_label)
                .items(transmissionsList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        transmissionChosen = which;
                        transmissionDropDown.setText(text);
                        numberOfDoorsDropDown.setText(String.valueOf(getResources().getString(R.string.lyc_choose_number_of_doors)));
                        numberOfDoorsChosen = -1;
                    }
                })
                .show();
    }

    private void openNumberOfDoorsDropdown() {
        new MaterialDialog.Builder(getContext())
                .title(R.string.lyc_number_of_doors_label)
                .items(numberOfDoorsList)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        numberOfDoorsChosen = which;
                        numberOfDoorsDropDown.setText(text);
                    }
                })
                .show();
    }

    private void enableDropDown(TextView dropdown, View bottomLine) {
        dropdown.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        dropdown.setClickable(true);
        bottomLine.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccentDark));
    }

    private void disableDropDown(TextView dropdown, String label, View bottomLine) {
        dropdown.setText(label);
        dropdown.setTextColor(ContextCompat.getColor(getContext(), R.color.grey_400));
        dropdown.setClickable(false);
        bottomLine.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey_400));
    }

    private void callGetCitiesApi(String loadingMode) {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CITIES_ROUTE + RentalEngine.RENTAL_API_COUNTRY_ALBANIA;
        new HttpGetCitiesAsync().execute(urlApi, loadingMode);
    }

    private void callGetMakesApi(String loadingMode) {
        String urlApi = RentalEngine.RENTAL_API_EDMUNDS_CAR_MAKES_ROUTE;
        new HttpGetMakesAsync().execute(urlApi, loadingMode);
    }

    private void initializeAddresses(String loadingMode) {
        if (cities != null) {
            addressesList = new ArrayList<>();
            for (City iCity : cities) {
                addressesList.add(iCity.getName());
            }
        }

        if (loadingMode.equalsIgnoreCase(SECOND_TIME)) {
            openAddressDropdown();
        }
    }

    private void initializeMakes(ArrayList<Make> makesParam, String loadingMode) {
        makes = new ArrayList<>();
        makes = makesParam;

        if (makes != null) {
            makesList = new ArrayList<>();
            for (Make iMake : makes) {
                makesList.add(iMake.getName());
            }
        }

        if (loadingMode.equalsIgnoreCase(SECOND_TIME)) {
            openMakeDropdown();
        }
    }

    private void initializeModelsSpinner(ArrayList<Model> modelsParam) {
        models = new ArrayList<>();
        models = modelsParam;

        if (models != null) {
            modelsList = new ArrayList<>();
            for (Model iModel : models) {
                modelsList.add(iModel.getName());
            }
        }
    }

    private void initializeYearsSpinner() {
        yearsList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        int actualYear = calendar.get(Calendar.YEAR);
        int thirtyYearsEarlier = actualYear - 30;

        for (int i=actualYear; i>=thirtyYearsEarlier; i--) {
            yearsList.add(String.valueOf(i));
        }
    }

    private void initializeOdometerSpinner() {
        odometersList = new ArrayList<>();
        odometersList.add("0-50,000 km");
        odometersList.add("50,000-100,000 km");
        odometersList.add("100,000-150,000 km");
        odometersList.add("150,000+ km");
    }

    private void initializeTransmissionSpinner() {
        transmissionsList = new ArrayList<>();
        transmissionsList.add("Manuale");
        transmissionsList.add("Automatike");
    }

    private void initializeNumberOfDoorsSpinner() {
        numberOfDoorsList = new ArrayList<>();
        numberOfDoorsList.add("2 dyer");
        numberOfDoorsList.add("4 dyer");
    }

    private boolean areFieldsFilled() {
        if (addressChosen != -1 && makeChosen != -1 && !modelChosen.equalsIgnoreCase("-1") && !yearChosen.equalsIgnoreCase("-1") && odometerChosen != -1 && transmissionChosen != -1 && numberOfDoorsChosen != -1) {
            return true;
        }
        if (addressChosen == -1) {
            playYoyo(addressDropDown);
        }
        if (makeChosen == -1) {
            playYoyo(makeDropDown);
        }
        if (modelChosen.equalsIgnoreCase("-1")) {
            playYoyo(modelDropDown);
        }
        if (yearChosen.equalsIgnoreCase("-1")) {
            playYoyo(yearDropDown);
        }
        if (odometerChosen == -1) {
            playYoyo(odometerDropDown);
        }
        if (transmissionChosen == -1) {
            playYoyo(transmissionDropDown);
        }
        if (numberOfDoorsChosen == -1) {
            playYoyo(numberOfDoorsDropDown);
        }
        return false;
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private void fillCarModel() {
        if (carModel == null) {
            carModel = new Car();
        }

        carModel.setManufacturer(mMake.getName());
        carModel.setModel(mModel.getName());
        carModel.setProductionYear(yearChosen);
        carModel.setTransmission(transmissionsList.get(transmissionChosen));
        carModel.setBodyType(numberOfDoorsList.get(numberOfDoorsChosen));
        carModel.setAddress(cities.get(addressChosen).getName());
        carModel.setOdometer(odometersList.get(odometerChosen));
        carModel.setHasAc(hasAirConditioner);
        carModel.setUserId(RentalEngine.getUser().getId());
    }

    private void tryToListYourCar() {
        if (areFieldsFilled()) {
            nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            nextButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
            enableActionsButton(false);

            fillCarModel();
            if (RentalEngine.isNetworkAvailable(getActivity())) {
                new CreateCar().execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE);
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(750);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarSecondStepFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
                                    enableActionsButton(true);
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_fill_all_fields)), false));
        }
    }

    private void showErrorMessage(String errorMessage) {
        nextButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        nextButton.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private void enableActionsButton(boolean enable) {
        addressDropDown.setClickable(enable);
        makeDropDown.setClickable(enable);
        modelDropDown.setClickable(enable);
        yearDropDown.setClickable(enable);
        odometerDropDown.setClickable(enable);
        transmissionDropDown.setClickable(enable);
        numberOfDoorsDropDown.setClickable(enable);
    }

    private void openListYourCarThirdFragment(final int carId) {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(1250);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarSecondStepFragment thread Exception: " + e.toString());
                }
                finally {
                    ListYourCarThirdStepFragment listYourCarThirdStepFragment = new ListYourCarThirdStepFragment();
                    listYourCarThirdStepFragment.setCarId(carId);
                    EventBus.getDefault().post(new OpenNewFragmentEvent(listYourCarThirdStepFragment, RentalEngine.LIST_YOUR_CAR_THIRD_STEP_FRAGMENT_TAG));
                }
            }
        };
        timer.start();
    }

    private void callOnSuccessCPB() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(750);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarSecondStepFragment thread Exception: " + e.toString());
                } finally {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            nextButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                            nextButton.setAnimationListener(onAnimationEndListener);
                            nextButton.setEnabled(false);
                        }
                    });
                }
            }
        };
        timer.start();
    }

    public InputStream getCreateCarInputStream(String url) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();
        int hasAc = 0;
        if (carModel.isHasAc()) {
            hasAc = 1;
        }

        try {
            RequestBody formBody = new FormBody.Builder()
                    .addEncoded("manufacturer", carModel.getManufacturer())
                    .addEncoded("model", carModel.getModel())
                    .addEncoded("transmission", carModel.getTransmission())
                    .addEncoded("production_year", carModel.getProductionYear())
                    .addEncoded("body_type", carModel.getBodyType())
                    .addEncoded("address", carModel.getAddress())
                    .addEncoded("odometer", carModel.getOdometer())
                    .addEncoded("has_ac", String.valueOf(hasAc))
                    .addEncoded("user_id", String.valueOf(carModel.getUserId()))
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class HttpGetCitiesAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private ArrayList<City> tempCities;
        private String loadingMode = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            addressesReceived = false;
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                loadingMode = uri[1];

                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    Gson gson = new Gson();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    Type collectionType = new TypeToken<Collection<City>>() {}.getType();
                    tempCities = gson.fromJson(reader, collectionType);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarSecondStepFragment HttpGetCitiesAsync doInBackground Exception: " + e.toString());
                tempCities = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (tempCities != null && tempCities.size() != 0) {
                    cities = tempCities;
                    initializeAddresses(loadingMode);
                }
            }
            if (addressLoading != null) {
                addressLoading.smoothToHide();
                addressLoading.setVisibility(View.GONE);
            }
            addressesReceived = true;
        }
    }

    private class HttpGetMakesAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private Makes makes;
        private String loadingMode = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            makesReceived = false;
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                loadingMode = uri[1];

                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    Gson gson = new Gson();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    makes = gson.fromJson(reader, Makes.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarSecondStepFragment HttpGetMakesAsync doInBackground Exception: " + e.toString());
                makes = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (makes != null) {
                    RentalEngine.setMakes(makes);
                    if (mMakes == null) {
                        mMakes = makes;
                        initializeMakes(makes.getMakes(), loadingMode);
                    }
                }
            }
            if (makeLoading != null) {
                makeLoading.smoothToHide();
                makeLoading.setVisibility(View.GONE);
            }
            makesReceived = true;
        }
    }

    private class CreateCar extends AsyncTask<String, String, String> {
        private CarResponse mCarResponse;

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = getCreateCarInputStream(uri[0]);
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                Gson gson = gsonBuilder.create();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarResponse = gson.fromJson(reader, CarResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarSecondStepFragment CreateCar doInBackground Exception: " + e.toString());
                mCarResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mCarResponse != null) {
                switch (mCarResponse.getResponseStatus()) {
                    case 0:
                        if (mCarResponse.getResponseData() != null) {
                            callOnSuccessCPB();
                            openListYourCarThirdFragment(mCarResponse.getResponseData().getId());
                        }
                        else {
                            showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_create_car_failed)));
                            enableActionsButton(true);
                        }
                        break;
                    case 1:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_create_car_failed)));
                        enableActionsButton(true);
                        break;
                    default:
                        showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_create_car_failed)));
                        enableActionsButton(true);
                        break;
                }
            }
            else {
                showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_create_car_failed)));
                enableActionsButton(true);
            }
        }
    }
}
