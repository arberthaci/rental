package al.inovacion.rental.home;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.plumillonforge.android.chipview.Chip;
import com.squareup.picasso.Picasso;

import net.danlew.android.joda.JodaTimeAndroid;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.data.MSharedPreferences;
import al.inovacion.rental.data.event.BackButtonEvent;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.DismissSnackBarEvent;
import al.inovacion.rental.data.event.OnIddleCPBEvent;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.OpenViewCarEvent;
import al.inovacion.rental.data.event.RefreshToolbarImageEvent;
import al.inovacion.rental.data.event.SearchFiltersEvent;
import al.inovacion.rental.data.event.SetDefaultCarImageEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowFabButtonEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.event.StopRefreshingEvent;
import al.inovacion.rental.data.event.SwipeToRefreshEvent;
import al.inovacion.rental.data.event.UpdateMenuItemsEvent;
import al.inovacion.rental.data.event.UpdateNavHeaderEvent;
import al.inovacion.rental.data.models.api.City;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.data.models.api.UserProfile;
import al.inovacion.rental.data.models.app.Filter;
import al.inovacion.rental.data.models.app.HttpResponse;
import al.inovacion.rental.data.models.edmunds.Make;
import al.inovacion.rental.data.models.edmunds.Makes;
import al.inovacion.rental.data.models.edmunds.Model;
import al.inovacion.rental.engine.CustomTypefaceSpan;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.home.info.AboutUsFragment;
import al.inovacion.rental.home.info.HowRentalWorksFragment;
import al.inovacion.rental.home.info.SendFeedbackFragment;
import al.inovacion.rental.home.info.TermsOfServiceFragment;
import al.inovacion.rental.home.interact.cars.ViewCarFragment;
import al.inovacion.rental.home.interact.list_your_car.ListYourCarSecondStepFragment;
import al.inovacion.rental.home.interact.user_account.AccountFragment;
import al.inovacion.rental.home.interact.cars.BrowseCarsFragment;
import al.inovacion.rental.home.interact.cars.DashboardFragment;
import al.inovacion.rental.home.interact.FavoritesFragment;
import al.inovacion.rental.home.interact.list_your_car.ListYourCarFragment;
import al.inovacion.rental.home.interact.MessagesFragment;
import al.inovacion.rental.home.interact.TripHistoryFragment;
import al.inovacion.rental.intro.IntroActivity;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.R.string.ok;

/**
 * Created by Arbër Thaçi on 17-03-23.
 * Email: arberlthaci@gmail.com
 */

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private CoordinatorLayout coordinatorLayout;
    private Snackbar snackbar;
    private CoordinatorLayout.LayoutParams cordinatorLayoutParams;
    private AppBarLayout appBarLayout;
    private AppBarLayout.Behavior appBarLayoutBehavior;
    private CollapsingToolbarLayout homeCollapsingToolbarLayout;
    private Toolbar toolbar;
    private ImageView toolbarImage;
    private CircleImageView accountImage;
    private NestedScrollView homeNestedScrollView;
    private SwipeRefreshLayout swipeToRefresh;
    private FloatingActionButton fabAction;
    private MenuItem searchBtn;
    private Menu menu;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private View headerLayout;
    private LinearLayout navHeader;
    private CircleImageView userImage;
    private TextView userFirstLastName;
    private LinearLayout llSearchInfo;
    private TextView tvSearchInfo;
    private TextView tvStartDateValue;
    private TextView tvEndDateValue;
    private DatePickerDialog datePickerDialog;

    private String currentFragmentTag = "empty";
    private boolean isCardIdConfirmed = false;
    private boolean hasAvatar = false;
    private Uri mUri;
    private String mFileName = "";
    private String mPath = "";
    private boolean isOnActivityResult = false;
    private ArrayList<String> mManufacturers = new ArrayList<>();
    private int manufacturerPosition;
    private String manufacturerChoosed;
    private ArrayList<String> mModels = new ArrayList<>();
    private int modelPosition;
    private String modelChoosed;
    private int productionYearPosition;
    private String productionYearChoosed;
    private int odometerPosition;
    private String odometerChoosed;
    private int transmissionPosition;
    private String transmissionChoosed;
    private int bodyTypePosition;
    private String bodyTypeChoosed;
    private int airConditionerPosition;
    private int airConditionerChoosed;
    private int warrantyPrice;
    private ArrayList<String> mAddresses = new ArrayList<>();
    private int addressPosition;
    private String addressChoosed;
    private int startDay = -1;
    private int startMonth = -1;
    private int startYear = -1;
    private boolean isStartDateSet = false;
    private boolean isEndDateSet = false;
    private DateTime dtStartDate;
    private DateTime dtEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        JodaTimeAndroid.init(this);
        Fresco.initialize(this);

        setContentView(R.layout.activity_home);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        loadUserData();
        initializeComponents();
        afterInitialization();

        if (savedInstanceState == null) {
            openBrowseCarsFragment();
        }

        checkIfCardIdIsUploaded();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            if (currentFragmentTag == RentalEngine.DASHBOARD_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.MESSAGES_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.TRIP_HISTORY_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.FAVORITES_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.ACCOUNT_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.HOW_RENTAL_WORKS_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.ABOUT_US_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.SEND_FEEDBACK_FRAGMENT_TAG ||
                    currentFragmentTag == RentalEngine.TERMS_OF_SERVICES_FRAGMENT_TAG) {
                openBrowseCarsFragment();
            }
            else if (currentFragmentTag == RentalEngine.VIEW_CAR_FRAGMENT_TAG) {
                if (RentalEngine.getViewCarMode() == RentalEngine.CARS_TO_BOOK) {
                    openBrowseCarsFragment();
                }
                else if (RentalEngine.getViewCarMode() == RentalEngine.USER_CARS) {
                    DashboardFragment mDashboardFragment = new DashboardFragment();
                    openNewFragment(mDashboardFragment, RentalEngine.DASHBOARD_FRAGMENT_TAG);
                }
            }
            else if (currentFragmentTag == RentalEngine.CAR_OWNER_FRAGMENT_TAG) {
                if (RentalEngine.getSelectedCar() != null) {
                    openNewFragment(new ViewCarFragment(), RentalEngine.VIEW_CAR_FRAGMENT_TAG);
                }
            }
            else if (currentFragmentTag == RentalEngine.LIST_YOUR_CAR_SECOND_STEP_FRAGMENT_TAG ||
                        currentFragmentTag == RentalEngine.LIST_YOUR_CAR_THIRD_STEP_FRAGMENT_TAG ||
                        currentFragmentTag == RentalEngine.LIST_YOUR_CAR_FOURTH_STEP_FRAGMENT_TAG ||
                        currentFragmentTag == RentalEngine.LIST_YOUR_CAR_FIFTH_STEP_FRAGMENT_TAG) {
                askToExitFromCarListing();
            }
            else if (currentFragmentTag == RentalEngine.UPDATE_PASSWORD_FRAGMENT_TAG) {
                AccountFragment mAccountFragment = new AccountFragment();
                openNewFragment(mAccountFragment, RentalEngine.ACCOUNT_FRAGMENT_TAG);
            }
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        searchBtn = menu.findItem(R.id.action_search);
        if (currentFragmentTag.equalsIgnoreCase(RentalEngine.BROWSE_CARS_FRAGMENT_TAG)) {
            searchBtn.setIcon(ContextCompat.getDrawable(this, android.R.drawable.ic_menu_search));
            searchBtn.setVisible(true);
        }
        else if (currentFragmentTag.equalsIgnoreCase(RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG)) {
            searchBtn.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_advance));
            searchBtn.setVisible(true);
        }
        else {
            searchBtn.setVisible(false);
        }
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                final View v = findViewById(R.id.action_search);

                if (v != null) {
                    v.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return false;
                        }
                    });
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            if (currentFragmentTag.equalsIgnoreCase(RentalEngine.BROWSE_CARS_FRAGMENT_TAG)) {
                showSearchFilters();
            }
            else if (currentFragmentTag.equalsIgnoreCase(RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG)) {
                ListYourCarSecondStepFragment listYourCarSecondStepFragment = new ListYourCarSecondStepFragment();
                openNewFragment(listYourCarSecondStepFragment, RentalEngine.LIST_YOUR_CAR_SECOND_STEP_FRAGMENT_TAG);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_browse_cars:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.BROWSE_CARS_FRAGMENT_TAG)) {
                    openBrowseCarsFragment();
                }
                break;
            case R.id.nav_dashboard:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.DASHBOARD_FRAGMENT_TAG)) {
                    DashboardFragment mDashboardFragment = new DashboardFragment();
                    openNewFragment(mDashboardFragment, RentalEngine.DASHBOARD_FRAGMENT_TAG);
                }
                break;
            case R.id.nav_messages:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.MESSAGES_FRAGMENT_TAG)) {
                    MessagesFragment mMessagesFragment = new MessagesFragment();
                    openNewFragment(mMessagesFragment, RentalEngine.MESSAGES_FRAGMENT_TAG);
                }
                break;
            case R.id.nav_list_your_car:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG)) {
                    ListYourCarFragment mListYourCarFragment = new ListYourCarFragment();
                    openNewFragment(mListYourCarFragment, RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG);
                }
                break;
            case R.id.nav_trip_history:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.TRIP_HISTORY_FRAGMENT_TAG)) {
                    TripHistoryFragment mTripHistoryFragment = new TripHistoryFragment();
                    openNewFragment(mTripHistoryFragment, RentalEngine.TRIP_HISTORY_FRAGMENT_TAG);
                }
                break;
            case R.id.nav_favorites:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.FAVORITES_FRAGMENT_TAG)) {
                    FavoritesFragment mFavoritesFragment = new FavoritesFragment();
                    openNewFragment(mFavoritesFragment, RentalEngine.FAVORITES_FRAGMENT_TAG);
                }
                break;
            case R.id.nav_account:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.ACCOUNT_FRAGMENT_TAG)) {
                    AccountFragment mAccountFragment = new AccountFragment();
                    openNewFragment(mAccountFragment, RentalEngine.ACCOUNT_FRAGMENT_TAG);
                }
                break;
            case R.id.nav_how_rental_works:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.HOW_RENTAL_WORKS_FRAGMENT_TAG)) {
                    HowRentalWorksFragment mHowRentalWorksFragment = new HowRentalWorksFragment();
                    openNewFragment(mHowRentalWorksFragment, RentalEngine.HOW_RENTAL_WORKS_FRAGMENT_TAG);
                    unCheckMenuItems();
                }
                break;
            case R.id.nav_about_us:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.ABOUT_US_FRAGMENT_TAG)) {
                    AboutUsFragment mAboutUsFragment = new AboutUsFragment();
                    openNewFragment(mAboutUsFragment, RentalEngine.ABOUT_US_FRAGMENT_TAG);
                    unCheckMenuItems();
                }
                break;
            case R.id.nav_send_feedback:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.SEND_FEEDBACK_FRAGMENT_TAG)) {
                    SendFeedbackFragment mSendFeedbackFragment = new SendFeedbackFragment();
                    openNewFragment(mSendFeedbackFragment, RentalEngine.SEND_FEEDBACK_FRAGMENT_TAG);
                    unCheckMenuItems();
                }
                break;
            case R.id.nav_terms_of_service:
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.TERMS_OF_SERVICES_FRAGMENT_TAG)) {
                    TermsOfServiceFragment mTermsOfServiceFragment = new TermsOfServiceFragment();
                    openNewFragment(mTermsOfServiceFragment, RentalEngine.TERMS_OF_SERVICES_FRAGMENT_TAG);
                    unCheckMenuItems();
                }
                break;
            case R.id.nav_exit_application:
                logOutUser();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llNavHeaderHome:
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                if (!currentFragmentTag.equalsIgnoreCase(RentalEngine.ACCOUNT_FRAGMENT_TAG)) {
                    AccountFragment mAccountFragment = new AccountFragment();
                    openNewFragment(mAccountFragment, RentalEngine.ACCOUNT_FRAGMENT_TAG);
                }
                break;
            case R.id.fabHome:
                showDialogToChangeUserPhoto();
                break;
            case R.id.ivSearchStartDate:
                showSearchDatePicker(true);
                break;
            case R.id.ivSearchEndDate:
                showSearchDatePicker(false);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeAPicture();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RentalEngine.PICK_IMAGE_REQUEST:
                    if (data != null && data.getData() != null) {
                        mUri = data.getData();
                        mPath = RentalEngine.getRealPathFromURI(this, mUri);
                        mFileName = mPath.substring(mPath.lastIndexOf("/")+1);
                        float tempRotation = RentalEngine.getCorrectRotationFromOrientation(this, mUri);
                        AccountFragment.setAvatarData(mUri, mFileName, mPath);
                        loadBluredToolbarImage(String.valueOf(mUri), tempRotation);
                        loadAccountImage(String.valueOf(mUri), tempRotation);
                        if (accountImage != null) {
                            accountImage.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                case RentalEngine.REQUEST_IMAGE_CAPTURE:
                    mPath = mUri.getPath();
                    float tempRotation = RentalEngine.getCorrectRotationFromOrientation(this, mUri);
                    AccountFragment.setAvatarData(mUri, mFileName, mPath);
                    loadBluredToolbarImage(String.valueOf(mUri), tempRotation);
                    loadAccountImage(String.valueOf(mUri), tempRotation);
                    if (accountImage != null) {
                        accountImage.setVisibility(View.VISIBLE);
                    }
                    break;
            }
            setOnActivityResult(true);
            callOnIddleCPB();
        }
    }

    private void initializeComponents() {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.home_coordinator_layout);
        appBarLayout = (AppBarLayout) findViewById(R.id.homeAppBarLayout);
        homeCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.homeCollapsingToolbarLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarImage = (ImageView) findViewById(R.id.toolbarImage);
        accountImage = (CircleImageView) findViewById(R.id.toolbarAvatarImage);
        homeNestedScrollView = (NestedScrollView) findViewById(R.id.homeNestedScrollView);
        swipeToRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_to_refresh);
        fabAction = (FloatingActionButton) findViewById(R.id.fabHome);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    private void afterInitialization() {
        prepareToolbar();
        prepareNavigationDrawer();
        prepareSwipeToRefresh();
        prepareHeaderLayout();
        prepareFabAction();
    }

    private void prepareToolbar() {
        //appBarLayout.setExpanded(false, true); //// TODO: 17-03-30 Delete this line after all the fragments are done
        cordinatorLayoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
        appBarLayoutBehavior = (AppBarLayout.Behavior) cordinatorLayoutParams.getBehavior();
        if (appBarLayoutBehavior == null) {
            cordinatorLayoutParams.setBehavior(new AppBarLayout.Behavior());
            appBarLayoutBehavior = (AppBarLayout.Behavior) cordinatorLayoutParams.getBehavior();
        }
        homeCollapsingToolbarLayout.setTitle(" ");
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    private void prepareNavigationDrawer() {
        toggle = new ActionBarDrawerToggle(HomeActivity.this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        menu = navigationView.getMenu();
        prepareMenuItemFonts(isCardIdConfirmed);
        MenuItem tools = menu.findItem(R.id.itemGroupLabelCommunicateWithUs);
        SpannableString s = new SpannableString(tools.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.RentalItemGroupStle), 0, s.length(), 0);
        tools.setTitle(s);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void prepareSwipeToRefresh() {
        swipeToRefresh.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new SwipeToRefreshEvent());
            }
        });
    }

    private void prepareHeaderLayout() {
        headerLayout = navigationView.getHeaderView(0);
        navHeader = (LinearLayout) headerLayout.findViewById(R.id.llNavHeaderHome);
        userImage = (CircleImageView) headerLayout.findViewById(R.id.civDrawerUserImage);
        userFirstLastName = (TextView) headerLayout.findViewById(R.id.tvDrawerUserFirstLastName);
        updateNavHeaderView();
        navHeader.setOnClickListener(this);
    }

    private void prepareFabAction() {
        fabAction.setVisibility(View.GONE);
        fabAction.setOnClickListener(this);
    }

    private void prepareMenuItemFonts(boolean isOk) {
        for (int i=0; i<menu.size(); i++) {
            MenuItem mMenuItem = menu.getItem(i);

            SubMenu subMenu = mMenuItem.getSubMenu();
            if (subMenu!=null && subMenu.size()>0) {
                for (int j=0; j<subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            applyFontToMenuItem(mMenuItem);
            if (i == RentalEngine.MENU_ITEM_ACCOUNT_INDICATOR) {
                applyColorToMenuItem(mMenuItem, isOk);
            }
        }
    }

    private void applyFontToMenuItem(MenuItem mMenuItem) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/SourceSansPro-Semibold.otf");
        SpannableString mNewTitle = new SpannableString(mMenuItem.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mMenuItem.setTitle(mNewTitle);
    }

    private void applyColorToMenuItem(MenuItem mMenuItem, boolean isOk) {
        SpannableString mNewTitle;
        if (isOk) {
            mNewTitle = new SpannableString(mMenuItem.getTitle().toString().replace("(1)", ""));
            mNewTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorAccent)), 0, mNewTitle.length(), 0);
        }
        else {
            mNewTitle = new SpannableString(mMenuItem.getTitle() + " (1)");
            mNewTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.red_700)), 0, mNewTitle.length(), 0);
        }
        mMenuItem.setTitle(mNewTitle);
    }

    private void unCheckMenuItems() {
        for (int i=0; i<7; i++) {
            menu.getItem(i).setChecked(false);
        }
    }

    private void loadUserData() {
        User tempUser = new User();

        MSharedPreferences mSharedPreferences = new MSharedPreferences(this, RentalEngine.RENTAL_SHARED_PREF);
        tempUser.setId(mSharedPreferences.getInt(RentalEngine.PREF_ID_KEY));
        tempUser.setFirstName(mSharedPreferences.getString(RentalEngine.PREF_FIRST_NAME_KEY));
        tempUser.setLastName(mSharedPreferences.getString(RentalEngine.PREF_LAST_NAME_KEY));
        tempUser.setEmail(mSharedPreferences.getString(RentalEngine.PREF_EMAIL_KEY));
        tempUser.setPhone(mSharedPreferences.getString(RentalEngine.PREF_PHONE_KEY));
        tempUser.setUser_profile(new UserProfile(mSharedPreferences.getString(RentalEngine.PREF_AVATAR), mSharedPreferences.getString(RentalEngine.PREF_CARD_ID), mSharedPreferences.getInt(RentalEngine.PREF_CARD_ID_STATUS)));
        RentalEngine.setUser(tempUser);

        if (tempUser.getUser_profile().getStatusId() == RentalEngine.APPROVED) {
            isCardIdConfirmed = true;
        }
        if (!tempUser.getUser_profile().getAvatar().equalsIgnoreCase("")) {
            hasAvatar = true;
        }
    }

    private void checkIfCardIdIsUploaded() {
        if (RentalEngine.getUser().getUser_profile() != null && RentalEngine.getUser().getUser_profile().getIdCard() != null && !RentalEngine.getUser().getUser_profile().getIdCard().equalsIgnoreCase("")) {
            // show nothing
        }
        else {
            new MaterialDialog.Builder(this)
                    .title(R.string.app_name)
                    .content(R.string.snackbar_no_card_id)
                    .positiveText(ok)
                    .show();
        }
    }

    private void openBrowseCarsFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.home_fragment_container, BrowseCarsFragment.newInstance(), currentFragmentTag)
                .commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(SetFragmentTagEvent event) {
        if (event != null) {
            setFragmentTag(event.getFragmentTag(), event.getToolbarTitle());
        }
    }

    private void setFragmentTag(String fragmentTag, String toolbarTitle) {
        currentFragmentTag = fragmentTag;
        homeCollapsingToolbarLayout.setTitle(toolbarTitle);
        swipeToRefresh.setEnabled(false);
        invalidateOptionsMenu();
        setToolbarImage(fragmentTag);
        setCheckedItem();
    }

    private void setToolbarImage(String fragmentTag) {
        switch (fragmentTag) {
            case RentalEngine.BROWSE_CARS_FRAGMENT_TAG:
                enableCollapsingToolbar(false, null);
                swipeToRefresh.setEnabled(true);
                break;
            case RentalEngine.VIEW_CAR_FRAGMENT_TAG:
                if (!isOnActivityResult) {
                    enableCollapsingToolbar(true, ContextCompat.getDrawable(this, R.drawable.car_photo_not_found));
                    if (RentalEngine.getSelectedCar() != null && RentalEngine.getSelectedCar().getPhotos() != null && RentalEngine.getSelectedCar().getPhotos().size() != 0) {
                        loadToolbarImage(RentalEngine.getSelectedCar().getPhotos().get(0).getPath());
                    }
                }
                else {
                    setOnActivityResult(false);
                }
                break;
            case RentalEngine.CAR_OWNER_FRAGMENT_TAG:
                enableCollapsingToolbar(true, ContextCompat.getDrawable(this, R.drawable.no_avatar));

                if (RentalEngine.getCarOwner() != null && RentalEngine.getCarOwner().getUser_profile() != null && RentalEngine.getCarOwner().getUser_profile().getAvatar() != null) {
                    if (accountImage != null) {
                        accountImage.setVisibility(View.VISIBLE);
                    }
                    loadBluredToolbarImage(RentalEngine.getCarOwner().getUser_profile().getAvatar());
                    loadAccountImage(RentalEngine.getCarOwner().getUser_profile().getAvatar());
                }
                break;
            case RentalEngine.DASHBOARD_FRAGMENT_TAG:
                enableCollapsingToolbar(false, null);
                swipeToRefresh.setEnabled(true);
                break;
            case RentalEngine.MESSAGES_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.LIST_YOUR_CAR_SECOND_STEP_FRAGMENT_TAG:
                enableCollapsingToolbar(true, ContextCompat.getDrawable(this, R.drawable.lyc_toolbar_car_1));
                break;
            case RentalEngine.LIST_YOUR_CAR_THIRD_STEP_FRAGMENT_TAG:
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.LIST_YOUR_CAR_FOURTH_STEP_FRAGMENT_TAG:
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.LIST_YOUR_CAR_FIFTH_STEP_FRAGMENT_TAG:
                enableCollapsingToolbar(true, ContextCompat.getDrawable(this, R.drawable.lyc_toolbar_car_1));
                break;
            case RentalEngine.TRIP_HISTORY_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.FAVORITES_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.ACCOUNT_FRAGMENT_TAG:
                if (!isOnActivityResult) {
                    enableCollapsingToolbar(true, ContextCompat.getDrawable(this, R.drawable.no_avatar));
                    if (hasAvatar) {
                        if (accountImage != null) {
                            accountImage.setVisibility(View.VISIBLE);
                        }
                        loadBluredToolbarImage(RentalEngine.getUser().getUser_profile().getAvatar());
                        loadAccountImage(RentalEngine.getUser().getUser_profile().getAvatar());
                    }
                }
                else {
                    setOnActivityResult(false);
                }
                break;
            case RentalEngine.UPDATE_PASSWORD_FRAGMENT_TAG:
                enableCollapsingToolbar(true, ContextCompat.getDrawable(this, R.drawable.no_avatar));
                if (hasAvatar) {
                    if (accountImage != null) {
                        accountImage.setVisibility(View.VISIBLE);
                    }
                    loadBluredToolbarImage(RentalEngine.getUser().getUser_profile().getAvatar());
                    loadAccountImage(RentalEngine.getUser().getUser_profile().getAvatar());
                }
                break;
            case RentalEngine.HOW_RENTAL_WORKS_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.ABOUT_US_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.SEND_FEEDBACK_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
            case RentalEngine.TERMS_OF_SERVICES_FRAGMENT_TAG:
                // todo
                enableCollapsingToolbar(false, null);
                break;
        }
    }

    private void enableCollapsingToolbar(boolean enable, Drawable drawable) {
        if (fabAction != null) {
            fabAction.setVisibility(View.GONE);
        }
        if (accountImage != null) {
            accountImage.setVisibility(View.GONE);
        }

        if (enable) {
            appBarLayout.setExpanded(true, true);
            toolbarImage.setVisibility(View.VISIBLE);
            toolbarImage.setImageDrawable(drawable);
            appBarLayoutBehavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return true;
                }
            });
            ViewCompat.setNestedScrollingEnabled(homeNestedScrollView, true);
        }
        else {
            appBarLayout.setExpanded(false, true);
            toolbarImage.setVisibility(View.GONE);
            appBarLayoutBehavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
            ViewCompat.setNestedScrollingEnabled(homeNestedScrollView, false);
        }
    }

    private void loadToolbarImage(String path) {
        Picasso.with(this)
                .load(path)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error_loading)
                .into(toolbarImage);
    }

    private void loadBluredToolbarImage(String path) {
        Picasso.with(this)
                .load(path)
                .transform(new BlurTransformation(this, 35, 1))
                .placeholder(R.drawable.loading)
                .error(R.drawable.error_loading)
                .into(toolbarImage);
    }

    private void loadBluredToolbarImage(String path, float rotation) {
        Picasso.with(this)
                .load(path)
                .rotate(rotation)
                .transform(new BlurTransformation(this, 35, 1))
                .placeholder(R.drawable.loading)
                .error(R.drawable.error_loading)
                .into(toolbarImage);
    }

    private void loadAccountImage(String path) {
        Picasso.with(this)
                .load(path)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error_loading)
                .into(accountImage);
    }

    private void loadAccountImage(String path, float rotation) {
        Picasso.with(this)
                .load(path)
                .rotate(rotation)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error_loading)
                .into(accountImage);
    }

    private void setCheckedItem() {
        switch (currentFragmentTag) {
            case RentalEngine.BROWSE_CARS_FRAGMENT_TAG:
                menu.getItem(0).setChecked(true);
                break;
            case RentalEngine.DASHBOARD_FRAGMENT_TAG:
                menu.getItem(1).setChecked(true);
                break;
            case RentalEngine.MESSAGES_FRAGMENT_TAG:
                menu.getItem(2).setChecked(true);
                break;
            case RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG:
                menu.getItem(3).setChecked(true);
                break;
            case RentalEngine.TRIP_HISTORY_FRAGMENT_TAG:
                menu.getItem(4).setChecked(true);
                break;
            case RentalEngine.FAVORITES_FRAGMENT_TAG:
                menu.getItem(5).setChecked(true);
                break;
            case RentalEngine.ACCOUNT_FRAGMENT_TAG:
                menu.getItem(6).setChecked(true);
                break;
        }
    }

    private void openNewFragment(Fragment mFragment, String fragmentTag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (!fragmentTag.equalsIgnoreCase(RentalEngine.DASHBOARD_FRAGMENT_TAG)) {
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        }
        transaction.replace(R.id.home_fragment_container, mFragment);
        transaction.commit();
    }

    private void logOutUser() {
        clearSharedPreferences();
        clearCachedData();
        openIntroActivity();
    }

    private void clearSharedPreferences() {
        MSharedPreferences mSharedPreferences = new MSharedPreferences(this, RentalEngine.RENTAL_SHARED_PREF);
        mSharedPreferences.clear();
    }

    private void clearCachedData() {
        RentalEngine.setUser(null);
        RentalEngine.setCarsList(null);
        RentalEngine.setSelectedCar(null);
        RentalEngine.setCarOwner(null);
    }

    private void openIntroActivity() {
        Intent introIntent = new Intent(this, IntroActivity.class);
        introIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.startActivity(introIntent);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OpenNewFragmentEvent event) {
        if (event != null) {
            openNewFragment(event.getFragment(), event.getTag());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ShowSnackBarMessageEvent event) {
        if (event != null) {
            showSnackBarMessage(event.getMessage());
        }
    }

    private void showSnackBarMessage(String message) {
        snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(DismissSnackBarEvent event) {
        if (event != null) {
            dismissSnackbar();
        }
    }

    private void dismissSnackbar() {
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(UpdateMenuItemsEvent event) {
        if (event != null) {
            updateMenuItem(event.getMenuItemIndicator());
        }
    }

    private void updateMenuItem(int menuItemIndicator) {
        switch (menuItemIndicator) {
            case RentalEngine.MENU_ITEM_ACCOUNT_INDICATOR:
                isCardIdConfirmed = true;
                prepareMenuItemFonts(isCardIdConfirmed);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(UpdateNavHeaderEvent event) {
        if (event != null) {
            loadUserData();
            updateNavHeaderView();
        }
    }

    private void updateNavHeaderView() {
        if (RentalEngine.getUser() != null) {
            userFirstLastName.setText(RentalEngine.getUser().getFirstName() + " " + RentalEngine.getUser().getLastName());
        }
        if (hasAvatar) {
            Picasso.with(this)
                    .load(RentalEngine.getUser().getUser_profile().getAvatar())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.error_loading)
                    .into(userImage);
        }
    }

    private void showDialogToChangeUserPhoto() {
        new MaterialDialog.Builder(this)
                .title(R.string.account_change_photo_label)
                .items(R.array.account_change_photo_arrays)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            requestToChangeUserPhoto(RentalEngine.REQUEST_IMAGE_CAPTURE);
                        }
                        else if (which == 1) {
                            requestToChangeUserPhoto(RentalEngine.PICK_IMAGE_REQUEST);
                        }
                        return true;
                    }
                })
                .positiveText(R.string.account_confirm_option)
                .negativeText(R.string.account_cancel_option)
                .show();
    }

    private void requestToChangeUserPhoto(int requestCode) {
        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
            }
            else {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
            }
            else {
                takeAPicture();
            }
        }
    }

    private void chooseImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RentalEngine.PICK_IMAGE_REQUEST);
    }

    private void takeAPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mUri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mUri);

        startActivityForResult(intent, RentalEngine.REQUEST_IMAGE_CAPTURE);
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), String.valueOf(getResources().getString(R.string.app_image_folder_name)));

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        mFileName = "IMG_"+ timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + mFileName);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CloseKeyboardEvent event) {
        if (event != null) {
            closeKeyboard();
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(BackButtonEvent event) {
        if (event != null) {
            closeKeyboard();
            onBackPressed();
        }
    }

    private void askToExitFromCarListing() {
        new MaterialDialog.Builder(this)
                .title(R.string.lyc_do_you_want_to_exit_title)
                .content(R.string.lyc_do_you_want_to_exit_content)
                .positiveText(R.string.home_yes_option)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        openBrowseCarsFragment();
                        if (currentFragmentTag == RentalEngine.LIST_YOUR_CAR_THIRD_STEP_FRAGMENT_TAG ||
                                currentFragmentTag == RentalEngine.LIST_YOUR_CAR_FOURTH_STEP_FRAGMENT_TAG ||
                                currentFragmentTag == RentalEngine.LIST_YOUR_CAR_FIFTH_STEP_FRAGMENT_TAG) {
                            showSnackBarMessage(getResources().getString(R.string.snackbar_car_listing_suspended));
                        }
                    }
                })
                .negativeText(R.string.home_no_option)
                .show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(ShowFabButtonEvent event) {
        if (event != null) {
            if (fabAction != null) {
                fabAction.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(StopRefreshingEvent event) {
        if (event != null) {
            if (swipeToRefresh != null) {
                swipeToRefresh.setRefreshing(false);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(OpenViewCarEvent event) {
        if (event != null && event.getCar() != null) {
            RentalEngine.setSelectedCar(event.getCar());
            ViewCarFragment viewCarFragment = new ViewCarFragment();
            RentalEngine.setViewCarMode(event.getMode());
            setOnActivityResult(false);
            openNewFragment(viewCarFragment, RentalEngine.VIEW_CAR_FRAGMENT_TAG);
        }
    }

    private void showSearchDatePicker(final boolean firstTime) {
        if (llSearchInfo != null) {
            llSearchInfo.setVisibility(View.GONE);
        }
        if (startYear == -1) {
            Calendar c = Calendar.getInstance();
            startYear = c.get(Calendar.YEAR);
            startMonth = c.get(Calendar.MONTH);
            startDay = c.get(Calendar.DAY_OF_MONTH);
        }
        datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        DateTime tempDate = new DateTime().withYear(year).withMonthOfYear(monthOfYear+1).withDayOfMonth(dayOfMonth);
                        boolean isBeforeNow = false;
                        boolean isAfterOneYear = false;

                        if (tempDate.minusDays(1).plusSeconds(15).isBeforeNow()) {
                            isBeforeNow = true;
                            tvSearchInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_searching_date_invalid)));
                            llSearchInfo.setVisibility(View.VISIBLE);
                        }
                        if (tempDate.minusYears(1).isAfterNow()) {
                            isAfterOneYear = true;
                            tvSearchInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_date_one_year_exceeded)));
                            llSearchInfo.setVisibility(View.VISIBLE);
                        }

                        if (!isBeforeNow && !isAfterOneYear) {
                            if (firstTime) {
                                dtStartDate = tempDate;
                                startYear = year;
                                startMonth = monthOfYear;
                                startDay = dayOfMonth;
                                if (isEndDateSet) {
                                    if (dtStartDate.isBefore(dtEndDate)) {
                                        isStartDateSet = true;
                                    }
                                    else {
                                        tvSearchInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_start_date_invalid)));
                                        llSearchInfo.setVisibility(View.VISIBLE);
                                    }
                                }
                                else {
                                    isStartDateSet = true;
                                }

                                if (isStartDateSet && tvStartDateValue != null) {
                                    tvStartDateValue.setText(getDateToString(dtStartDate));
                                }
                            }
                            else {
                                dtEndDate = tempDate.withHourOfDay(23).withMinuteOfHour(59);
                                if (isStartDateSet) {
                                    if (dtStartDate.isBefore(dtEndDate)) {
                                        isEndDateSet = true;
                                    }
                                    else {
                                        tvSearchInfo.setText(String.valueOf(getResources().getString(R.string.snackbar_end_date_invalid)));
                                        llSearchInfo.setVisibility(View.VISIBLE);
                                    }
                                }
                                else {
                                    isEndDateSet = true;
                                }

                                if (isEndDateSet && tvEndDateValue != null) {
                                    tvEndDateValue.setText(getDateToString(dtEndDate));
                                }
                            }
                        }
                    }
                }, startYear, startMonth, startDay);
        datePickerDialog.show();
    }

    private String getDateToString(DateTime mDateTime) {
        return String.valueOf(mDateTime.getDayOfMonth() + "/" + String.format(Locale.ENGLISH,"%02d", mDateTime.getMonthOfYear()) + "/" + mDateTime.getYear());
    }

    private void showSearchFilters() {
        initializeFlags();

        boolean wrapInScrollView = true;
        MaterialDialog mMaterialDialog = new MaterialDialog.Builder(this)
                .title(R.string.search_dialog_title)
                .customView(R.layout.search_filters_dialog, wrapInScrollView)
                .positiveText(R.string.search_dialog_positive_button)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if (proceedWithSearch()) {
                            EventBus.getDefault().post(new SearchFiltersEvent(prepareFilters()));
                        }
                    }
                })
                .build();

        View view = mMaterialDialog.getCustomView();
        if (view != null) {
            llSearchInfo = (LinearLayout) view.findViewById(R.id.llSearchInfo);
            tvSearchInfo = (TextView) view.findViewById(R.id.tvSearchInfo);
            ImageView ivSetStartDate = (ImageView) view.findViewById(R.id.ivSearchStartDate);
            ivSetStartDate.setOnClickListener(this);
            tvStartDateValue = (TextView) view.findViewById(R.id.tvSearchStartDateValue);
            ImageView ivSetEndDate = (ImageView) view.findViewById(R.id.ivSearchEndDate);
            ivSetEndDate.setOnClickListener(this);
            tvEndDateValue = (TextView) view.findViewById(R.id.tvSearchEndDateValue);

            final EditText etWarrantyPrice = (EditText) view.findViewById(R.id.etSearchWarratyPriceValue);
            etWarrantyPrice.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                    String tempPrice = s.toString();
                    if (tempPrice.equalsIgnoreCase("")) {
                        warrantyPrice = -1;
                    }
                    else {
                        warrantyPrice = Integer.parseInt(tempPrice);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) { }
            });

            if (etWarrantyPrice != null) {
                loadManufacturerSpinner(view, etWarrantyPrice);
                loadModelSpinner(view, etWarrantyPrice);
                loadProductionYearSpinner(view, etWarrantyPrice);
                loadOdometerSpinner(view, etWarrantyPrice);
                loadTransmissionSpinner(view, etWarrantyPrice);
                loadBodyTypeSpinner(view, etWarrantyPrice);
                loadAirConditionerSpinner(view, etWarrantyPrice);
                loadAddressSpinner(view, etWarrantyPrice);
            }
        }

        mMaterialDialog.show();
    }

    private void initializeFlags() {
        manufacturerPosition = 0;
        manufacturerChoosed = "";
        modelPosition = 0;
        modelChoosed = "";
        productionYearPosition = 0;
        productionYearChoosed = "";
        odometerPosition = 0;
        odometerChoosed = "";
        transmissionPosition = 0;
        transmissionChoosed = "";
        bodyTypePosition = 0;
        bodyTypeChoosed = "";
        warrantyPrice = -1;
        airConditionerPosition = 0;
        airConditionerChoosed = -1;
        addressPosition = 0;
        addressChoosed = "";
        isStartDateSet = false;
        isEndDateSet = false;
        startDay = -1;
        startMonth = -1;
        startYear = -1;
    }

    private boolean proceedWithSearch() {
        if (!isStartDateSet &&
                !isEndDateSet &&
                manufacturerPosition == 0 &&
                modelPosition == 0 &&
                productionYearPosition == 0 &&
                odometerPosition == 0 &&
                transmissionPosition == 0 &&
                bodyTypePosition == 0 &&
                airConditionerPosition == 0 &&
                warrantyPrice == -1 &&
                addressPosition == 0) {
            return false;
        }
        return true;
    }

    private List<Chip> prepareFilters() {
        List<Chip> filters = new ArrayList<>();
        //String querySearch = "";

        /*if (RentalEngine.getUser() != null) {
            querySearch = "user_id=" + RentalEngine.getUser().getId();
        }
        else {
            querySearch = "user_id=0";
        }*/
        if (isStartDateSet && isEndDateSet) {
            filters.add(new Filter(getDateToString(dtStartDate) + " - " + getDateToString(dtEndDate), "&available_start_date=" + dtStartDate.getYear() + "-" + String.format(Locale.ENGLISH,"%02d", dtStartDate.getMonthOfYear()) + "-" + String.format(Locale.ENGLISH,"%02d", dtStartDate.getDayOfMonth()) + "&available_end_date=" + dtEndDate.getYear() + "-" + String.format(Locale.ENGLISH,"%02d", dtEndDate.getMonthOfYear()) + "-" + String.format(Locale.ENGLISH,"%02d", dtEndDate.getDayOfMonth())));
        }
        else {
            if (isStartDateSet) {
                filters.add(new Filter(getResources().getString(R.string.search_start_date_label) + getDateToString(dtStartDate), "&available_start_date=" + dtStartDate.getYear() + "-" + String.format(Locale.ENGLISH,"%02d", dtStartDate.getMonthOfYear()) + "-" + String.format(Locale.ENGLISH,"%02d", dtStartDate.getDayOfMonth())));
            }
            if (isEndDateSet) {
                filters.add(new Filter(getResources().getString(R.string.search_end_date_label) + getDateToString(dtEndDate), "&available_end_date=" + dtEndDate.getYear() + "-" + String.format(Locale.ENGLISH,"%02d", dtEndDate.getMonthOfYear()) + "-" + String.format(Locale.ENGLISH,"%02d", dtEndDate.getDayOfMonth())));
            }
        }
        if (manufacturerPosition != 0) {
            filters.add(new Filter(manufacturerChoosed, "&manufacturer=" + manufacturerChoosed));
        }
        if (modelPosition != 0) {
            filters.add(new Filter(modelChoosed, "&model=" + modelChoosed));
        }
        if (productionYearPosition != 0) {
            filters.add(new Filter(productionYearChoosed, "&production_year=" + productionYearChoosed));
        }
        if (odometerPosition != 0) {
            filters.add(new Filter(odometerChoosed, "&odometer=" + odometerChoosed));
        }
        if (transmissionPosition != 0) {
            filters.add(new Filter(transmissionChoosed, "&transmission=" + transmissionChoosed));
        }
        if (bodyTypePosition != 0) {
            filters.add(new Filter(bodyTypeChoosed, "&body_type=" + bodyTypeChoosed));
        }
        if (airConditionerPosition != 0) {
            String tempAC = getResources().getString(R.string.bc_not_specified_value);
            if (airConditionerChoosed == 0) {
                tempAC = getResources().getString(R.string.bc_air_conditioner_false_filter_value);
            }
            else if (airConditionerChoosed == 1) {
                tempAC = getResources().getString(R.string.bc_air_conditioner_true_filter_value);
            }
            filters.add(new Filter(tempAC, "&has_ac=" + airConditionerChoosed));
        }
        if (warrantyPrice != -1) {
            filters.add(new Filter("≤ " + warrantyPrice + getResources().getString(R.string.bc_price_unit_label), "&warranty_price=" + warrantyPrice));
        }
        if (addressPosition != 0) {
            filters.add(new Filter(addressChoosed, "&address=" + addressChoosed));
        }

        return filters;
    }

    private void loadManufacturerSpinner(final View mView, final EditText etWarranty) {
        mManufacturers = prepareManufacturers();

        MaterialSpinner spinner = (MaterialSpinner) mView.findViewById(R.id.sSearchManufacturer);
        spinner.setItems(mManufacturers);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                manufacturerPosition = position;
                manufacturerChoosed = item;
                modelPosition = 0;
                modelChoosed = "";
                loadModelSpinner(mView, etWarranty);
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareManufacturers() {
        ArrayList<String> tempManufacturers = new ArrayList<>();
        tempManufacturers.add(getResources().getString(R.string.search_dialog_choose));
        if (RentalEngine.getMakes() == null || RentalEngine.getMakes().getMakes() == null) {
            callGetMakesApi();
        }
        else {
            ArrayList<Make> tempMakes = RentalEngine.getMakes().getMakes();
            for (Make iMake : tempMakes) {
                tempManufacturers.add(iMake.getName());
            }
        }
        return tempManufacturers;
    }

    private void callGetMakesApi() {
        String urlApi = RentalEngine.RENTAL_API_EDMUNDS_CAR_MAKES_ROUTE;
        new HttpGetMakesAsync().execute(urlApi);
    }

    private class HttpGetMakesAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private Makes makes;

        @Override
        protected String doInBackground(String... uri) {
            try {
                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    Gson gson = new Gson();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    makes = gson.fromJson(reader, Makes.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "HomeActivity HttpGetMakesAsync doInBackground Exception: " + e.toString());
                makes = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (makes != null && makes.getMakes() != null) {
                    RentalEngine.setMakes(makes);
                    for (Make iMake : makes.getMakes()) {
                        mManufacturers.add(iMake.getName());
                    }
                }
            }
        }
    }

    private void loadModelSpinner(View view, final EditText etWarranty) {
        prepareModels();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchModel);
        spinner.setItems(mModels);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                modelPosition = position;
                modelChoosed = item;
                etWarranty.clearFocus();
            }
        });
    }

    private void prepareModels() {
        mModels = new ArrayList<>();
        mModels.add(getResources().getString(R.string.search_dialog_choose));
        if (manufacturerPosition != 0) {
            ArrayList<Model> models = RentalEngine.getMakes().getMakes().get(manufacturerPosition -1).getModels();
            for (Model iModel : models) {
                mModels.add(iModel.getName());
            }
        }
    }

    private void loadProductionYearSpinner(View view, final EditText etWarranty) {
        ArrayList<String> mProductionYearsList = prepareProductionYears();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchProductionYear);
        spinner.setItems(mProductionYearsList);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                productionYearPosition = position;
                productionYearChoosed = item;
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareProductionYears() {
        ArrayList<String> tempYears = new ArrayList<>();
        tempYears.add(getResources().getString(R.string.search_dialog_choose));

        Calendar calendar = Calendar.getInstance();
        int actualYear = calendar.get(Calendar.YEAR);
        int thirtyYearsEarlier = actualYear - 30;
        for (int i=actualYear; i>=thirtyYearsEarlier; i--) {
            tempYears.add(String.valueOf(i));
        }

        return tempYears;
    }

    private void loadOdometerSpinner(View view, final EditText etWarranty) {
        ArrayList<String> mOdometersList = prepareOdometers();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchOdometer);
        spinner.setItems(mOdometersList);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                odometerPosition = position;
                odometerChoosed = item;
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareOdometers() {
        ArrayList<String> tempOdometers = new ArrayList<>();
        tempOdometers.add(getResources().getString(R.string.search_dialog_choose));
        tempOdometers.add("0-50,000 km");
        tempOdometers.add("50,000-100,000 km");
        tempOdometers.add("100,000-150,000 km");
        tempOdometers.add("150,000+ km");
        return tempOdometers;
    }

    private void loadTransmissionSpinner(View view, final EditText etWarranty) {
        ArrayList<String> mTransmissionsList = prepareTransmissions();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchTransmission);
        spinner.setItems(mTransmissionsList);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                transmissionPosition = position;
                transmissionChoosed = item;
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareTransmissions() {
        ArrayList<String> tempTransmissions = new ArrayList<>();
        tempTransmissions.add(getResources().getString(R.string.search_dialog_choose));
        tempTransmissions.add("Manuale");
        tempTransmissions.add("Automatike");
        return tempTransmissions;
    }

    private void loadBodyTypeSpinner(View view, final EditText etWarranty) {
        ArrayList<String> mBodyTypes = prepareBodyTypes();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchBodyType);
        spinner.setItems(mBodyTypes);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                bodyTypePosition = position;
                bodyTypeChoosed = item;
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareBodyTypes() {
        ArrayList<String> tempBodyTypes = new ArrayList<>();
        tempBodyTypes.add(getResources().getString(R.string.search_dialog_choose));
        tempBodyTypes.add("2 dyer");
        tempBodyTypes.add("4 dyer");
        return tempBodyTypes;
    }

    private void loadAirConditionerSpinner(View view, final EditText etWarranty) {
        ArrayList<String> mAirConditionerList = prepareAirConditioner();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchAirConditioner);
        spinner.setItems(mAirConditionerList);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                airConditionerPosition = position;
                if (position == 1) {
                    airConditionerChoosed = 1;
                }
                else if (position == 2) {
                    airConditionerChoosed = 0;
                }
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareAirConditioner() {
        ArrayList<String> tempAirConditioner = new ArrayList<>();
        tempAirConditioner.add(getResources().getString(R.string.search_dialog_choose));
        tempAirConditioner.add(getResources().getString(R.string.home_yes_option));
        tempAirConditioner.add(getResources().getString(R.string.home_no_option));
        return tempAirConditioner;
    }

    private void loadAddressSpinner(View view, final EditText etWarranty) {
        mAddresses = prepareAddresses();

        MaterialSpinner spinner = (MaterialSpinner) view.findViewById(R.id.sSearchAddress);
        spinner.setItems(mAddresses);
        spinner.setArrowColor(ContextCompat.getColor(this, R.color.colorAccent));
        spinner.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                addressPosition = position;
                addressChoosed = item;
                etWarranty.clearFocus();
            }
        });
    }

    private ArrayList<String> prepareAddresses() {
        ArrayList<String> tempAddresses = new ArrayList<>();
        tempAddresses.add(getResources().getString(R.string.search_dialog_choose));
        callGetCitiesApi();
        return tempAddresses;
    }

    private void callGetCitiesApi() {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CITIES_ROUTE + RentalEngine.RENTAL_API_COUNTRY_ALBANIA;
        new HttpGetCitiesAsync().execute(urlApi);
    }

    private class HttpGetCitiesAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private ArrayList<City> tempCities;

        @Override
        protected String doInBackground(String... uri) {
            try {
                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    Gson gson = new Gson();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    Type collectionType = new TypeToken<Collection<City>>() {}.getType();
                    tempCities = gson.fromJson(reader, collectionType);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "HomeActivity HttpGetCitiesAsync doInBackground Exception: " + e.toString());
                tempCities = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (tempCities != null && tempCities.size() != 0) {
                    for (City iCity : tempCities) {
                        mAddresses.add(iCity.getName());
                    }
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(RefreshToolbarImageEvent event) {
        if (event != null) {
            loadToolbarImage(event.getImagePath());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(SetDefaultCarImageEvent event) {
        if (event != null) {
            toolbarImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.car_photo_not_found));
        }
    }

    public void setOnActivityResult(boolean is) {
        isOnActivityResult = is;
    }

    public void callOnIddleCPB() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(750);
                } catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "HomeActivity thread Exception: " + e.toString());
                } finally {
                    EventBus.getDefault().post(new OnIddleCPBEvent());
                }
            }
        };
        timer.start();
    }
}
