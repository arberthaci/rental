package al.inovacion.rental.home.interact;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.engine.RentalEngine;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Arbër Thaçi on 17-03-24.
 * Email: arberlthaci@gmail.com
 */

public class TripHistoryFragment extends Fragment {

    public TripHistoryFragment() {}

    public static TripHistoryFragment newInstance() {
        return new TripHistoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.TRIP_HISTORY_FRAGMENT_TAG, getResources().getString(R.string.menu_item_trip_history)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trip_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //initializeComponents(view);
        //afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
