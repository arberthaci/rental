package al.inovacion.rental.home.interact.list_your_car;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.engine.RentalEngine;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Arbër Thaçi on 17-03-24.
 * Email: arberlthaci@gmail.com
 */

public class ListYourCarFragment extends Fragment {

    private TextView firstInfo;
    private TextView secondInfo;
    private TextView thirdInfo;

    public ListYourCarFragment() {}

    public static ListYourCarFragment newInstance() {
        return new ListYourCarFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.LIST_YOUR_CAR_FRAGMENT_TAG, getResources().getString(R.string.menu_item_list_your_car)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_your_car, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().post(new CloseKeyboardEvent());
        initializeComponents(view);
        runAnimation();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        firstInfo = (TextView) view.findViewById(R.id.tvLYCFirstInfo);
        secondInfo = (TextView) view.findViewById(R.id.tvLYCSecondInfo);
        thirdInfo = (TextView) view.findViewById(R.id.tvLYCThirdInfo);
    }

    private void runAnimation() {
        final Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.textview_fade);
        animation.reset();

        firstInfo.clearAnimation();
        firstInfo.setVisibility(View.VISIBLE);
        firstInfo.startAnimation(animation);

        final Handler handlerForSecond = new Handler();
        handlerForSecond.postDelayed(new Runnable() {
            @Override
            public void run() {
                firstInfo.clearAnimation();
                secondInfo.clearAnimation();
                secondInfo.setVisibility(View.VISIBLE);
                secondInfo.startAnimation(animation);
            }
        }, animation.getDuration());

        final Handler handlerForThird = new Handler();
        handlerForThird.postDelayed(new Runnable() {
            @Override
            public void run() {
                firstInfo.clearAnimation();
                secondInfo.clearAnimation();
                thirdInfo.clearAnimation();
                thirdInfo.setVisibility(View.VISIBLE);
                thirdInfo.startAnimation(animation);
            }
        }, animation.getDuration() + animation.getDuration());
    }
}
