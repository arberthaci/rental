package al.inovacion.rental.home.interact.list_your_car;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.OpenNewFragmentEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.ShowSnackBarMessageEvent;
import al.inovacion.rental.data.models.api.CarResponse;
import al.inovacion.rental.data.models.app.ImageObject;
import al.inovacion.rental.engine.Functions;
import al.inovacion.rental.engine.RentalEngine;
import al.inovacion.rental.engine.circularprogressbutton.CircularProgressButton;
import al.inovacion.rental.engine.circularprogressbutton.OnAnimationEndListener;
import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Arbër Thaçi on 17-04-03.
 * Email: arberlthaci@gmail.com
 */

public class ListYourCarFourthStepFragment extends Fragment implements View.OnClickListener {

    private ImageView chooseImageBtnLicense;
    private ImageView takePictureBtnLicense;
    private ImageView ivLicenseStatus;
    private TextView tvInfoLicense;
    private TextView tvLicenseLabel;
    private ImageView carLicenseImage;

    private ImageView chooseImageBtnInsurance;
    private ImageView takePictureBtnInsurance;
    private ImageView ivInsuranceStatus;
    private TextView tvInfoInsurance;
    private TextView tvInsuranceLabel;
    private ImageView carInsuranceImage;

    private ImageView chooseImageBtnKolaudim;
    private ImageView takePictureBtnKolaudim;
    private ImageView ivKolaudimStatus;
    private TextView tvInfoKolaudim;
    private TextView tvKolaudimLabel;
    private ImageView carKolaudimImage;

    private CircularProgressButton nextButton;

    private int carId = -1;
    private int totalItemsToUploads = 0;

    private boolean licenseUploaded = false;
    private Uri mUriLicense;
    private String mFileNameLicense = "";
    private String mPathLicense = "";

    private boolean insuranceUploaded = false;
    private Uri mUriInsurance;
    private String mFileNameInsurance = "";
    private String mPathInsurance = "";

    private boolean kolaudimUploaded = false;
    private Uri mUriKolaudim;
    private String mFileNameKolaudim = "";
    private String mPathKolaudim = "";

    private int px;
    private int py;

    private int selectedOption = -1;
    private static final int LICENSE_OPTION = 1;
    private static final int INSURANCE_OPTION = 2;
    private static final int KOLAUDIM_OPTION = 3;

    public ListYourCarFourthStepFragment() {}

    public static ListYourCarFourthStepFragment newInstance() {
        return new ListYourCarFourthStepFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.LIST_YOUR_CAR_FOURTH_STEP_FRAGMENT_TAG, getResources().getString(R.string.lyc_toolbar_fourth_step)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_your_car_fourth_step, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());
        py= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 175, getResources().getDisplayMetrics());

        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLYCFSChooseImageFromGalleryCarLicense:
                selectedOption = LICENSE_OPTION;
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
                }
                else {
                    chooseImagesFromGallery();
                }
                break;
            case R.id.ivLYCFSTakeAPictureCarLicense:
                selectedOption = LICENSE_OPTION;
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
                }
                else {
                    takeAPicture();
                }
                break;
            case R.id.ivLYCFSChooseImageFromGalleryCarInsurance:
                selectedOption = INSURANCE_OPTION;
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
                }
                else {
                    chooseImagesFromGallery();
                }
                break;
            case R.id.ivLYCFSTakeAPictureCarInsurance:
                selectedOption = INSURANCE_OPTION;
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
                }
                else {
                    takeAPicture();
                }
                break;
            case R.id.ivLYCFSChooseImageFromGalleryCarKolaudim:
                selectedOption = KOLAUDIM_OPTION;
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE }, RentalEngine.PICK_IMAGE_REQUEST);
                }
                else {
                    chooseImagesFromGallery();
                }
                break;
            case R.id.ivLYCFSTakeAPictureCarKolaudim:
                selectedOption = KOLAUDIM_OPTION;
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, RentalEngine.REQUEST_IMAGE_CAPTURE);
                }
                else {
                    takeAPicture();
                }
                break;
            case R.id.cpbLYCFSNext:
                tryToUploadCarDocumentation();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == RentalEngine.PICK_IMAGE_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseImagesFromGallery();
            }
        }
        else if (requestCode == RentalEngine.REQUEST_IMAGE_CAPTURE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                takeAPicture();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RentalEngine.PICK_IMAGE_REQUEST:
                    if (data != null && data.getData() != null) {
                        float tempRotation = 0;
                        switch (selectedOption) {
                            case LICENSE_OPTION:
                                mUriLicense = data.getData();
                                mPathLicense = RentalEngine.getRealPathFromURI(getContext(), mUriLicense);
                                mFileNameLicense = mPathLicense.substring(mPathLicense.lastIndexOf("/")+1);
                                tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUriLicense);
                                loadPicture(mUriLicense, tempRotation, selectedOption);
                                break;
                            case INSURANCE_OPTION:
                                mUriInsurance = data.getData();
                                mPathInsurance = RentalEngine.getRealPathFromURI(getContext(), mUriInsurance);
                                mFileNameInsurance = mPathInsurance.substring(mPathInsurance.lastIndexOf("/")+1);
                                tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUriInsurance);
                                loadPicture(mUriInsurance, tempRotation, selectedOption);
                                break;
                            case KOLAUDIM_OPTION:
                                mUriKolaudim = data.getData();
                                mPathKolaudim = RentalEngine.getRealPathFromURI(getContext(), mUriKolaudim);
                                mFileNameKolaudim = mPathKolaudim.substring(mPathKolaudim.lastIndexOf("/")+1);
                                tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUriKolaudim);
                                loadPicture(mUriKolaudim, tempRotation, selectedOption);
                                break;
                        }
                    }
                    break;
                case RentalEngine.REQUEST_IMAGE_CAPTURE:
                    float tempRotation = 0;
                    switch (selectedOption) {
                        case LICENSE_OPTION:
                            tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUriLicense);
                            mPathLicense = mUriLicense.getPath();
                            loadPicture(mUriLicense, tempRotation, selectedOption);
                            break;
                        case INSURANCE_OPTION:
                            tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUriInsurance);
                            mPathInsurance = mUriInsurance.getPath();
                            loadPicture(mUriInsurance, tempRotation, selectedOption);
                            break;
                        case KOLAUDIM_OPTION:
                            tempRotation = RentalEngine.getCorrectRotationFromOrientation(getContext(), mUriKolaudim);
                            mPathKolaudim = mUriKolaudim.getPath();
                            loadPicture(mUriKolaudim, tempRotation, selectedOption);
                            break;
                    }
                    break;
            }
            selectedOption = -1;
        }
    }

    private OnAnimationEndListener onAnimationEndListener = new OnAnimationEndListener() {
        @Override
        public void onAnimationEnd() {
            nextButton.clearAnimation();
        }
    };

    private void initializeComponents(View view) {
        chooseImageBtnLicense = (ImageView) view.findViewById(R.id.ivLYCFSChooseImageFromGalleryCarLicense);
        takePictureBtnLicense = (ImageView) view.findViewById(R.id.ivLYCFSTakeAPictureCarLicense);
        ivLicenseStatus = (ImageView) view.findViewById(R.id.ivLYCFSCarLicenseStatus);
        tvInfoLicense = (TextView) view.findViewById(R.id.tvLYCFSCarLicenseInfo);
        tvLicenseLabel = (TextView) view.findViewById(R.id.tvLYCFSCarLicenseLabel);
        carLicenseImage = (ImageView) view.findViewById(R.id.ivLYCFSCarLicense);

        chooseImageBtnInsurance = (ImageView) view.findViewById(R.id.ivLYCFSChooseImageFromGalleryCarInsurance);
        takePictureBtnInsurance = (ImageView) view.findViewById(R.id.ivLYCFSTakeAPictureCarInsurance);
        ivInsuranceStatus = (ImageView) view.findViewById(R.id.ivLYCFSCarInsuranceStatus);
        tvInfoInsurance = (TextView) view.findViewById(R.id.tvLYCFSCarInsuranceInfo);
        tvInsuranceLabel = (TextView) view.findViewById(R.id.tvLYCFSCarInsuranceLabel);
        carInsuranceImage = (ImageView) view.findViewById(R.id.ivLYCFSCarInsurance);

        chooseImageBtnKolaudim = (ImageView) view.findViewById(R.id.ivLYCFSChooseImageFromGalleryCarKolaudim);
        takePictureBtnKolaudim = (ImageView) view.findViewById(R.id.ivLYCFSTakeAPictureCarKolaudim);
        ivKolaudimStatus = (ImageView) view.findViewById(R.id.ivLYCFSCarKolaudimStatus);
        tvInfoKolaudim = (TextView) view.findViewById(R.id.tvLYCFSCarKolaudimInfo);
        tvKolaudimLabel = (TextView) view.findViewById(R.id.tvLYCFSCarKolaudimLabel);
        carKolaudimImage = (ImageView) view.findViewById(R.id.ivLYCFSCarKolaudim);

        nextButton = (CircularProgressButton) view.findViewById(R.id.cpbLYCFSNext);
    }

    private void afterInitialization() {
        chooseImageBtnLicense.setOnClickListener(this);
        takePictureBtnLicense.setOnClickListener(this);

        chooseImageBtnInsurance.setOnClickListener(this);
        takePictureBtnInsurance.setOnClickListener(this);

        chooseImageBtnKolaudim.setOnClickListener(this);
        takePictureBtnKolaudim.setOnClickListener(this);

        nextButton.setIndeterminateProgressMode(true);
        nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
        nextButton.setOnClickListener(this);

        ViewTreeObserver vto = nextButton.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                if (Functions.hasJellyBeanMR1()) {
                    nextButton.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    nextButton.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    private void chooseImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RentalEngine.PICK_IMAGE_REQUEST);
    }

    private void takeAPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        switch (selectedOption) {
            case LICENSE_OPTION:
                mUriLicense = Uri.fromFile(getOutputMediaFile(selectedOption));
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mUriLicense);
                break;
            case INSURANCE_OPTION:
                mUriInsurance = Uri.fromFile(getOutputMediaFile(selectedOption));
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mUriInsurance);
                break;
            case KOLAUDIM_OPTION:
                mUriKolaudim = Uri.fromFile(getOutputMediaFile(selectedOption));
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mUriKolaudim);
                break;
        }

        startActivityForResult(intent, RentalEngine.REQUEST_IMAGE_CAPTURE);
    }

    private File getOutputMediaFile(int option){
        String tempFilename = "";
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), String.valueOf(getResources().getString(R.string.app_image_folder_name)));

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        tempFilename = "IMG_"+ timeStamp + ".jpg";

        switch (option) {
            case LICENSE_OPTION:
                mFileNameLicense = tempFilename;
                break;
            case INSURANCE_OPTION:
                mFileNameInsurance = tempFilename;
                break;
            case KOLAUDIM_OPTION:
                mFileNameKolaudim = tempFilename;
                break;
        }

        return new File(mediaStorageDir.getPath() + File.separator + tempFilename);
    }

    private void loadPicture(Uri uri, float rotation, int option) {
        switch (option) {
            case LICENSE_OPTION:
                if (tvInfoLicense.getVisibility() == View.VISIBLE) {
                    tvInfoLicense.setVisibility(View.GONE);
                    carLicenseImage.setVisibility(View.VISIBLE);
                    tvLicenseLabel.setVisibility(View.VISIBLE);
                }

                Picasso.with(getActivity())
                        .load(uri)
                        .resize(px, py)
                        .centerCrop()
                        .rotate(rotation)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error_loading)
                        .into(carLicenseImage);
                break;
            case INSURANCE_OPTION:
                if (tvInfoInsurance.getVisibility() == View.VISIBLE) {
                    tvInfoInsurance.setVisibility(View.GONE);
                    carInsuranceImage.setVisibility(View.VISIBLE);
                    tvInsuranceLabel.setVisibility(View.VISIBLE);
                }

                Picasso.with(getActivity())
                        .load(uri)
                        .resize(px, py)
                        .centerCrop()
                        .rotate(rotation)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error_loading)
                        .into(carInsuranceImage);
                break;
            case KOLAUDIM_OPTION:
                if (tvInfoKolaudim.getVisibility() == View.VISIBLE) {
                    tvInfoKolaudim.setVisibility(View.GONE);
                    carKolaudimImage.setVisibility(View.VISIBLE);
                    tvKolaudimLabel.setVisibility(View.VISIBLE);
                }

                Picasso.with(getActivity())
                        .load(uri)
                        .resize(px, py)
                        .centerCrop()
                        .rotate(rotation)
                        .placeholder(R.drawable.loading)
                        .error(R.drawable.error_loading)
                        .into(carKolaudimImage);
                break;
        }
    }

    private void playYoyo(View view) {
        YoYo.with(Techniques.Shake)
                .duration(RentalEngine.YOYO_DURATION)
                .repeat(RentalEngine.YOYO_REPEAT)
                .playOn(view);
    }

    private boolean areCarDocumentationUploaded() {
        if (mUriLicense != null && mUriKolaudim != null && mUriInsurance != null) {
            return true;
        }

        if (mUriLicense == null) {
            playYoyo(tvInfoLicense);
        }
        if (mUriKolaudim == null) {
            playYoyo(tvInfoKolaudim);
        }
        if (mUriInsurance == null) {
            playYoyo(tvInfoInsurance);
        }

        return false;
    }

    private void enableActionsButton(boolean enable) {
        chooseImageBtnLicense.setEnabled(enable);
        takePictureBtnLicense.setEnabled(enable);
        chooseImageBtnInsurance.setEnabled(enable);
        takePictureBtnInsurance.setEnabled(enable);
        chooseImageBtnKolaudim.setEnabled(enable);
        takePictureBtnKolaudim.setEnabled(enable);
    }

    private void checkViews(int mode) {
        switch (mode) {
            case LICENSE_OPTION:
                ivLicenseStatus.setVisibility(View.VISIBLE);
                if (licenseUploaded) {
                    ivLicenseStatus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_success));
                }
                else {
                    ivLicenseStatus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_failed));
                    chooseImageBtnLicense.setEnabled(true);
                    takePictureBtnLicense.setEnabled(true);
                }
                break;
            case KOLAUDIM_OPTION:
                ivKolaudimStatus.setVisibility(View.VISIBLE);
                if (kolaudimUploaded) {
                    ivKolaudimStatus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_success));
                }
                else {
                    ivKolaudimStatus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_failed));
                    chooseImageBtnKolaudim.setEnabled(true);
                    takePictureBtnKolaudim.setEnabled(true);
                }
                break;
            case INSURANCE_OPTION:
                ivInsuranceStatus.setVisibility(View.VISIBLE);
                if (insuranceUploaded) {
                    ivInsuranceStatus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_success));
                }
                else {
                    ivInsuranceStatus.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_failed));
                    chooseImageBtnInsurance.setEnabled(true);
                    takePictureBtnInsurance.setEnabled(true);
                }
                break;
        }
    }

    private void showErrorMessage(String errorMessage) {
        nextButton.setProgress(CircularProgressButton.ERROR_STATE_PROGRESS);
        nextButton.setEnabled(true);
        EventBus.getDefault().post(new ShowSnackBarMessageEvent(errorMessage, false));
    }

    private void tryToUploadCarDocumentation() {
        if (areCarDocumentationUploaded()) {
            nextButton.setProgress(CircularProgressButton.IDLE_STATE_PROGRESS);
            nextButton.setProgress(CircularProgressButton.INDETERMINATE_STATE_PROGRESS);
            enableActionsButton(false);

            if (RentalEngine.isNetworkAvailable(getActivity())) {
                totalItemsToUploads = 0;
                if (!licenseUploaded) {
                    totalItemsToUploads++;
                    new UploadCarDocumentation(LICENSE_OPTION, totalItemsToUploads).execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + carId);
                }
                if (!kolaudimUploaded) {
                    totalItemsToUploads++;
                    new UploadCarDocumentation(KOLAUDIM_OPTION, totalItemsToUploads).execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + carId);
                }
                if (!insuranceUploaded) {
                    totalItemsToUploads++;
                    new UploadCarDocumentation(INSURANCE_OPTION, totalItemsToUploads).execute(RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + carId);
                }
            }
            else {
                Thread timer = new Thread() {
                    public void run() {
                        try {
                            sleep(750);
                        }
                        catch (InterruptedException e) {
                            Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFourthStepFragment thread Exception: " + e.toString());
                        }
                        finally {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_no_internet_error_message)));
                                    enableActionsButton(true);
                                }
                            });
                        }
                    }
                };
                timer.start();
            }
        }
        else {
            EventBus.getDefault().post(new ShowSnackBarMessageEvent(String.valueOf(getResources().getString(R.string.snackbar_select_all_three_car_documentation)), false));
        }
    }

    private void openListYourCarFifthFragment() {
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(500);
                }
                catch (InterruptedException e) {
                    Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFourthStepFragment thread Exception: " + e.toString());
                }
                finally {
                    ListYourCarFifthFragment listYourCarFifthFragment = new ListYourCarFifthFragment();
                    listYourCarFifthFragment.setCarId(carId);
                    EventBus.getDefault().post(new OpenNewFragmentEvent(listYourCarFifthFragment, RentalEngine.LIST_YOUR_CAR_FIFTH_STEP_FRAGMENT_TAG));
                }
            }
        };
        timer.start();
    }

    public InputStream getUploadCardDocumentationInputStream(String url, String formDataKey, ImageObject imageObject) {
        InputStream mInputStream = null;
        OkHttpClient client = new OkHttpClient();

        try {
            MediaType MEDIA_TYPE = MediaType.parse(RentalEngine.getMimeType(imageObject.getImagePath()));
            File tempFile = new File(imageObject.getImagePath());

            RequestBody formBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(formDataKey, imageObject.getImageTitle(), RequestBody.create(MEDIA_TYPE, tempFile))
                    .addFormDataPart(RentalEngine.RENTAL_KINEZERI_NAME, RentalEngine.RENTAL_KINEZERI_VALUE)
                    .build();

            Request request = new Request.Builder()
                    .addHeader(RentalEngine.RENTAL_API_HEADER_TOKEN_NAME, RentalEngine.RENTAL_API_HEADER_TOKEN_VALUE)
                    .url(url)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            mInputStream = response.body().byteStream();
        }
        catch (Exception e) {
            Log.i(RentalEngine.RENTAL_LOG_TAG, "Error for URL " + url, e);
        }

        return mInputStream;
    }

    private class UploadCarDocumentation extends AsyncTask<String, String, String> {
        private int mode;
        private int itemIndex;
        private CarResponse mCarResponse;

        public UploadCarDocumentation(int mode, int itemIndex) {
            this.mode = mode;
            this.itemIndex = itemIndex;
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                InputStream inputStream = null;
                switch (mode) {
                    case LICENSE_OPTION:
                        inputStream = getUploadCardDocumentationInputStream(uri[0], RentalEngine.CAR_DOCUMENT_LICENSE_PARAM, new ImageObject(mUriLicense, mPathLicense, mFileNameLicense, 0));
                        break;
                    case KOLAUDIM_OPTION:
                        inputStream = getUploadCardDocumentationInputStream(uri[0], RentalEngine.CAR_DOCUMENT_KOLAUDIMI_PARAM, new ImageObject(mUriKolaudim, mPathKolaudim, mFileNameKolaudim, 0));
                        break;
                    case INSURANCE_OPTION:
                        inputStream = getUploadCardDocumentationInputStream(uri[0], RentalEngine.CAR_DOCUMENT_INSURANCE_PARAM, new ImageObject(mUriInsurance, mPathInsurance, mFileNameInsurance, 0));
                        break;
                }
                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                Gson gson = gsonBuilder.create();
                if (inputStream != null) {
                    Reader reader = new InputStreamReader(inputStream);
                    mCarResponse = gson.fromJson(reader, CarResponse.class);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "ListYourCarFourthFragment UploadCarDocumentation doInBackground Exception: " + e.toString());
                mCarResponse = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (mCarResponse != null && mCarResponse.getResponseStatus() == 0) {
                switch (mode) {
                    case LICENSE_OPTION:
                        licenseUploaded = true;
                        break;
                    case KOLAUDIM_OPTION:
                        kolaudimUploaded = true;
                        break;
                    case INSURANCE_OPTION:
                        insuranceUploaded = true;
                        break;
                }
            }
            checkViews(mode);
            if (itemIndex == totalItemsToUploads) {
                if (licenseUploaded && kolaudimUploaded && insuranceUploaded) {
                    nextButton.setProgress(CircularProgressButton.SUCCESS_STATE_PROGRESS);
                    nextButton.setAnimationListener(onAnimationEndListener);
                    nextButton.setEnabled(false);
                    enableActionsButton(true);
                    openListYourCarFifthFragment();
                }
                else {
                    showErrorMessage(String.valueOf(getResources().getString(R.string.snackbar_update_failed)));
                }
            }
        }
    }
}
