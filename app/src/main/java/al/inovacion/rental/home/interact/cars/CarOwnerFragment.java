package al.inovacion.rental.home.interact.cars;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import org.greenrobot.eventbus.EventBus;

import al.inovacion.rental.R;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.models.api.User;
import al.inovacion.rental.engine.RentalEngine;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Arbër Thaçi on 17-04-29.
 * Email: arberlthaci@gmail.com
 */

public class CarOwnerFragment extends Fragment {

    private TextView tvFirstName;
    private TextView tvLastName;
    private TextView tvEmail;
    private TextView tvPhone;

    private User carOwner;
    String firstName = "";
    String lastName = "";

    public CarOwnerFragment() { }

    public static CarOwnerFragment newInstance() {
        return new CarOwnerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
        getCarOwner();
    }

    @Override
    public void onResume() {
        super.onResume();
        getCarOwner();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.CAR_OWNER_FRAGMENT_TAG, firstName + " " + lastName));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_car_owner, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeComponents(view);
        afterInitialization();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        tvFirstName = (TextView) view.findViewById(R.id.tvCOFirstNameValue);
        tvLastName = (TextView) view.findViewById(R.id.tvCOLastNameValue);
        tvEmail = (TextView) view.findViewById(R.id.tvCOEmailValue);
        tvPhone = (TextView) view.findViewById(R.id.tvCOwnerPhoneValue);
    }

    private void afterInitialization() {
        loadUser();
    }

    private void getCarOwner() {
        if (carOwner == null && RentalEngine.getCarOwner() != null) {
            carOwner = RentalEngine.getCarOwner();

            firstName = carOwner.getFirstName();
            lastName = carOwner.getLastName();
        }
    }

    private void loadUser() {
        if (carOwner != null) {
            tvFirstName.setText(carOwner.getFirstName());
            tvLastName.setText(carOwner.getLastName());
            tvEmail.setText(carOwner.getEmail());
            tvPhone.setText(carOwner.getPhone());
        }
    }
}
