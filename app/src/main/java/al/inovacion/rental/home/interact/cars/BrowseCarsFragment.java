package al.inovacion.rental.home.interact.cars;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.ChipViewAdapter;
import com.plumillonforge.android.chipview.OnChipClickListener;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import al.inovacion.rental.R;
import al.inovacion.rental.adapter.CarsAdapter;
import al.inovacion.rental.adapter.FilterChipViewAdapter;
import al.inovacion.rental.data.event.CloseKeyboardEvent;
import al.inovacion.rental.data.event.SearchFiltersEvent;
import al.inovacion.rental.data.event.SetFragmentTagEvent;
import al.inovacion.rental.data.event.StopRefreshingEvent;
import al.inovacion.rental.data.event.SwipeToRefreshEvent;
import al.inovacion.rental.data.models.api.Car;
import al.inovacion.rental.data.models.app.Filter;
import al.inovacion.rental.data.models.app.HttpResponse;
import al.inovacion.rental.engine.RentalEngine;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Arbër Thaçi on 17-03-23.
 * Email: arberlthaci@gmail.com
 */

public class BrowseCarsFragment extends Fragment {

    private LinearLayout noInternetView;
    private LinearLayout noDataView;
    private LinearLayout errorView;
    private AVLoadingIndicatorView loading;
    private RecyclerView rvCars;
    private LinearLayout chipView;
    private ChipView filterChips;

    private CarsAdapter carsAdapter;
    private ArrayList<Car> carsList = new ArrayList<>();
    private ChipViewAdapter filterAdapter;

    private static final int NO_INTERNET_MODE = 1;
    private static final int NO_DATA_MODE = 2;
    private static final int ERROR_MODE = 3;
    private static final int DATA_AVAILABLE = 4;

    private static final int REFRESH_MODE = 1;
    private static final int SEARCH_FILTERS_MODE = 2;

    public BrowseCarsFragment() {}

    public static BrowseCarsFragment newInstance() {
        return new BrowseCarsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(getContext(), new Crashlytics());
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().post(new SetFragmentTagEvent(RentalEngine.BROWSE_CARS_FRAGMENT_TAG, getResources().getString(R.string.menu_item_browse_cars)));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_browse_cars, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().post(new CloseKeyboardEvent());
        initializeComponents(view);
        afterInitialization();

        if (RentalEngine.getCarsList() != null) {
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(250);
                    }
                    catch (InterruptedException e) {
                        Log.i(RentalEngine.RENTAL_LOG_TAG, "BrowseYourCarFragment thread Exception: " + e.toString());
                    }
                    finally {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                fillCarList();
                            }
                        });
                    }
                }
            };
            timer.start();
        }
        else {
            if (RentalEngine.isNetworkAvailable(getActivity())) {
                callGetCarsToBookApi();
            } else {
                loading.smoothToHide();
                hideViews(NO_INTERNET_MODE);
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initializeComponents(View view) {
        noDataView = (LinearLayout) view.findViewById(R.id.llNoData);
        noInternetView = (LinearLayout) view.findViewById(R.id.llNoInternet);
        errorView = (LinearLayout) view.findViewById(R.id.llError);
        loading = (AVLoadingIndicatorView) view.findViewById(R.id.bcLoading);
        rvCars = (RecyclerView) view.findViewById(R.id.rvBCCars);
        chipView = (LinearLayout) view.findViewById(R.id.llChipView);
        filterChips = (ChipView) view.findViewById(R.id.bcChipView);
    }

    private void afterInitialization() {
        prepareCarsView();
        prepareFiltersView();
    }

    private void prepareCarsView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCars.setLayoutManager(mLayoutManager);
        rvCars.setItemAnimator(new DefaultItemAnimator());
        rvCars.setNestedScrollingEnabled(false);
        rvCars.setHasFixedSize(false);

        carsAdapter = new CarsAdapter(carsList, RentalEngine.CARS_TO_BOOK);
        rvCars.setAdapter(carsAdapter);
    }

    private void prepareFiltersView() {
        filterAdapter = new FilterChipViewAdapter(getContext());
        filterChips.setAdapter(filterAdapter);
        filterChips.setChipBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        filterChips.setChipBackgroundColorSelected(ContextCompat.getColor(getContext(), R.color.colorAccentLight));
        filterChips.setOnChipClickListener(new OnChipClickListener() {
            @Override
            public void onChipClick(Chip chip) {
                filterAdapter.remove(chip);
                if (filterAdapter.count() == 0) {
                    callGetCarsToBookApi();
                }
                else {
                    callGetFilteredCarsApi(filterAdapter.getChipList());
                }
                checkChipView();
            }
        });
        filterAdapter.setChipList(RentalEngine.getFilters());
        checkChipView();
    }

    private void checkChipView() {
        if (filterAdapter.count() == 0) {
            chipView.setVisibility(View.GONE);
        }
        else {
            chipView.setVisibility(View.VISIBLE);
        }
    }

    private void callGetCarsToBookApi() {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + "/" + RentalEngine.getUser().getId() + RentalEngine.RENTAL_API_CAN_BOOK_CARS_ROUTE;
        new HttpGetCarsAsync().execute(urlApi);
    }

    private void callGetFilteredCarsApi(List<Chip> filters) {
        String urlApi = RentalEngine.RENTAL_API_BASE_URL + RentalEngine.RENTAL_API_CARS_ROUTE + RentalEngine.RENTAL_API_SEARCH_FILTERS_ROUTE;
        if (RentalEngine.getUser() != null) {
            urlApi += "user_id=" + RentalEngine.getUser().getId();
        }
        else {
            urlApi += "user_id=0";
        }
        for (Chip iChip : filters) {
            urlApi += ((Filter) iChip).getValue();
        }
        new HttpGetCarsAsync().execute(urlApi);
    }

    private void hideViews(int mode) {
        rvCars.setVisibility(View.GONE);
        noDataView.setVisibility(View.GONE);
        noInternetView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);

        if (mode == NO_INTERNET_MODE) {
            noInternetView.setVisibility(View.VISIBLE);
        }
        else if (mode == NO_DATA_MODE) {
            noDataView.setVisibility(View.VISIBLE);
        }
        else if (mode == ERROR_MODE) {
            errorView.setVisibility(View.VISIBLE);
        }
        else if (mode == DATA_AVAILABLE) {
            rvCars.setVisibility(View.VISIBLE);
        }
    }

    private void fillCarList() {
        carsList = RentalEngine.getCarsList();
        carsAdapter.setItemsList(carsList);
        loading.smoothToHide();
        hideViews(DATA_AVAILABLE);
    }

    private void refreshRecyclerView(int refreshMode, List<Chip> filters) {
        if (RentalEngine.isNetworkAvailable(getActivity())) {
            if (rvCars.getVisibility() == View.GONE) {
                hideViews(-1);
            }

            if (refreshMode == REFRESH_MODE) {
                callGetCarsToBookApi();
                checkChipView();
                filterAdapter.getChipList().clear();
            }
            else if (refreshMode == SEARCH_FILTERS_MODE) {
                callGetFilteredCarsApi(filters);
                RentalEngine.setFilters(filters);
                filterAdapter.setChipList(filters);
            }
            checkChipView();
        }
        else {
            loading.smoothToShow();
            Thread timer = new Thread() {
                public void run() {
                    try {
                        sleep(1000);
                    }
                    catch (InterruptedException e) {
                        Log.i(RentalEngine.RENTAL_LOG_TAG, "BrowseYourCarFragment Swipe thread Exception: " + e.toString());
                    }
                    finally {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new StopRefreshingEvent());
                                loading.smoothToHide();
                                noInternetView.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            };
            timer.start();
            hideViews(-1);
        }
    }

    private class HttpGetCarsAsync extends AsyncTask<String, String, String> {
        private HttpResponse tempResponse;
        private ArrayList<Car> tempCars;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.smoothToShow();
            hideViews(-1);
        }

        @Override
        protected String doInBackground(String... uri) {
            try {
                tempResponse = RentalEngine.retrieveStream(uri[0]);
                if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setDateFormat(RentalEngine.GSON_DATETIME_FORMAT);
                    Gson gson = gsonBuilder.create();
                    Reader reader = new InputStreamReader(tempResponse.getInputStream());
                    Type collectionType = new TypeToken<Collection<Car>>() {}.getType();
                    tempCars = gson.fromJson(reader, collectionType);
                }
            }
            catch (Exception e) {
                Log.i(RentalEngine.RENTAL_LOG_TAG, "BrowseCarsFragment HttpGetCarsAsync doInBackground Exception: " + e.toString());
                tempCars = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            EventBus.getDefault().post(new StopRefreshingEvent());
            if (tempResponse.getStatus() == RentalEngine.HTTP_RESPONSE_SUCCESS) {
                if (tempCars != null && tempCars.size() != 0 && carsAdapter != null) {
                    RentalEngine.setCarsList(tempCars);
                    fillCarList();
                }
                else {
                    hideViews(NO_DATA_MODE);
                }
            }
            else {
                hideViews(ERROR_MODE);
            }
            loading.smoothToHide();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(SwipeToRefreshEvent event) {
        if (event != null) {
            refreshRecyclerView(REFRESH_MODE, null);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(SearchFiltersEvent event) {
        if (event != null) {
            refreshRecyclerView(SEARCH_FILTERS_MODE, event.getFilters());
        }
    }
}
