package al.inovacion.rental.data.models.app;

import java.io.InputStream;

/**
 * Created by Arbër Thaçi on 17-05-09.
 * Email: arberlthaci@gmail.com
 */

public class HttpResponse {
    private InputStream inputStream;
    private int status;

    public HttpResponse(InputStream inputStream, int status) {
        this.inputStream = inputStream;
        this.status = status;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public int getStatus() {
        return status;
    }
}
