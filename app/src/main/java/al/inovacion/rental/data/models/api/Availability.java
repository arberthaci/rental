package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Arbër Thaçi on 17-05-11.
 * Email: arberlthaci@gmail.com
 */

public class Availability {
    @SerializedName("id")
    private int id;
    @SerializedName("start_date")
    private Date startDate;
    @SerializedName("end_date")
    private Date endDate;
    @SerializedName("is_active")
    private boolean isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
