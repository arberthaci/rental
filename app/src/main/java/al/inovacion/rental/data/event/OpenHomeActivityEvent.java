package al.inovacion.rental.data.event;

import al.inovacion.rental.data.models.api.User;

/**
 * Created by Arbër Thaçi on 17-03-23.
 * Email: arberlthaci@gmail.com
 */

public class OpenHomeActivityEvent {
    private User user;

    public OpenHomeActivityEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
