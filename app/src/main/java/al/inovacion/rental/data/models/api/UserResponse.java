package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arbër Thaçi on 17-03-22.
 * Email: arberlthaci@gmail.com
 */

public class UserResponse {
    private User responseData;
    @SerializedName("responseStatus")
    private int responseStatus;

    public User getResponseData() {
        return responseData;
    }

    public void setResponseData(User responseData) {
        this.responseData = responseData;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }
}
