package al.inovacion.rental.data.event;

/**
 * Created by Arbër Thaçi on 17-03-24.
 * Email: arberlthaci@gmail.com
 */

public class SetFragmentTagEvent {
    private String fragmentTag;
    private String toolbarTitle;

    public SetFragmentTagEvent(String fragmentTag, String toolbarTitle) {
        this.fragmentTag = fragmentTag;
        this.toolbarTitle = toolbarTitle;
    }

    public String getFragmentTag() {
        return fragmentTag;
    }

    public String getToolbarTitle() {
        return toolbarTitle;
    }
}
