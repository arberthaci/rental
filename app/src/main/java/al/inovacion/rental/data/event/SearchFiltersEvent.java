package al.inovacion.rental.data.event;

import com.plumillonforge.android.chipview.Chip;

import java.util.List;

/**
 * Created by Arbër Thaçi on 17-04-30.
 * Email: arberlthaci@gmail.com
 */

public class SearchFiltersEvent {
    private List<Chip> filters;

    public SearchFiltersEvent(List<Chip> filters) {
        this.filters = filters;
    }

    public List<Chip> getFilters() {
        return filters;
    }
}
