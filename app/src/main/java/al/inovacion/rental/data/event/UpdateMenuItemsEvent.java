package al.inovacion.rental.data.event;

/**
 * Created by Arbër Thaçi on 17-04-15.
 * Email: arberlthaci@gmail.com
 */

public class UpdateMenuItemsEvent {
    private int menuItemIndicator;

    public UpdateMenuItemsEvent(int menuItemIndicator) {
        this.menuItemIndicator = menuItemIndicator;
    }

    public int getMenuItemIndicator() {
        return menuItemIndicator;
    }
}
