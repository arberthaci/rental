package al.inovacion.rental.data.event;

import al.inovacion.rental.data.models.api.User;

/**
 * Created by Arbër Thaçi on 17-03-29.
 * Email: arberlthaci@gmail.com
 */

public class StoreUserDataEvent {
    private User user;

    public StoreUserDataEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
