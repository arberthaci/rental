package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Arbër Thaçi on 17-05-12.
 * Email: arberlthaci@gmail.com
 */

public class CarDocument {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("document_type_id")
    private int documentTypeId;
    @SerializedName("document")
    private String document;
    @SerializedName("expire_date")
    private Date expireDate;
    @SerializedName("status")
    private String status;
    @SerializedName("status_id")
    private int statusId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
