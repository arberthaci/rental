package al.inovacion.rental.data.models.edmunds;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 17-03-29.
 * Email: arberlthaci@gmail.com
 */

public class Makes {
    private ArrayList<Make> makes;
    @SerializedName("makesCount")
    private int makesCount;

    public ArrayList<Make> getMakes() {
        return makes;
    }

    public void setMakes(ArrayList<Make> makes) {
        this.makes = makes;
    }

    public int getMakesCount() {
        return makesCount;
    }

    public void setMakesCount(int makesCount) {
        this.makesCount = makesCount;
    }
}
