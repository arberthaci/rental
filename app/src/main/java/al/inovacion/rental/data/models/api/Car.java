package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 17-04-16.
 * Email: arberlthaci@gmail.com
 */

public class Car {
    @SerializedName("id")
    private int id;
    @SerializedName("manufacturer")
    private String manufacturer;
    @SerializedName("model")
    private String model;
    @SerializedName("transmission")
    private String transmission;
    @SerializedName("production_year")
    private String productionYear;
    @SerializedName("body_type")
    private String bodyType;
    @SerializedName("daily_price")
    private String dailyPrice;
    @SerializedName("warranty_price")
    private String warrantyPrice;
    @SerializedName("address")
    private String address;
    @SerializedName("has_ac")
    private boolean hasAc;
    @SerializedName("odometer")
    private String odometer;
    private ArrayList<CarPhoto> photos;
    private ArrayList<Availability> availabilities;
    private ArrayList<Availability> busy_times;
    private ArrayList<CarDocument> documents;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("is_active")
    private boolean isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(String productionYear) {
        this.productionYear = productionYear;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getDailyPrice() {
        return dailyPrice;
    }

    public void setDailyPrice(String dailyPrice) {
        this.dailyPrice = dailyPrice;
    }

    public String getWarrantyPrice() {
        return warrantyPrice;
    }

    public void setWarrantyPrice(String warrantyPrice) {
        this.warrantyPrice = warrantyPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isHasAc() {
        return hasAc;
    }

    public void setHasAc(boolean hasAc) {
        this.hasAc = hasAc;
    }

    public String getOdometer() {
        return odometer;
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public ArrayList<CarPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<CarPhoto> photos) {
        this.photos = photos;
    }

    public ArrayList<Availability> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(ArrayList<Availability> availabilities) {
        this.availabilities = availabilities;
    }

    public ArrayList<Availability> getBusy_times() {
        return busy_times;
    }

    public void setBusy_times(ArrayList<Availability> busy_times) {
        this.busy_times = busy_times;
    }

    public ArrayList<CarDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<CarDocument> documents) {
        this.documents = documents;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
