package al.inovacion.rental.data.models.edmunds;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 17-03-29.
 * Email: arberlthaci@gmail.com
 */

public class Year {
    @SerializedName("id")
    private int id;
    @SerializedName("states")
    private ArrayList<String> states;
    @SerializedName("year")
    private int year;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<String> getStates() {
        return states;
    }

    public void setStates(ArrayList<String> states) {
        this.states = states;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
