package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arbër Thaçi on 17-04-19.
 * Email: arberlthaci@gmail.com
 */

public class CarPhotoResponse {
    private CarPhoto responseData;
    @SerializedName("responseStatus")
    private int responseStatus;

    public CarPhoto getResponseData() {
        return responseData;
    }

    public void setResponseData(CarPhoto responseData) {
        this.responseData = responseData;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }
}
