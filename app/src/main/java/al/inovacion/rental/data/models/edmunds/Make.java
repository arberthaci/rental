package al.inovacion.rental.data.models.edmunds;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 17-03-29.
 * Email: arberlthaci@gmail.com
 */

public class Make {
    @SerializedName("id")
    private int id;
    private ArrayList<Model> models;
    @SerializedName("name")
    private String name;
    @SerializedName("niceName")
    private String niceName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Model> getModels() {
        return models;
    }

    public void setModels(ArrayList<Model> models) {
        this.models = models;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNiceName() {
        return niceName;
    }

    public void setNiceName(String niceName) {
        this.niceName = niceName;
    }
}
