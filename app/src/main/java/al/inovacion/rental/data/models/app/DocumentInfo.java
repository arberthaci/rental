package al.inovacion.rental.data.models.app;

/**
 * Created by Arbër Thaçi on 17-05-15.
 * Email: arberlthaci@gmail.com
 */

public class DocumentInfo {
    private String warningMessage;
    private boolean isWarning;

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    public boolean isWarning() {
        return isWarning;
    }

    public void setWarning(boolean warning) {
        isWarning = warning;
    }
}
