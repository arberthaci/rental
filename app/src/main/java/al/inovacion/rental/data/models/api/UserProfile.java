package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arbër Thaçi on 17-04-13.
 * Email: arberlthaci@gmail.com
 */

public class UserProfile {
    @SerializedName("id_card")
    private String idCard;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("status")
    private String status;
    @SerializedName("status_id")
    private int statusId;

    public UserProfile(String avatar, String idCard, int statusId) {
        this.avatar = avatar;
        this.idCard = idCard;
        this.statusId = statusId;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
