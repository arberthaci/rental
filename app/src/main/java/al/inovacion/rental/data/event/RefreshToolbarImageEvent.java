package al.inovacion.rental.data.event;

/**
 * Created by Arbër Thaçi on 17-05-03.
 * Email: arberlthaci@gmail.com
 */

public class RefreshToolbarImageEvent {
    String imagePath;

    public RefreshToolbarImageEvent(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }
}
