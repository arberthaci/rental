package al.inovacion.rental.data.event;

import al.inovacion.rental.data.models.api.Car;

/**
 * Created by Arbër Thaçi on 17-04-27.
 * Email: arberlthaci@gmail.com
 */

public class OpenViewCarEvent {
    private Car car;
    private int mode;

    public OpenViewCarEvent(Car car, int mode) {
        this.car = car;
        this.mode = mode;
    }

    public Car getCar() {
        return car;
    }

    public int getMode() {
        return mode;
    }
}
