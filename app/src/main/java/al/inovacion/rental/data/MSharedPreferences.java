package al.inovacion.rental.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Arbër Thaçi on 17-03-23.
 * Email: arberlthaci@gmail.com
 */

public class MSharedPreferences {

    private SharedPreferences sharedPrefs;
    private SharedPreferences.Editor mEditor;

    public MSharedPreferences(Context mContext, String sharedPreferencesKey) {
        sharedPrefs = mContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE);
    }

    public String getString(String key){
        return getString(key, "");
    }

    public String getString(String key, String defValue){
        return sharedPrefs.getString(key, defValue);
    }

    public int getInt(String key){
        return getInt(key, 0);
    }

    public int getInt(String key, int defValue){
        return sharedPrefs.getInt(key, defValue);
    }


    public void putString(String key, String value){
        mEditor = sharedPrefs.edit();
        mEditor.putString(key, value);
        mEditor.apply();
    }

    public void putInt(String key, int value){
        mEditor = sharedPrefs.edit();
        mEditor.putInt(key, value);
        mEditor.apply();
    }

    public void putBoolean(String key, boolean value){
        mEditor = sharedPrefs.edit();
        mEditor.putBoolean(key, value);
        mEditor.apply();
    }

    public void clear(){
        mEditor = sharedPrefs.edit();
        mEditor.clear();
        mEditor.apply();
    }
}