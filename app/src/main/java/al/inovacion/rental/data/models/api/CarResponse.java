package al.inovacion.rental.data.models.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arbër Thaçi on 17-04-16.
 * Email: arberlthaci@gmail.com
 */

public class CarResponse {
    private Car responseData;
    @SerializedName("responseStatus")
    private int responseStatus;

    public Car getResponseData() {
        return responseData;
    }

    public void setResponseData(Car responseData) {
        this.responseData = responseData;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }
}
