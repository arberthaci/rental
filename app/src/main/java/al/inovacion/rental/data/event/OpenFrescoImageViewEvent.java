package al.inovacion.rental.data.event;

import java.util.ArrayList;

import al.inovacion.rental.data.models.app.ImageObject;

/**
 * Created by Arbër Thaçi on 17-04-30.
 * Email: arberlthaci@gmail.com
 */

public class OpenFrescoImageViewEvent {
    private ArrayList<ImageObject> photoImages;
    private int selectedPosition;

    public OpenFrescoImageViewEvent(ArrayList<ImageObject> photoImages, int selectedPosition) {
        this.photoImages = photoImages;
        this.selectedPosition = selectedPosition;
    }

    public ArrayList<ImageObject> getPhotoImages() {
        return photoImages;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}
