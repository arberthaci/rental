package al.inovacion.rental.data.event;

import android.support.v4.app.Fragment;

/**
 * Created by Arbër Thaçi on 17-03-22.
 * Email: arberlthaci@gmail.com
 */

public class OpenNewFragmentEvent {
    private Fragment fragment;
    private String tag;

    public OpenNewFragmentEvent(Fragment fragment, String tag) {
        this.fragment = fragment;
        this.tag = tag;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public String getTag() {
        return tag;
    }
}
