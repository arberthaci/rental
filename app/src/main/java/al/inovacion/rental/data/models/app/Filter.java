package al.inovacion.rental.data.models.app;

import com.plumillonforge.android.chipview.Chip;

/**
 * Created by Arbër Thaçi on 17-05-10.
 * Email: arberlthaci@gmail.com
 */

public class Filter implements Chip {
    private String mName;
    private int mType = 0;
    private String value;

    public Filter(String name, int type) {
        this(name);
        mType = type;
    }

    public Filter(String name, String value) {
        this(name);
        this.value = value;
    }

    public Filter(String name) {
        mName = name;
    }

    @Override
    public String getText() {
        return mName;
    }

    public int getType() {
        return mType;
    }

    public String getValue() {
        return value;
    }
}
