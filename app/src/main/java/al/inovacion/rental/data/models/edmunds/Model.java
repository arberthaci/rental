package al.inovacion.rental.data.models.edmunds;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Arbër Thaçi on 17-03-29.
 * Email: arberlthaci@gmail.com
 */

public class Model {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("niceName")
    private String niceName;
    @SerializedName("states")
    private ArrayList<String> states;
    private ArrayList<Year> years;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNiceName() {
        return niceName;
    }

    public void setNiceName(String niceName) {
        this.niceName = niceName;
    }

    public ArrayList<String> getStates() {
        return states;
    }

    public void setStates(ArrayList<String> states) {
        this.states = states;
    }

    public ArrayList<Year> getYears() {
        return years;
    }

    public void setYears(ArrayList<Year> years) {
        this.years = years;
    }
}
