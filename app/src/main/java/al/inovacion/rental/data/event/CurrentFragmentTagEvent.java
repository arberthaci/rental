package al.inovacion.rental.data.event;

/**
 * Created by Arbër Thaçi on 17-03-22.
 * Email: arberlthaci@gmail.com
 */

public class CurrentFragmentTagEvent {
    private String tag;

    public CurrentFragmentTagEvent(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
