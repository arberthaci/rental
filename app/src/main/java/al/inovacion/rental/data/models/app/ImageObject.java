package al.inovacion.rental.data.models.app;

import android.net.Uri;

import al.inovacion.rental.engine.RentalEngine;

/**
 * Created by Arbër Thaçi on 17-03-31.
 * Email: arberlthaci@gmail.com
 */

public class ImageObject {
    private int id;
    private Uri imageUri;
    private String imagePath;
    private String imageThumbnailPath;
    private String imageTitle;
    private float imageRotation;
    private int type;
    private int documentType;
    private int imageStatus;
    private int statusId;

    // local image
    public ImageObject(Uri imageUri, String imagePath, String imageTitle, float imageRotation) {
        this.id = -1;
        this.imageUri = imageUri;
        this.imagePath = imagePath;
        this.imageThumbnailPath = null;
        this.imageTitle = imageTitle;
        this.imageRotation = imageRotation;
        this.type = RentalEngine.LOCAL_IMAGE;
        this.imageStatus = -1;
    }

    // server image
    public ImageObject(int id, String imagePath, String imageThumbnailPath, String imageTitle, int documentType, int statusId) {
        this.id = id;
        this.imageUri = null;
        this.imagePath = imagePath;
        this.imageThumbnailPath = imageThumbnailPath;
        this.imageTitle = imageTitle;
        this.type = RentalEngine.SERVER_IMAGE;
        this.documentType = documentType;
        this.imageStatus = -1;
        this.statusId = statusId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageThumbnailPath() {
        return imageThumbnailPath;
    }

    public void setImageThumbnailPath(String imageThumbnailPath) {
        this.imageThumbnailPath = imageThumbnailPath;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public float getImageRotation() {
        return imageRotation;
    }

    public void setImageRotation(float imageRotation) {
        this.imageRotation = imageRotation;
    }

    public int getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(int imageStatus) {
        this.imageStatus = imageStatus;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDocumentType() {
        return documentType;
    }

    public void setDocumentType(int documentType) {
        this.documentType = documentType;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
