package al.inovacion.rental.data.event;

/**
 * Created by Arbër Thaçi on 17-05-03.
 * Email: arberlthaci@gmail.com
 */

public class DeleteCarPhotoEvent {
    private int photoId;
    private int position;

    public DeleteCarPhotoEvent(int photoId, int position) {
        this.photoId = photoId;
        this.position = position;
    }

    public int getPhotoId() {
        return photoId;
    }

    public int getPosition() {
        return position;
    }
}
