package al.inovacion.rental.data.event;

/**
 * Created by Arbër Thaçi on 17-03-22.
 * Email: arberlthaci@gmail.com
 */

public class ShowSnackBarMessageEvent {
    private String message;
    private boolean hasCallback;

    public ShowSnackBarMessageEvent(String message, boolean hasCallback) {
        this.message = message;
        this.hasCallback = hasCallback;
    }

    public String getMessage() {
        return message;
    }

    public boolean hasCallback() {
        return hasCallback;
    }
}
